$(document).ready(function() {		 

	//$("#purchaseStockListTable").dataTable();
	$('#purchaseStockListTable').dataTable( {
	    "aoColumnDefs" : [ {
	        "bSortable" : false,
	        "aTargets" : [ 'no-sort' ]
	    } ]
	});
	
	$(".no-sort1").removeClass("sorting_asc");
	
	var errors = 0;
	showMessage();
	
	function showMessage(){
		var errMsg = $("#errorMsg").val();
		var sucMsg = $("#successMsg").val();
		if(errMsg != null && errMsg.length > 0)
			swal("Stock Purchase", errMsg , "error");
		else if(sucMsg != null && sucMsg.length > 0)
			swal("Stock Purchase", sucMsg , "success");
	}
	
	$('input#purchaseDateStr').datepicker({
		dateFormat: 'dd/mm/yy',
		setDate: new Date()
	});
	
	$(".delete_link").click(function(e){
		e.preventDefault();
		var deleteId = $("#stockId").val();
		swal({
			title : "Delete Confirmation",
			text : "Are you sure to delete?",
			showCancelButton: true,
			confirmButtonText: "Confirm"
		}, function() {
			window.location.href = "/deletePurchase/" + deleteId;
		});
	});
	
	$("#reset").click(function(e){
		e.preventDefault();
		errors= 0;
		$("#purchaseForm .error").text('');
		$("#purchaseForm input[type=text]").val('');
		$("#purchaseForm input[type=file]").val('');
		$("#purchaseForm select").val(1);
		$("#id").val(0);
		$("#prevAvgPrice").val(0);
		$("#stockId").val(0);
		$("#prevCodeNo").val('');
		$("#oldCodeNumber").val('');
		$("#errorMsg").val('');
		$("#successMsg").val('');
	});
	
	/*$("#codeNumber").change(function(){
		$("#purchasePrice").val(0);
		$("#averagePrice").val(0);
		var prevCode = $("#prevCodeNo").val();
		var purPrice = $("#purchasePrice").val();
		if( purPrice == null || prevCode == "undefined" || purPrice.length <= 0)
			purPrice = 0;
		if(prevCode!=null && prevCode!="" && typeof prevCode != "undefined"){
			var curCode = $("#codeNumber").val();
			if(curCode != prevCode)
				$("#averagePrice").val(purPrice);
			else
			{				
				purPrice = parseFloat(purPrice);
				var avgPrice = $("#prevAvgPrice").val();
				avgPrice = parseFloat(avgPrice);
				if(avgPrice > 0)				
					$("#averagePrice").val((avgPrice+purPrice)/2);
			}
		}
		
	});*/
	
	$("#currencyId").change(function(){
		if($("#currencyId").val() == 2)
			$(".exchangeRateDiv").show();
		else
			$(".exchangeRateDiv").hide();
	});
	
	$("#purchasePrice").change(function(){
		showAveragePrice();
	});
	
	function showAveragePrice(){
		var sId = $("#stockId").val();
		var purPrice = $("#purchasePrice").val();
		var purErr = 0;
		if(purPrice==null || purPrice=="" || typeof purPrice == "undefined" || !purPrice.trim()){				
			$("#purchasePriceErrMsg").text("Purchase Price is required.");
			purErr=1;
		}
		if(!$.isNumeric(purPrice)){
			$("#purchasePriceErrMsg").text("Purchase Price allow only number.");
			purErr=1;
		}
		if(purPrice <= 0)
		{
			$("#purchasePriceErrMsg").text("Purchase Price must greater than 0.");
			purErr=1;
		}
		if(purErr == 0){
			purPrice = parseFloat(purPrice);
			var avgPrice = $("#prevAvgPrice").val();
			avgPrice = parseFloat(avgPrice);
			if(avgPrice > 0)				
				$("#averagePrice").val((avgPrice+purPrice)/2);
			else
				$("#averagePrice").val(purPrice);
		}
			
	}
	
	$(".delete_link").click(function(e){
		e.preventDefault();
		var deleteId = $("#pId").val();
		swal({
			title : "Delete Confirmation",
			text : "Are you sure to delete?",
			showCancelButton: true,
			confirmButtonText: "Confirm"
		}, function() {
			window.location.href = "/purchaseStockDelete?id=" + deleteId;
		});
	});
	
	$(".update_link").click(function(e){
		e.preventDefault();
		var id = $("#pId").val();
		var datas = $(this).closest("tr").find(".sData");// Finds all children <td> elements that has class - ids
		var ids = $(this).closest("tr").find(".ids");// Finds all children <td> elements that has class - ids
		var idsArray = new Array();
		var dataArray = new Array();
		$.each(ids,function(){
			idsArray.push($(this).val());
		});
		$.each(datas,function(){
			dataArray.push($(this).context.innerHTML);
		});
		//alert("data[] "+dataArray + " id[] "+idsArray);
		$("#id").val(id);
		//$("#categoryId").val(idsArray[0]);
		$("#currencyId").val(idsArray[1]);
		$("#imageByte").val(idsArray[2]); //image
		$("#imageString").val(idsArray[3]); //image		
		$("#codeNumber").val(dataArray[0]);
		$("#stockName").val(dataArray[1]);
		$("#categoryName").val(dataArray[2]);
		$("#subCategory").val(dataArray[3]);
		$("#brandName").val(dataArray[4]);
		$("#color").val(dataArray[5]);
		$("#purchasePrice").val(dataArray[6]);
		$("#averagePrice").val(dataArray[7]);
		$("#sellingPrice1").val(dataArray[8]);
		$("#sellingPrice2").val(dataArray[9]);
		$("#sellingPrice3").val(dataArray[10]);
		$("#sellingPrice4").val(dataArray[11]);
		$("#description").val(dataArray[12]);
		$("#quantityLarge").val(dataArray[13]);
		$("#quantityMedium").val(dataArray[14]);
		$("#quantitySmall").val(dataArray[15]);
		$("#unitMeasurement").val(dataArray[16]);

		$("#currencyName").val(dataArray[18]);		
		$("#packingMedium").val($("#packingM").val());
		$("#packingSmall").val($("#packingS").val());
	});
	
	$("#removeStock").click(function(e){
		e.preventDefault();
		var row = btn.parentNode.parentNode;
		row.parentNode.removeChild(row);
	});

	$( "#codeNumber" ).autocomplete({
		minLength: 2,
        source: function (request, response) {
        	$.ajax({
    			type	:	"GET",
    			url		:	"searchStockByCode.html?code="+$.trim($('#codeNumber').val()),
    			dataType:	'json',
    			success :	function (data) {
    				if(data.length <= 0){
    					$("#stockId").val(0);
    		        	$("#averagePrice").val($("#purchasePrice").val());//prevent not to calculate averagePrice when previous autocomplete data is existing
    				}
    				response(
    						$.map(data, function( value, key ){
    							return {
    								label: value.codeNumber+"("+value.stockName+"-"+value.brandName+")",
    								codeNumber: value.codeNumber,
    								stockName: value.stockName,
    								stockId: value.id,
    								brandName: value.brandName, 								
    								categoryId: value.categoryId,
    								currencyId: value.currencyId,
    								subCategory: value.subCategory,
    								categoryName: value.categoryName,    								
    								color: value.color,
    								purchasePrice: value.purchasePrice,
    								averagePrice: value.averagePrice,
    								sellingPrice1: value.sellingPrice1,
    								sellingPrice2: value.sellingPrice2,
    								sellingPrice3: value.sellingPrice3,
    								sellingPrice4: value.sellingPrice4,
    								description: value.description,
    								quantityLarge: value.quantityLarge,    								
    								unitMeasurement: value.unitMeasurement,
    								packingMedium: value.packingMedium,
    								packingSmall: value.packingSmall,
    								currencyName: value.currencyName,
    								totalQty : value.totalQty,
    								imageByte: value.imageByte,
    								imageString: value.imageString
    							};
    						})
    					);
                }
    		});
        },
        focus: function( event, ui ) {
          	return false;
        },
        select: function( event, ui ) {
        	$("#codeNumber").val(ui.item.codeNumber);
        	$("#prevCodeNo").val(ui.item.codeNumber);
        	$("#stockId").val(ui.item.stockId);
        	$("#prevAvgPrice").val(ui.item.averagePrice);//to calculate avgPrice for existing stock data
    		$("#categoryId").val(ui.item.categoryId);  		
    		$("#stockName").val(ui.item.stockName);
    		$("#categoryName").val(ui.item.categoryName);
    		$("#subCategory").val(ui.item.subCategory);
    		$("#brandName").val(ui.item.brandName);
    		$("#color").val(ui.item.color);
    		$("#purchasePrice").val(ui.item.purchasePrice);
    		$("#averagePrice").val(ui.item.averagePrice);
    		$("#sellingPrice1").val(ui.item.sellingPrice1);
    		$("#sellingPrice2").val(ui.item.sellingPrice2);
    		$("#sellingPrice3").val(ui.item.sellingPrice3);
    		$("#sellingPrice4").val(ui.item.sellingPrice4);
    		$("#description").val(ui.item.description);
    		$("#quantityLarge").val(ui.item.quantityLarge);
    		$("#unitMeasurement").val(ui.item.unitMeasurement);
    		$("#imageByte").val(ui.item.imageByte); //image
    		$("#imageString").val(ui.item.imageString); //image
    		$("#currencyName").val(ui.item.currencyName);
    		$("#packingMedium").val(ui.item.packingMedium);
    		$("#packingSmall").val(ui.item.packingSmall);
    		$("#totalQty").val(ui.item.totalQty);
          	return false;
        }
      });
	
	$("#save").click(function(e){
		e.preventDefault();	
		checkValid();
		if(errors==0){	
		//	$("#stockForm").attr("action","registerProducts");
			$("#purchaseForm").submit(); 
		}
	});
	
	function checkValid(){
		$("#purchaseForm .error").text('');
		errors=0;
		var codeNumberErr=checkField("Code Number ",$("#codeNumber").val(),true,null,255, null);
		var stockIdErr=checkField("Stock Id ",$("#stockId").val(),true,0,50, "n");
		var purchasePriceErr=checkField("Purchase Price ",$("#purchasePrice").val(),true,0,50, "n");		
		var quantityLargeErr=checkField("Quantity Large ",$("#quantityLarge").val(),true,0,50, "n");
		var quantityMediumErr=checkField("Quantity Medium ",$("#quantityMedium").val(),true,0,50, "n");
		var quantitySmallErr=checkField("Quantity Small ",$("#quantitySmall").val(),true,0,50, "n");
		var purchaseDateStrErr=checkField("Purchase Date ",$("#purchaseDateStr").val(),true,0,50, null);
		
		if(codeNumberErr){
			$("#codeNumberErrMsg").text(codeNumberErr);
			errors=1;
		}
		if(stockIdErr){
			$("#stockIdErrMsg").text("Please choose existing stock by typing code number.");
			errors=1;
		}
		if(purchasePriceErr){
			$("#purchasePriceErrMsg").text(purchasePriceErr);
			errors=1;
		}
		if(purchaseDateStrErr){
			$("#purchaseDateStrErrMsg").text(purchaseDateStrErr);
			errors=1;
		}
		if(quantityLargeErr){
			$("#quantityLargeErrMsg").text(quantityLargeErr);
			errors=1;
		}		
		if(quantityMediumErr){
			$("#quantityMediumErrMsg").text(quantityMediumErr);
			errors=1;
		}
		if(quantitySmallErr){
			$("#quantitySmallErrMsg").text(quantitySmallErr);
			errors=1;
		}
	}
	
});