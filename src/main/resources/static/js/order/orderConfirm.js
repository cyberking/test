$(document).ready(function() {
	
	showMessage();
	function showMessage(){
		var errMsg = $("#errorMsg").val();
		var sucMsg = $("#successMsg").val();
		if(errMsg != null && errMsg.length > 0)
			swal("Order Confirm", errMsg , "error");
		else if(sucMsg != null && sucMsg.length > 0)
			swal("Order Confirm", sucMsg , "success");
	}

	$("#save").click(function(e){
		e.preventDefault();	
		checkValid();
		if(errors==0){	
			$("#orderRecConfirmFormId").submit(); 
		}
	});
	
	$("#confirm").click(function(e){
		e.preventDefault();
		var confirmOk = $("#confirmOk").val();
		var orderId = $("#orderId").val();
		if(confirmOk == 0)
			window.location.href = "viewInvoiceOrderList?id="+orderId;
		else
			swal("Order Confirm", "Remaining Stock Balance is not enough. Please check each order delivery quantity." , "error");
		
	});
	
	$(".update_link").click(function(e){
		e.preventDefault();
		var row = $(this).closest('tr');
		var orderRecordId = row.find("#orderRecordId").val();
		var orderId = row.find("#oId").val();
		var stockId = row.find("#stkId").val();
		var totalStockBalance = row.find(".cls-totalQty").text();
		var codeNo = row.find(".cls-codeNo").text();
		var stkName = row.find(".cls-stkName").text();
		var catName = row.find(".cls-catName").text();
		var subCat = row.find(".cls-subCat").text();
		var brandName = row.find(".cls-brandName").text();
		var color = row.find(".cls-color").text();
		var qtyL = row.find(".cls-qtyL").text();
		var qtyM = row.find(".cls-qtyM").text();
		var qtyS = row.find(".cls-qtyS").text();
		var packingL = row.find(".cls-packingL").text();
		var packingM = row.find(".cls-packingM").text();
		var orderStaus = row.find(".cls-orderStaus").text();
		var deliQtyL = row.find(".cls-deli-qtyL").text();
		var deliQtyM = row.find(".cls-deli-qtyM").text();
		var deliQtyS = row.find(".cls-deli-qtyS").text();
		var sellingPrice = row.find(".cls-sellingPrice").text();
		var disAmt = row.find(".cls-disAmt").text();
		
		//alert("row[] "+ row + " orderRecordId "+orderRecordId);	
		
		$("#id").val(orderRecordId);
		$("#orderId").val(orderId);
		$("#stockId").val(stockId);
		$("#codeNumber").val(codeNo);
		$("#stockName").val(stkName);
		$("#categoryName").val(catName);
		$("#subCategory").val(subCat);
		$("#brandName").val(brandName);
		$("#color").val(color);
		$("#qtyL").val(qtyL);
		$("#qtyM").val(qtyM);
		$("#qtyS").val(qtyS);		
		$("#deliveryQtyL").val(orderStaus);
		$("#sellingPrice").val(sellingPrice);
		$("#orderRecDiscAmt").val(disAmt);
		$("#deliveryQtyL").val(deliQtyL);
		$("#deliveryQtyM").val(deliQtyM);
		$("#deliveryQtyS").val(deliQtyS);
		$("#totalStockBalance").val(totalStockBalance);
	});
	
	function checkValid(){
		$("#orderRecConfirmFormId .error").text('');
		errors=0;
		var idErr=checkField("Stock Id ",$("#stockId").val(),true,0,50, "n");
		var sellingPriceErr=checkField("Selling Price ",$("#sellingPrice").val(),true,0,50, "n");		
		var deliveryQtyLErr=checkField("Delivery Quantity Large ",$("#deliveryQtyL").val(),true,0,50, "n");
		var deliveryQtyMErr=checkField("Delivery Quantity Medium ",$("#deliveryQtyM").val(),true,0,50, "n");
		var deliveryQtySErr=checkField("Delivery Quantity Small ",$("#deliveryQtyS").val(),true,0,50, "n");
		
		if(idErr){
			$("#idErrMsg").text("Please choose one order record to update.");
			errors=1;
		}
		if(sellingPriceErr){
			$("#sellingPriceErrMsg").text(sellingPriceErr);
			errors=1;
		}
		if(deliveryQtyLErr){
			$("#deliveryQtyLErrMsg").text(deliveryQtyLErr);
			errors=1;
		}
		if(deliveryQtyMErr){
			$("#deliveryQtyMErrMsg").text(deliveryQtyMErr);
			errors=1;
		}
		if(deliveryQtySErr){
			$("#deliveryQtySErrMsg").text(deliveryQtySErr);
			errors=1;
		}		
		
	}

});