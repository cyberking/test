$(document).ready(function() {
	
	showMessage();
	function showMessage(){
		var errMsg = $("#errorMsg").val();
		var sucMsg = $("#successMsg").val();
		if(errMsg != null && errMsg.length > 0)
			swal("Order Confirm", errMsg , "error");
		else if(sucMsg != null && sucMsg.length > 0)
		{
			//swal("Order Confirm", sucMsg , "success");
			window.location.href = "viewInvoiceOrderList?id="+$("#orderId").val()+"&msg="+sucMsg;
		}
	}
	
	showInvoiceConfirmOKMsg();
	
	function showInvoiceConfirmOKMsg(){
		var invMsg = $("#invConfirmOKMsg").val();
		if(invMsg != null && invMsg.length > 0)
			swal("Order Confirm", invMsg , "success");
	}
	
	$("#discAmt").change(function(){
	   var totalSellingPrice = $("#sellingPrice").val();
	   totalSellingPrice = parseFloat(totalSellingPrice);
	   var discAmt = $("#discAmt").val();
	   if(discAmt != null && discAmt != "")
	   {
		   discAmt =  parseFloat(discAmt);
		   var totalAmt = totalSellingPrice - discAmt;
		   $("#totalAmt").val(totalAmt);
	   }
	   else
		   $("#totalAmt").val(totalSellingPrice);
	});
	
	$('input#deliveryDate').datepicker({
		dateFormat: 'dd/mm/yy',
		setDate: new Date()
	});
	$("#orderConfirm").click(function(e){
		e.preventDefault();	
		checkValid();
		if(errors==0){	
		
			$("#orderFormId").submit(); 
		}
	});

	function checkValid(){
		$("#orderFormId.error").text('');
		errors=0;
		var deliveryDateErr=checkField("Delivery Date ",$("#deliveryDate").val(),true,0,50, null);
		console.log("deliveryDateErr"+deliveryDateErr);
		if(deliveryDateErr){
			$("#deliveryDateErrMsg").text(deliveryDateErr);
			errors=1;
		}
	}
});