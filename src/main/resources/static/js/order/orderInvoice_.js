$(document).ready(function() {
	
	showMessage();
	function showMessage(){
		var errMsg = $("#errorMsg").val();
		var sucMsg = $("#successMsg").val();
		if(errMsg != null && errMsg.length > 0)
			swal("Order Confirm", errMsg , "error");
		else if(sucMsg != null && sucMsg.length > 0)
		{
			//swal("Order Confirm", sucMsg , "success");
			window.location.href = "viewInvoiceOrderList?id="+$("#orderId").val()+"&msg="+sucMsg;
		}
	}
	
	showInvoiceConfirmOKMsg();
	
	function showInvoiceConfirmOKMsg(){
		var invMsg = $("#invConfirmOKMsg").val();
		if(invMsg != null && invMsg.length > 0)
			swal("Order Confirm", invMsg , "success");
	}
	
	$("#discAmt").change(function(){
	   var totalSellingPrice = $("#sellingPrice").val();
	   totalSellingPrice = parseFloat(totalSellingPrice);
	   var discAmt = $("#discAmt").val();
	   if(discAmt != null && discAmt != "")
	   {
		   discAmt =  parseFloat(discAmt);
		   var totalAmt = totalSellingPrice - discAmt;
		   $("#totalAmt").val(totalAmt);
	   }
	   else
		   $("#totalAmt").val(totalSellingPrice);
	});

});