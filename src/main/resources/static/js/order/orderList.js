$(document).ready(function() {
	
	$('#orderListTable').dataTable( {
    "aoColumnDefs" : [ {
        "bSortable" : false,
        "aTargets" : [ 'no-sort' ]
    	} ]
	} );
	
	$(".no-sort1").removeClass("sorting_asc");
	
	showMessage();
	function showMessage(){
		var errMsg = $("#errorMsg").val();
		var sucMsg = $("#successMsg").val();
		if(errMsg != null && errMsg.length > 0)
			swal("Order Cancel", errMsg , "error");
		else if(sucMsg != null && sucMsg.length > 0)
			swal("Order Cancel", sucMsg , "success");
	}
	
	$(".delete_link").click(function(e){
		e.preventDefault();
		var row = $(this).closest('tr');
		var deleteId = row.find("#orderId").val();
		swal({
			title : "Cancel Confirmation",
			text : "Are you sure to cancel this order?",
			showCancelButton: true,
			confirmButtonText: "Confirm"
		}, function() {
			window.location.href = "/orderCancel?id=" + deleteId;
		});
	});
	
});