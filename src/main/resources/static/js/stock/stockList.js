$(document).ready(function() {		 

	//$("#stockListTable").dataTable();
	
//	$("#stockListTable").find("thead th").removeClass("sorting");
	$('#stockListTable').dataTable( {
        /*"columnDefs": [ {
          "targets": 'no-sort',
          "orderable": false,
    } ],*/
    "aoColumnDefs" : [ {
        "bSortable" : false,
        "aTargets" : [ 'no-sort' ]
    } ]
} );
	
	$(".no-sort1").removeClass("sorting_asc");
	showMessage();
	function showMessage(){
		var errMsg = $("#errorMsg").val();
		var sucMsg = $("#successMsg").val();
		if(errMsg != null && errMsg.length > 0)
			swal("Delete Stock", errMsg , "error");
		else if(sucMsg != null && sucMsg.length > 0)
			swal("Delete Stock", sucMsg , "success");
	}
	
	$(".delete_link").click(function(e){
		e.preventDefault();
		var row = $(this).closest('tr');
		var deleteId = row.find(".stockId").val();
		swal({
			title : "Delete Confirmation",
			text : "Are you sure to delete?",
			showCancelButton: true,
			confirmButtonText: "Confirm"
		}, function() {
			window.location.href = "/stockDelete?id=" + deleteId;
			//$(".result-table tr#"+stockId).remove();
		});
	});
	
});