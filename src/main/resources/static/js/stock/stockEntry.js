$(document).ready(function() {		 
		
	var url = APP_CONTEXT;
	var errors = 0;
	
	$("#saleEntry").dataTable();
	
	$("#reset").click(function(e){
		e.preventDefault();
		errors= 0;
		$("#stockForm .error").text('');
		$("#stockForm input[type=text]").val('');
		$("#stockForm input[type=file]").val('');
		$("#stockForm select").val(1);
		$("#id").val(0);
		$("#prevAvgPrice").val(0);
		$("#stockId").val(0);
		$("#prevCodeNo").val('');
	});
	
	$("#codeNumber").change(function(){
		var prevCode = $("#prevCodeNo").val();
		if(prevCode!=null && prevCode!="" && typeof prevCode != "undefined"){
			var curCode = $("#codeNumber").val();
			if(curCode != prevCode)//if(!curCode.equals(prevCode))
				$("#prevAvgPrice").val(0);
		}
		
	});
	
	$("#currencyId").change(function(){
		if($("#currencyId").val() == 2)
			$(".exchangeRateDiv").show();
		else
			$(".exchangeRateDiv").hide();
	});
	
	$("#purchasePrice").change(function(){
		showAveragePrice();
	});
	
	function showAveragePrice(){
		var sId = $("#stockId").val();
		var purPrice = $("#purchasePrice").val();
		if(purPrice==null || purPrice=="" || typeof purPrice == "undefined" || !purPrice.trim()){				
			$("#purchasePriceErrMsg").text("Purchase Price is required.");
			errors=1;
		}
		if(!$.isNumeric(purPrice)){
			$("#purchasePriceErrMsg").text("Allow only number.");
			errors=1;
		}
		if(purPrice < 0)
		{
			$("#purchasePriceErrMsg").text("Value must greater than 0.");
			errors=1;
		}
		if(errors == 0){
			purPrice = parseFloat(purPrice);
			var avgPrice = $("#prevAvgPrice").val();
			avgPrice = parseFloat(avgPrice);
			/*$("#averagePrice").val(purPrice);
			if(sId != null && sId > 0 && prevAvgPrice > 0)
			{
				var avgNewPrice = (avgPrice+purPrice)/2;
				$("#averagePrice").val(avgNewPrice);
				$("#averagePrice").val((avgPrice+purPrice)/2);
			}*/
			if(sId == null || sId <= 0)
				 $("#averagePrice").val(purPrice);
			else
			{
				if(prevAvgPrice > 0)
				{
					var newAvgPrice = (avgPrice+purPrice)/2;
					$("#averagePrice").val(newAvgPrice);
				}
				
			}
		}
			
	}
	
	$(".delete_link").click(function(e){
		e.preventDefault();
		var deleteId = $("#stockId").val();
		swal({
			title : "Delete Confirmation",
			text : "Are you sure to delete?",
			showCancelButton: true,
			confirmButtonText: "Confirm"
		}, function() {
			window.location.href = "/stock/delete/" + deleteId;
		});
	});
	
	$(".update_link").click(function(e){
		e.preventDefault();
		var id = $("#pId").val();
		var datas = $(this).closest("tr").find(".sData");// Finds all children <td> elements that has class - ids
		var ids = $(this).closest("tr").find(".ids");// Finds all children <td> elements that has class - ids
		var idsArray = new Array();
		var dataArray = new Array();
		$.each(ids,function(){
			idsArray.push($(this).val());
		});
		$.each(datas,function(){
			dataArray.push($(this).context.innerHTML);
		});
		//alert("data[] "+dataArray + " id[] "+idsArray);
		$("#id").val(id);
		$("#categoryId").val(idsArray[0]);
		$("#currencyId").val(idsArray[1]);
		$("#imageByte").val(idsArray[2]); //image
		$("#imageString").val(idsArray[3]); //image
		
		$("#codeNumber").val(dataArray[0]);
		$("#productName").val(dataArray[1]);
		$("#categoryName").val(dataArray[2]);
		$("#subCategory").val(dataArray[3]);
		$("#brandName").val(dataArray[4]);
		$("#color").val(dataArray[5]);
		$("#purchasePrice").val(dataArray[6]);
		$("#averagePrice").val(dataArray[7]);
		$("#sellingPrice1").val(dataArray[8]);
		$("#sellingPrice2").val(dataArray[9]);
		$("#sellingPrice3").val(dataArray[10]);
		$("#sellingPrice4").val(dataArray[11]);
		$("#description").val(dataArray[12]);
		$("#quantityLarge").val(dataArray[13]);
		/*$("#quantityMedium").val(dataArray[14]);
		$("#quantitySmall").val(dataArray[15]);*/
		$("#unitMeasurement").val(dataArray[16]);
	//	$("#cashAccountId").val(dataArray[17]); image
		$("#currencyName").val(dataArray[18]);		
		$("#packingMedium").val($("#packingM").val());
		$("#packingSmall").val($("#packingS").val());
	});
	
	$("#removeStock").click(function(e){
		e.preventDefault();
		var row = btn.parentNode.parentNode;
		row.parentNode.removeChild(row);
	});

	$( "#codeNumber" ).autocomplete({
		minLength: 2,
        source: function (request, response) {
        	$.ajax({
    			type	:	"GET",
    			url		:	"searchStockByCode.html?code="+$.trim($('#codeNumber').val()),
    			dataType:	'json',
    			success :	function (data) {
    				if(data.length <= 0){
    					$("#stockId").val(0);
    		        	$("#averagePrice").val($("#purchasePrice").val());//prevent not to calculate averagePrice when previous autocomplete data is existing
    				}
    				response(
    						$.map(data, function( value, key ){
    							return {
    								label: value.codeNumber+"("+value.productName+"-"+value.brandName+")",
    								codeNumber: value.codeNumber,
    								productName: value.productName,
    								brandName: value.brandName,
    								id: value.id,    								
    								categoryId: value.categoryId,
    								currencyId: value.currencyId,
    								subCategory: value.subCategory,
    								categoryName: value.categoryName,    								
    								color: value.color,
    								purchasePrice: value.purchasePrice,
    								averagePrice: value.averagePrice,
    								sellingPrice1: value.sellingPrice1,
    								sellingPrice2: value.sellingPrice2,
    								sellingPrice3: value.sellingPrice3,
    								sellingPrice4: value.sellingPrice4,
    								description: value.description,
    								quantityLarge: value.quantityLarge,    								
    								unitMeasurement: value.unitMeasurement,
    								packingMedium: value.packingMedium,
    								packingSmall: value.packingSmall,
    								currencyName: value.currencyName,
    								imageByte: value.imageByte,
    								imageString: value.imageString
    							};
    						})
    					);
                }
    		});
        },
        focus: function( event, ui ) {
          	return false;
        },
        select: function( event, ui ) {
        	$("#codeNumber").val(ui.item.codeNumber);
        	$("#prevCodeNo").val(ui.item.codeNumber);
        	$("#stockId").val(ui.item.id);
        	$("#prevAvgPrice").val(ui.item.averagePrice);//to calculate avgPrice for existing stock data
    		$("#categoryId").val(ui.item.categoryId);
    		$("#currencyId").val(ui.item.currencyId);    		
    		$("#productName").val(ui.item.productName);
    		$("#categoryName").val(ui.item.categoryName);
    		$("#subCategory").val(ui.item.subCategory);
    		$("#brandName").val(ui.item.brandName);
    		$("#color").val(ui.item.color);
    		$("#purchasePrice").val(ui.item.purchasePrice);
    		$("#averagePrice").val(ui.item.averagePrice);
    		$("#sellingPrice1").val(ui.item.sellingPrice1);
    		$("#sellingPrice2").val(ui.item.sellingPrice2);
    		$("#sellingPrice3").val(ui.item.sellingPrice3);
    		$("#sellingPrice4").val(ui.item.sellingPrice4);
    		$("#description").val(ui.item.description);
    		$("#quantityLarge").val(ui.item.quantityLarge);
    		$("#unitMeasurement").val(ui.item.unitMeasurement);
    		$("#imageByte").val(ui.item.imageByte); //image
    		$("#imageString").val(ui.item.imageString); //image
    		$("#currencyName").val(ui.item.currencyName);
    		
    		$("#packingMedium").val($("#packingM").val());
    		$("#packingSmall").val($("#packingS").val());
          	return false;
        }
      });
	
	$("#register").click(function(e){
		e.preventDefault();	
		checkValid();
		if(errors==0){	
		//	$("#stockForm").attr("action","registerProducts");
			$("#stockForm").submit(); 
		}
	});
	
	function checkValid(){
		$("#stockForm .error").text('');
		errors=0;
		var codeNumberErr=checkField("Code number ",$("#codeNumber").val(),true,null,255, null);
		var productNameErr=checkField("Product name ",$("#productName").val(),true,null,255, null);
		var subCategoryErr=checkField("Sub category ",$("#subCategory").val(),true,null,255,null);
		var brandNameErr=checkField("Selling Price ",$("#brandName").val(),true,null,255, null);
		var colorErr=checkField("Color ",$("#color").val(),true,null,255, null);
		var unitMeasurementErr=checkField("Unit Measurement ",$("#unitMeasurement").val(),true,null,255, null);
		var stockImageErr=checkField("Image ",$("#stockImage").val(),true,null,null, null);
		
		var sellingPrice1Err=checkField("Selling Price1 ",$("#sellingPrice1").val(),true,0,50, "n");
		var sellingPrice2Err=checkField("Selling Price2 ",$("#sellingPrice2").val(),true,0,50, "n");
		var sellingPrice3Err=checkField("Selling Price3 ",$("#sellingPrice3").val(),true,0,50, "n");
		var sellingPrice4Err=checkField("Selling Price4 ",$("#sellingPrice4").val(),true,0,50, "n");
		var purchasePriceErr=checkField("Purchase Price ",$("#purchasePrice").val(),true,0,50, "n");
		var averagePriceErr=checkField("Average Price ",$("#averagePrice").val(),true,0,50, "n");
		var quantityLargeErr=checkField("Quantity Large ",$("#quantityLarge").val(),true,0,50, "n");
		var packingMediumErr=checkField("Packing Medium ",$("#packingMedium").val(),true,0,50, "n");
		var packingSmallErr=checkField("Packing Small ",$("#packingSmall").val(),true,0,50, "n");
		
		if(codeNumberErr){
			$("#codeNumberErrMsg").text(codeNumberErr);
			errors=1;
		}
		if(productNameErr){
			$("#productNameErrMsg").text(productNameErr);
			errors=1;
		}
		if(subCategoryErr){
			$("#subCategoryErrMsg").text(subCategoryErr);
			errors=1;
		}		
		if(brandNameErr){
			$("#brandNameErrMsg").text(brandNameErr);
			errors=1;
		}
		if(colorErr){
			$("#colorErrMsg").text(colorErr);
			errors=1;
		}
		if(unitMeasurementErr){
			$("#unitMeasurementErrMsg").text(unitMeasurementErr);
			errors=1;
		}
		if(stockImageErr){
			$("#stockImageErrMsg").text(stockImageErr);
			errors=1;
		}	
		else {
			var sIerror = isValidImage($("#stockImage").val());
			if(!sIerror){
				$("#stockImageErrMsg").text("Image type is not valid.");
				errors=1;
			}
		}
		
		if(sellingPrice1Err){
			$("#sellingPrice1ErrMsg").text(sellingPrice1Err);
			errors=1;
		}
		if(sellingPrice2Err){
			$("#sellingPrice2ErrMsg").text(sellingPrice2Err);
			errors=1;
		}
		if(sellingPrice3Err){
			$("#sellingPrice3ErrMsg").text(sellingPrice3Err);
			errors=1;
		}		
		if(sellingPrice4Err){
			$("#sellingPrice4ErrMsg").text(sellingPrice4Err);
			errors=1;
		}
		if(purchasePriceErr){
			$("#purchasePriceErrMsg").text(purchasePriceErr);
			errors=1;
		}
		if(averagePriceErr){
			$("#averagePriceErrMsg").text(averagePriceErr);
			errors=1;
		}
		if(quantityLargeErr){
			$("#quantityLargeErrMsg").text(quantityLargeErr);
			errors=1;
		}		
		if(packingMediumErr){
			$("#packingMediumErrMsg").text(packingMediumErr);
			errors=1;
		}
		if(packingSmallErr){
			$("#packingSmallErrMsg").text(packingSmallErr);
			errors=1;
		}
	}
	
});