$(document).ready(function() {		 
		
	var url = APP_CONTEXT;
	var errors = 0;
	
	$("#reset").click(function(e){
		e.preventDefault();
		errors= 0;
		$("#stkForm .error").text('');
		$("#stkForm input[type=text]").val('');
		$("#stkForm input[type=file]").val('');
		$("#stkForm select").val(1);
		$("#stkForm input[type=hidden]").val('');
		$("#id").val(0);
	});
	
	showMessage();
	
	function showMessage(){
		var errMsg = $("#errorMsg").val();
		var sucMsg = $("#successMsg").val();
		if(errMsg != null && errMsg.length > 0)
			swal("Stock Register", errMsg , "error");
		else if(sucMsg != null && sucMsg.length > 0)
			swal("Stock Register", sucMsg , "success");
	}
	
	$("#register").click(function(e){
		e.preventDefault();	
		checkValid();
		if(errors==0){
			$("#stkForm").attr("method","post"); 
			$("#stkForm").submit(); 
		}
	});
	
	function checkValid(){
		$("#stkForm .error").text('');
		errors=0;
		var codeNumberErr=checkField("Code Number ",$("#codeNumber").val(),true,null,255, null);
		var stockNameErr=checkField("Stock Name ",$("#stockName").val(),true,null,255, null);
		var subCategoryErr=checkField("Sub Category ",$("#subCategory").val(),true,null,255,null);
		var brandNameErr=checkField("Brand Name ",$("#brandName").val(),true,null,255, null);
		var colorErr=checkField("Color ",$("#color").val(),true,null,255, null);
		var unitMeasurementErr=checkField("Unit Measurement ",$("#unitMeasurement").val(),true,null,255, null);
		var stockImageErr=checkField("Image ",$("#stockImage").val(),true,null,null, null);		
		var sellingPrice1Err=checkField("Selling Price1 ",$("#sellingPrice1").val(),true,0,50, "n");
		var sellingPrice2Err=checkField("Selling Price2 ",$("#sellingPrice2").val(),true,0,50, "n");
		var sellingPrice3Err=checkField("Selling Price3 ",$("#sellingPrice3").val(),true,0,50, "n");
		var sellingPrice4Err=checkField("Selling Price4 ",$("#sellingPrice4").val(),true,0,50, "n");
		var averagePriceErr=checkField("Average Price ",$("#averagePrice").val(),true,0,50, "n");
		var packingMediumErr=checkField("Package (packing per box)",$("#packingMedium").val(),true,0,50, "n");
		var packingSmallErr=checkField("Pieces (pieces per package)",$("#packingSmall").val(),true,0,50, "n");
		
		var categoryIdErr=checkField("Category",$("#categoryId").val(),false,null,null, "s");
		//var currencyIdErr=checkField("Currency",$("#currencyId").val(),false,null,null, "s");
		if(categoryIdErr){
			$("#categoryIdErrMsg").text(categoryIdErr);
			errors=1;
		}
		/*if(currencyIdErr){
			$("#currencyIdErrMsg").text(currencyIdErr);
			errors=1;
		}*/
		
		if(codeNumberErr){
			$("#codeNumberErrMsg").text(codeNumberErr);
			errors=1;
		}
		if(stockNameErr){
			$("#stockNameErrMsg").text(stockNameErr);
			errors=1;
		}
		if(subCategoryErr){
			$("#subCategoryErrMsg").text(subCategoryErr);
			errors=1;
		}		
		if(brandNameErr){
			$("#brandNameErrMsg").text(brandNameErr);
			errors=1;
		}
		if(colorErr){
			$("#colorErrMsg").text(colorErr);
			errors=1;
		}
		if(unitMeasurementErr){
			$("#unitMeasurementErrMsg").text(unitMeasurementErr);
			errors=1;
		}
		if(stockImageErr){
			$("#stockImageErrMsg").text(stockImageErr);
			errors=1;
		}	
		else {
			var sIerror = isValidImage($("#stockImage").val());
			if(!sIerror){
				$("#stockImageErrMsg").text("Image type is not valid.");
				errors=1;
			}
		}
		
		if(sellingPrice1Err){
			$("#sellingPrice1ErrMsg").text(sellingPrice1Err);
			errors=1;
		}
		if(sellingPrice2Err){
			$("#sellingPrice2ErrMsg").text(sellingPrice2Err);
			errors=1;
		}
		if(sellingPrice3Err){
			$("#sellingPrice3ErrMsg").text(sellingPrice3Err);
			errors=1;
		}		
		if(sellingPrice4Err){
			$("#sellingPrice4ErrMsg").text(sellingPrice4Err);
			errors=1;
		}	
		if(packingMediumErr){
			$("#packingMediumErrMsg").text(packingMediumErr);
			errors=1;
		}
		if(packingSmallErr){
			$("#packingSmallErrMsg").text(packingSmallErr);
			errors=1;
		}
	}
	
});