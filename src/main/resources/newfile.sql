
/* Drop Tables */

DROP TABLE IF EXISTS new_table;
DROP TABLE IF EXISTS test;




/* Create Tables */

CREATE TABLE new_table
(
	id bigint NOT NULL UNIQUE
) WITHOUT OIDS;


CREATE TABLE test
(
	id bigserial NOT NULL UNIQUE,
	PRIMARY KEY (id)
) WITHOUT OIDS;



/* Create Foreign Keys */

ALTER TABLE new_table
	ADD FOREIGN KEY (id)
	REFERENCES test (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;



