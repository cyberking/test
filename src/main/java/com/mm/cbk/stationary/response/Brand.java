package com.mm.cbk.stationary.response;

import java.util.List;

import lombok.Data;

@Data
public class Brand {
	String brandName;
	List<CategoryResponse> categoryList;
}
