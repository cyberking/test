package com.mm.cbk.stationary.response;

import java.util.List;

import com.mm.cbk.stationary.form.Customer;
import com.mm.cbk.stationary.form.OrderForm;
import com.mm.cbk.stationary.form.ProductRequestForm;
import com.mm.cbk.stationary.form.Stock;
import com.mm.cbk.stationary.form.UnitMeasurement;

import lombok.Data;

@Data
public class Response {

//	String roleId;
//	String roleName;
//	List<Product> productList;
//	List<ProductRequestForm> productRequestList;
//	List<Customer> customerList;
//	List<Brand> brandList;
//	List<Brand> brandNameList;
	
	String roleId;
	String roleName;
	List<Stock> productList;
	List<ProductRequestForm> productRequestList;
	List<Customer> customerList;
	List<Brand> brandList;
	List<Brand> brandNameList;
	List<String> categoryList;
	String customerLevel;
	List<UnitMeasurement> unitList;
	List<OrderForm> orderFormList;
}
