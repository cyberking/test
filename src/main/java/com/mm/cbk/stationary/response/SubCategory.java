package com.mm.cbk.stationary.response;

import java.util.List;

import com.mm.cbk.stationary.form.StockForm;

import lombok.Data;

@Data
public class SubCategory {
	String subCategoryName;
	List<StockForm> stockFormList;
}
