package com.mm.cbk.stationary.response;

import java.util.List;

import lombok.Data;

@Data
public class Category {
	String categoryName;
	List<SubCategory> subCategoryList;
}
