package com.mm.cbk.stationary.response;

import lombok.Data;

@Data
public class ResponseForm {
	Meta meta;
	Response response;
}
