package com.mm.cbk.stationary.response;

import lombok.Data;

@Data
public class Meta {

	String responseCode;
	String responseMessage;
}
