package com.mm.cbk.stationary.repository;

import java.util.List;

import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mm.cbk.stationary.form.UnitMeasurement;

@PersistenceContext
@Transactional
@Repository
public interface UnitMeasurementRepository extends JpaRepository<UnitMeasurement, Integer> {

	@Query(nativeQuery = true, value = "select * from unit_measurement")
	List<UnitMeasurement> getAllUnitMeasurement();
}
