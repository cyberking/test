package com.mm.cbk.stationary.repository;

import java.util.List;

import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mm.cbk.stationary.form.CustomerLevel;

@Repository
@Transactional
@PersistenceContext
public interface CustomerLevelRepository extends JpaRepository<CustomerLevel, Integer> {
	
	@Query(nativeQuery = true, value = "select * from customer_level")
	List<CustomerLevel> getCustomerLevelList();
	
	
}
