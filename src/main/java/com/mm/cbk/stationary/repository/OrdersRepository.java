package com.mm.cbk.stationary.repository;

import java.util.Date;
import java.util.List;

import javax.persistence.PersistenceContext;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mm.cbk.stationary.form.Orders;

@PersistenceContext
@Repository
@Transactional
public interface OrdersRepository extends JpaRepository<Orders, String> {

	@Query(nativeQuery = true, value = "select * from orders where id = ?1")
	Orders getOrderById(Integer orderId);

	@Query(nativeQuery = true, value = "select * from orders order by order_date asc")
	List<Orders> getAllOrderList();

	@Query(nativeQuery = true, value = "select * from orders where order_status != ?1 order by order_date asc")
	List<Orders> getOrderListByNotTargetStatus(Integer orderStatus);

	@Modifying
	@Query(nativeQuery = true, value = "update orders set order_status = ?1, discount_amount = ?2, selling_price = ?4, delivery_date = ?5 where id = ?3")
	void updateOrderStatusAndDiscount(Integer code, Double discAmt, Integer orderId, Double sellingAmt,
			Date deliveryDate);

	@Modifying
	@Query(nativeQuery = true, value = "update orders set order_status = ?2 where id = ?1")
	void cancelOrderStatus(Integer id, Integer code);

	@Modifying
	@Query(nativeQuery = true, value = "insert into orders (delivery_address,order_date,order_status,customer_id,user_id) values (:location,now(),0,:customerId,:userId)")
	void insertOrders(@Param("location") String deliAddress, @Param("customerId") int customerId,
			@Param("userId") int userId);

	// @Query(nativeQuery= true, value ="select COUNT(orders.id) AS orders,
	// order_record.*, orders.* from orders LEFT JOIN order_record on
	// order_record.order_id = orders.id group by order_record.id, orders.id
	// order by orders.id asc")
	// List<Orders> getOrderHistoryWithOrderRecord();

	// @Query(nativeQuery = true, value = "select * from orders inner join
	// order_record on order_record.order_id = orders.id")
	// List<Orders> getOrderHistoryWithOrderRecord();

	// @Query(nativeQuery = true, value = "select orders,order_record from
	// orders inner join order_record on order_record.order_id = orders.id")
	// List<Orders> getOrderHistoryWithOrderRecord();

	// @Query("select com.mm.cbk.stationary.form.Orders from Orders inner join
	// OrderRecord on OrderRecord.order.id = Orders.id")
	// List<Orders> getOrderHistoryWithOrderRecord();

	@Query(nativeQuery = true, value = "select * from orders where customer_id = :customerId order by orders.id asc")
	List<Orders> getOrdersList(@Param("customerId") int customerId);

}
