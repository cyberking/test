package com.mm.cbk.stationary.repository;

import java.util.List;

import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mm.cbk.stationary.form.Product;

@PersistenceContext
@Transactional
@Repository
public interface ProductRepository extends JpaRepository<Product, String> {

	/* @Modifying
	 @Query(nativeQuery = true, value = "insert into product (product_name,
	 average_price, category, brand, quality, quantity, description, code,
	 image_str, color) values (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10)")
	 int insertProduct(String productName, double price, String category,
	 String brandName, String quality, int quantity,
	 String description, String codeNumber, String imageString, String color)*/;

	@Modifying
	@Query(nativeQuery = true, value = "insert into product(average_price, purchase_price, brand, code, color, description, exchange_rate, image_byte_array, image_str, product_name, quantity_large, quantity_medium, quantity_small, packing_medium, packing_small, reorder_level, price1, price2, price3, price4, sub_category, unit_measurement, category_id, currency_id) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13, ?14, ?15, ?16, ?17, ?18, ?19, ?20, ?21, ?22, ?23, ?24)")
	void insertProduct(double avgPrice, double bal, String brand, String code, String color, String desc, int exchangeRate, byte[] img,  String imgStr, String productName, int qtyL, int qtyM, int qtyS, int packM, int packS, String reorderLevel, double p1, double p2, double p3, double p4, String subCategory, String unitMeasurement, int catId, int curId);
	
	@Modifying
	@Query(nativeQuery = true, value = "update product set average_price = ?2, purchase_price = ?3, brand = ?4, code = ?5, color = ?6, description = ?7, exchange_rate = ?8, image_byte_array = ?9, image_str  = ?10, product_name = ?11, quantity_large = ?12, quantity_medium  = ?13, quantity_small = ?14, packing_medium = ?15, packing_small = ?16, reorder_level = ?17, price1 = ?18, price2 = ?19, price3 = ?20, price4 = ?21, sub_category = ?22, unit_measurement = ?23, category_id = ?24, currency_id = ?25 where product_id = ?1")
	void updateProduct(int id, double averagePrice, double purchasePrice, String brandName, String codeNumber,
			String color, String description, int exchangeRate, byte[] imageByte, String imageString,
			String productName, int quantityLarge, int quantityMedium, int quantitySmall, int packingMedium,
			int packingSmall, String reorderLevel, double sellingPrice1, double sellingPrice2, double sellingPrice3,
			double sellingPrice4, String subCategory, String unitMeasurement, int categoryId, int currencyId);
	
	@Query(nativeQuery = true, value = "select * from product order by code asc")
	List<Product> getProductList();

	@Query(nativeQuery = true, value = "select * from product where product_id = :productId")
	Product getProduct(@Param("productId") int productId);

	@Modifying
	@Query(nativeQuery = true, value = "delete from product where product_id = :productId")
	void deleteProduct(@Param("productId") int productId);
	
	@Query(nativeQuery = true, value= "select * from product where code like concat('%',:code,'%') ")
	List<Product> searchStockByCode(@Param("code") String codeNo);
	
	@Query(nativeQuery = true, value = "select * from product where code = :code")
	Product getProductByCodeNumber(@Param("code") String codeNumber);

	// @Modifying
	// @Transactional
	// @Query(nativeQuery = true, value = "update product set average_price =
	// :averagePrice, balance = :balance, brand = :brand, category = :category,
	// code = :code, color = :color, description = :description,
	// image_byte_array = :imageByte, image_str = :imageStr, pieces = :pieces,
	// product_name = :productName, quality = :quality, quantity= :quantity,
	// price1 =:price1, price2 = :price2, price3 = :price3, unit_measurement =
	// :unitMeasurement, sub_category = :subCategory where product_id =
	// :productId")
	// public void updateProduct(@Param("productId") int productId,
	// @Param("averagePrice") Double averagePrice,
	// @Param("balance") double balance, @Param("brand") String brandName,
	// @Param("category") String category,
	// @Param("code") String codeNumber, @Param("color") String color,
	// @Param("description") String description,
	// @Param("imageByte") byte[] imageByte, @Param("imageStr") String
	// imageString, @Param("pieces") int pieces,
	// @Param("productName") String productName, @Param("quality") String
	// quality, @Param("quantity") int quantity,
	// @Param("price1") Double sellingPrice1, @Param("price2") Double
	// sellingPrice2,
	// @Param("price3") Double sellingPrice3, @Param("unitMeasurement") String
	// unitMeasurement,
	// @Param("subCategory") String subCategory);
	//
	// // @Query(nativeQuery = true, value = "select p.product_id,
	// p.product_name,
	// // p.brand, p.category, p.code, p.color, p.average_price, p.quantity,
	// // p.pieces, p.image_str, p.image_byte_array, p.price1, p.description,
	// // p.unit_measurement from product p")
	// // public List<ProductRequestForm> getProductWithSellingPrice1();
	//
	// @Query(value = "select new com.mm.cbk.stationary.form.Product (p.id,
	// p.productName, p.brandName, p.category, p.codeNumber, p.color,
	// p.averagePrice, p.quantity, p.pieces, p.imageString, p.imageByte,
	// p.sellingPrice1, p.description, p.unitMeasurement) from Product p")
	// public List<Product> getProductWithSellingPrice1();
	//
	// @Query(nativeQuery = true, value = "select product_id, product_name,
	// brand, category, code, color, average_price, quantity, pieces, image_str,
	// image_byte_array, price2, description, unit_measurement from product")
	// public List<Product> getProductWithSellingPrice2();
	//
	// @Query(nativeQuery = true, value = "select product_id, product_name,
	// brand, category, code, color, average_price, quantity, pieces, image_str,
	// image_byte_array, price3, description, unit_measurement from product")
	// public List<Product> getProductWithSellingPrice3();
	//
	@Query(nativeQuery = true, value = "select distinct brand from product order by brand asc")
	public List<String> getBrandName();

	/*@Query(nativeQuery = true, value = "select distinct category from product where brand = :brand")
	public List<String> getCategoryByBrand(@Param("brand") String brandName);*/
	
	@Query(nativeQuery = true, value = "select distinct category from product where brand = :brand")
	public List<Integer> getCategoryByBrand(@Param("brand") String brandName);

	/*@Query(nativeQuery = true, value = "select distinct sub_category from product where brand = :brand and category = :category")
	public List<String> getSubCategoryByCategoryAndBrand(@Param("brand") String brandName,
			@Param("category") String category);*/
	
	@Query(nativeQuery = true, value = "select distinct sub_category from product where brand = :brand and category = :category")
	public List<String> getSubCategoryByCategoryAndBrand(@Param("brand") String brandName,
			@Param("category") Integer category);

	/*@Query(value = "select new com.mm.cbk.stationary.form.Product (p.id, p.productName, p.brandName, p.category, p.codeNumber, p.color, p.averagePrice, p.quantityLarge, p.pieces, p.imageString, p.imageByte, p.sellingPrice1, p.description, p.unitMeasurement, p.subCategory) from Product p where p.brandName = :brand and p.category = :category and p.subCategory =:subCategory")
	public List<Product> getProductWithSellingPrice1ByBrandAndCat(@Param("brand") String brandName,
			@Param("category") String category, @Param("subCategory") String subCategory);*/
	
	/*@Query(value = "select new com.mm.cbk.stationary.form.Product (p.id, p.productName, p.brandName, p.category, p.codeNumber, p.color, p.averagePrice, p.quantityLarge, p.pieces, p.imageString, p.imageByte, p.sellingPrice1, p.description, p.unitMeasurement, p.subCategory) from Product p where p.brandName = :brand and p.category = :category and p.subCategory =:subCategory")
	public List<Product> getProductWithSellingPrice1ByBrandAndCat(@Param("brand") String brandName,
			@Param("category") Integer category, @Param("subCategory") String subCategory);
	

	@Query(value = "select new com.mm.cbk.stationary.form.Product (p.id, p.productName, p.brandName, p.category, p.codeNumber, p.color, p.averagePrice, p.quantityLarge, p.pieces, p.imageString, p.imageByte, p.sellingPrice2, p.description, p.unitMeasurement, p.subCategory) from Product p where p.brandName = :brand and p.category = :category and p.subCategory =:subCategory")
	public List<Product> getProductWithSellingPrice2ByBrandAndCat(@Param("brand") String brandName,
			@Param("category") String category, @Param("subCategory") String subCategory);*/

	// @Query(value = "select new com.mm.cbk.stationary.form.Product (p.id,
	// p.productName, p.brandName, p.category, p.codeNumber, p.color,
	// p.averagePrice, p.quantity, p.pieces, p.imageString, p.imageByte,
	// p.sellingPrice3, p.description, p.unitMeasurement, p.subCategory) from
	// Product p where p.brandName = :brand and p.category = :category and
	// p.subCategory =:subCategory")
	// public List<Product>
	// getProductWithSellingPrice3ByBrandAndCat(@Param("brand") String
	// brandName,
	// @Param("category") String category, @Param("subCategory") String
	// subCategory);
	//
	// @Modifying
	// @Query(nativeQuery = true, value = "insert into product (average_price,
	// balance, brand, category, code, color, description, image_byte_array,
	// pieces, product_name, quality, quantity, price1, price2, price3,
	// unit_measurement, sub_category) values
	// (:averagePrice,:balance,:brand,:category,:code,:color,:description,:imageByte,:pieces,:productName,:quality,:quantity,:price1,:price2,:price3,:unitMeasurement,:subCategory)")
	// void insertProduct(@Param("averagePrice") Double averagePrice,
	// @Param("balance") double balance,
	// @Param("brand") String brandName, @Param("category") String category,
	// @Param("code") String codeNumber,
	// @Param("color") String color, @Param("description") String description,
	// @Param("imageByte") byte[] imageByte, @Param("pieces") int pieces,
	// @Param("productName") String productName,
	// @Param("quality") String quality, @Param("quantity") int quantity,
	// @Param("price1") Double sellingPrice1,
	// @Param("price2") Double sellingPrice2, @Param("price3") Double
	// sellingPrice3,
	// @Param("unitMeasurement") String unitMeasurement, @Param("subCategory")
	// String subCategory);
}
