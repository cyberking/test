package com.mm.cbk.stationary.repository;

import java.util.Date;
import java.util.List;

import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mm.cbk.stationary.form.OrderRecord;
import com.mm.cbk.stationary.form.Orders;

@PersistenceContext
@Transactional
@Repository
public interface OrderRecordRepository extends JpaRepository<OrderRecord, Integer> {

	@Query(nativeQuery = true, value = "select * from order_record")
	List<Orders> getOrderList();

	@Modifying
	@Query(nativeQuery = true, value = "insert into order_record (customer_name, user_name, location, itemCode, quantity, unit, price, pieces, brand_name, product_name, subcategory, category) values (?1, ?2, ?3, ?4,?5, ?6, ?7, ?8,?9, ?10, ?11, ?12)")
	int insertOrderData(String customerName, String userName, String location, String itemCode, int quantity,
			String unitOfMeasurment, String price, String pieces, String brandName, String productName,
			String subcategory, String category);

	@Query(nativeQuery = true, value = "select * from order_record where order_id = ?1")
	List<OrderRecord> getOrderRecordsListById(Integer orderId);

	@Modifying
	@Query(nativeQuery = true, value = "update order_record set delivery_qty_large = ?1, delivery_qty_medium = ?2, delivery_qty_small = ?3, discount_amount = ?4, updated_date = ?5, order_status = ?6, price = ?7 where id = ?8")
	void updateOrderRecordConfirm(Integer quantityLarge, Integer quantityMedium, Integer quantitySmall,
			Double discountAmt, Date date, Integer orderStatus, Double sellingPrice, Integer id);

	@Modifying
	@Query(nativeQuery = true, value = "update order_record set order_status = ?1, updated_date = ?2 where order_id = ?3")
	void updateOrderRecordInvoice(Integer code, Date date, Integer id);

	@Modifying
	@Query(nativeQuery = true, value = "update order_record set delivery_qty_large = ?1, delivery_qty_medium = ?2, delivery_qty_small = ?3 where id = ?4")
	void updateOrderRecordDeliveryQty(Integer deliveryQtyL, Integer deliveryQtyL2, Integer deliveryQtyL3, Integer id);

	@Modifying
	@Query(nativeQuery = true, value = "update order_record set order_status = ?2, updated_date = ?3 where order_id = ?1")
	void changeOrderStatusByOrderId(Integer orderId, Integer code, Date date);

//	@Transactional
//	@Query(nativeQuery = true, value = "select COUNT(orders.id) AS orders, order_record.*, orders.* from order_record LEFT JOIN orders on order_record.order_id = orders.id group by order_record.id, orders.id order by orders.id asc")
//	List<OrderRecord> getOrderHistoryWithRecord();
	
//	@Transactional
	@Query(nativeQuery = true, value = "select * from order_record inner JOIN orders on order_record.order_id = orders.id where orders.id = :orderId")
	List<OrderRecord> getOrderHistoryWithRecord(@Param("orderId") int orderId);
}
