package com.mm.cbk.stationary.repository;

import java.util.List;

import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mm.cbk.stationary.form.Users;

@PersistenceContext
@Transactional
@Repository
public interface UserRepository extends JpaRepository<Users, String> {

	@Modifying
	@Query(nativeQuery = true, value = "insert into users (name, password, role_id, login_id, active) values (?1, ?2, ?3, ?4, True)")
	int insertUser(String userName, String password, int roleId, String loginId);

	@Query(nativeQuery = true, value = "select * from users where login_id = :loginId and password = :password and active = True")
	Users getUserByNameAndPassword(@Param("loginId") String loginId, @Param("password") String password);

	@Query(nativeQuery = true, value = "select * from users where active = True")
	List<Users> getUserList();

	@Modifying
	@Query(nativeQuery = true, value = "update users set active = False where users.user_id = :userId")
	int deactiveUser(@Param("userId") int userId);

	@Query(nativeQuery = true, value = "select * from users where user_id = :userId")
	Users getUserById(@Param("userId") int userId);

	@Modifying
	@Query(nativeQuery = true, value = "update users set gcm_id = :gcmId where user_id = :userId")
	int addGCMId(@Param("userId") int userId, @Param("gcmId") String gcmId);

	@Modifying
	@Query(nativeQuery = true, value = "update users set gcm_id = :gcmId where login_id = :loginId")
	int addGCMIdByLoginId(@Param("loginId") String loginId, @Param("gcmId") String gcmId);

	@Modifying
	@Query(nativeQuery = true, value = "update users set name = :name, password = :password, role_id = :roleId where login_id = :loginId")
	int updateUser(@Param("loginId") String loginId, @Param("name") String name, @Param("password") String password,
			@Param("roleId") int roleId);

	@Query(nativeQuery = true, value = "select * from users where login_id = :loginId")
	Users getUserByloginId(@Param("loginId") String loginId);
}
