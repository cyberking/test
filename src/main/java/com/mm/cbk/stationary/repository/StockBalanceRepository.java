package com.mm.cbk.stationary.repository;

import java.util.Date;
import java.util.List;

import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mm.cbk.stationary.form.StockBalance;

@PersistenceContext
@Transactional
@Repository
public interface StockBalanceRepository extends JpaRepository<StockBalance, String> {

	@Query(nativeQuery = true, value = "select count(*) from stock_balance where stock_id = :stockId")
	Integer getStockBalanceCount(@Param("stockId") Integer stockId);

	@Query(nativeQuery = true, value = "select * from stock_balance where stock_id = ?1 and purchase_date = ?2")
	StockBalance getStockBalanceWithPurchaseDate(Integer stockId, Date date);

	@Modifying
	@Query(nativeQuery = true, value = "insert into stock_balance(balance, purchase_date, updated_date, stock_id)VALUES (?1, ?2, ?3, ?4)")
	void insertStockBalance(Integer totalQty, Date purchaseDate, Date date, Integer stockId);

	@Modifying
	@Query(nativeQuery = true, value = "update stock_balance set balance = ?2, updated_date = ?3 where id = ?1")
	void updateStockBalance(Integer id, Integer totalQty, Date date);

	@Query(nativeQuery = true, value = "select * from stock_balance where stock_id = ?1 order by purchase_date asc")
	List<StockBalance> getStockBalanceListByStockId(Integer stockId);

	@Modifying
	@Query(nativeQuery = true, value = "update stock_balance set balance = balance - ?2, purchase_date = ?3, updated_date = ?4 where stock_id = ?1")
	void updateBalanceByStockIdAndPurchaseDate(Integer stockId, Integer purchaseQty, Date purchaseDate, Date updateDate);

}
