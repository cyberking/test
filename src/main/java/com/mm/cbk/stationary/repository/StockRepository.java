package com.mm.cbk.stationary.repository;

import java.util.List;

import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mm.cbk.stationary.form.Stock;

@PersistenceContext
@Transactional
@Repository
public interface StockRepository extends JpaRepository<Stock, String> {

	@Query(nativeQuery = true, value = "select * from stock order by code asc")
	List<Stock> getAllStocks();

	@Modifying
	@Query(nativeQuery = true, value = "update stock set code = ?2, stock_name = ?3, category_id = ?4, sub_category = ?5, brand = ?6, color = ?7, unit_measurement = ?8, total_balance = ?9, average_price  = ?10, price1 = ?11, price2 = ?12, price3  = ?13, price4 = ?14, description = ?15"
			+ ", packing_medium = ?16, packing_small = ?17, image_byte_array = ?18 where id = ?1")
	void updateStock(Integer id, String codeNumber, String stockName, Integer categoryId, String subCategory,
			String brandName, String color, String unitMeasurement, Integer totalQty, Double averagePrice,
			Double sellingPrice1, Double sellingPrice2, Double sellingPrice3, Double sellingPrice4, String description,
			Integer packingMedium, Integer packingSmall, byte[] imageByte);

	@Query(nativeQuery = true, value = "select * from stock where code like concat('%',:code,'%') ")
	List<Stock> searchStockByCode(@Param("code") String code);

	@Query(nativeQuery = true, value = "select * from stock where id = :id")
	Stock getStockById(@Param("id") Integer id);

	@Modifying
	@Query(nativeQuery = true, value = "delete from stock where id = :id")
	void deleteStock(@Param("id") int id);

	@Modifying
	@Query(nativeQuery = true, value = "insert into stock(average_price, brand, code, color, description, image_byte_array, image_str,packing_medium, packing_small, price1, price2, price3, price4, stock_name, sub_category, total_balance, unit_measurement, category_id) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13, ?14, ?15, ?16, ?17, ?18)")
	void insertStock(Double averagePrice, String brandName, String codeNumber, String color, String description,
			byte[] imageByte, String imageString, Integer packingMedium, Integer packingSmall, Double sellingPrice1,
			Double sellingPrice2, Double sellingPrice3, Double sellingPrice4, String stockName, String subCategory,
			Integer totalQty, String unitMeasurement, Integer categoryId);

	@Query(nativeQuery = true, value = "select count(*) from stock where code = ?1")
	Integer getStockCountByCode(String codeNumber);

	@Modifying
	@Query(nativeQuery = true, value = "update stock set total_balance = total_balance + ?2, average_price = ?3 where id = ?1")
	void updateStockBalAndAvgPriceById(Integer stockId, Integer totalQty, Double averagePrice);

	@Modifying
	@Query(nativeQuery = true, value = "update stock set total_balance = total_balance - ?2 where id = ?1")
	void updateStockBalanceById(Integer stockId, Integer totalBalance);

	@Query(nativeQuery = true, value = "select distinct brand from stock order by brand asc")
	public List<String> getBrandName();

	@Query(nativeQuery = true, value = "select distinct sub_category from stock INNER JOIN category ON stock.category_id = category.id where brand = :brand and category.name = :category")
	public List<String> getSubCategoryByCategoryAndBrand(@Param("brand") String brandName,
			@Param("category") String category);

	@Query(value = "select new com.mm.cbk.stationary.form.Stock (s.id,s.codeNumber,s.stockName,s.category.name, s.subCategory,s.brandName,s.color,s.unitMeasurement, s.totalBalance,s.averagePrice,s.sellingPrice1,s.description,s.packingMedium,s.packingSmall,s.imageByte,s.updatedDate) from Stock s where s.brandName = :brand and s.category.name = :category and s.subCategory =:subCategory")
	public List<Stock> getProductWithSellingPrice1ByBrandAndCat(@Param("brand") String brandName,
			@Param("category") String category, @Param("subCategory") String subCategory);

	@Query(value = "select new com.mm.cbk.stationary.form.Stock (s.id,s.codeNumber,s.stockName,s.category.name, s.subCategory,s.brandName,s.color,s.unitMeasurement, s.totalBalance,s.averagePrice,s.sellingPrice2,s.description,s.packingMedium,s.packingSmall,s.imageByte,s.updatedDate) from Stock s where s.brandName = :brand and s.category.name = :category and s.subCategory =:subCategory")
	public List<Stock> getProductWithSellingPrice2ByBrandAndCat(@Param("brand") String brandName,
			@Param("category") String category, @Param("subCategory") String subCategory);

	@Query(value = "select new com.mm.cbk.stationary.form.Stock (s.id,s.codeNumber,s.stockName,s.category.name, s.subCategory,s.brandName,s.color,s.unitMeasurement, s.totalBalance,s.averagePrice,s.sellingPrice3,s.description,s.packingMedium,s.packingSmall,s.imageByte,s.updatedDate) from Stock s where s.brandName = :brand and s.category.name = :category and s.subCategory =:subCategory")
	public List<Stock> getProductWithSellingPrice3ByBrandAndCat(@Param("brand") String brandName,
			@Param("category") String category, @Param("subCategory") String subCategory);
	
	@Query(value = "select new com.mm.cbk.stationary.form.Stock (s.id,s.codeNumber,s.stockName,s.category.name, s.subCategory,s.brandName,s.color,s.unitMeasurement, s.totalBalance,s.averagePrice,s.sellingPrice4,s.description,s.packingMedium,s.packingSmall,s.imageByte,s.updatedDate) from Stock s where s.brandName = :brand and s.category.name = :category and s.subCategory =:subCategory")
	public List<Stock> getProductWithSellingPrice4ByBrandAndCat(@Param("brand") String brandName,
			@Param("category") String category, @Param("subCategory") String subCategory);

	@Query(nativeQuery = true, value = "select * from stock where code = :code")
	public Stock getStockByCode(@Param("code") String code);
}
