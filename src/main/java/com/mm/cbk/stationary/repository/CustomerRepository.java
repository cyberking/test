package com.mm.cbk.stationary.repository;

import java.util.List;

import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mm.cbk.stationary.form.Customer;

@PersistenceContext
@Transactional
@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {

	@Modifying
	@Query(nativeQuery = true, value = "insert into customers (customer_name, address, phone_number1, phone_number2, phone_number3, email1, email2, category) values (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8)")
	int insertCustomer(String customerName, String address, String phone1, String phone2, String phone3, String email1,
			String email2, String category);

	// @Query(nativeQuery = true, value = "select * from users where login_id =
	// :loginId and password = :password")
	// Users getUserByNameAndPassword(@Param("loginId") String loginId,
	// @Param("password") String password);

	@Query(nativeQuery = true, value = "select * from customers")
	List<Customer> getCustomerList();

	@Modifying
	@Query(nativeQuery = true, value = "delete from customers where customers.customer_id = ?1")
	int deleteCustomerById(int customerId);

	@Query(nativeQuery = true, value = "select * from customers where customer_id = ?1")
	Customer getCustomerById(int customerId);

	@Query(nativeQuery = true, value = "select * from customers where shop_name = ?1")
	Customer getCustomerByShopName(String shopName);

	// @Modifying
	// @Query(nativeQuery = true, value = "update users set gcm_id = :gcmId
	// where user_id = :userId")
	// int addGCMId(@Param("userId") int userId, @Param("gcmId") String gcmId);
	//
	// @Modifying
	// @Query(nativeQuery = true, value = "update users set gcm_id = :gcmId
	// where login_id = :loginId")
	// int addGCMIdByLoginId(@Param("loginId") String loginId, @Param("gcmId")
	// String gcmId);
	@Modifying
	@Query(nativeQuery = true, value = "update customers set customer_name = ?1, address = ?2, phone_number1 = ?3, phone_number2 = ?4, phone_number3 = ?5, email1 = ?6, email2 = ?7, level_id = ?8 where customer_id = ?9")
	int updateCustomer(String customerName, String address, String phone1, String phone2, String phone3, String email1,
			String email2, int levelId, int customerId);

	@Query(nativeQuery = true, value = "select * from customers c inner join users  u on c.login_id = u.login_id where shop_name = ?1 and u.login_id = ?2 and u.password = ?3")
	Customer checkSignUpCustomer(String shopName, String loginId, String password);
}
