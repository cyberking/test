package com.mm.cbk.stationary.repository;

import java.util.List;

import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mm.cbk.stationary.form.Currency;

@PersistenceContext
@Transactional
@Repository
public interface CurrencyRepository extends JpaRepository<Currency, Integer> {

	@Query(nativeQuery = true, value = "select * from currency")
	List<Currency> getAllCurrency();
}
