package com.mm.cbk.stationary.repository;

import java.util.Date;
import java.util.List;

import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mm.cbk.stationary.form.Purchase;

@PersistenceContext
@Transactional
@Repository
public interface PurchaseRepository extends JpaRepository<Purchase, String>{

	@Query(nativeQuery = true, value = "select * from purchase order by purchase_date asc")
	List<Purchase> getAllPurchaseStockList();

	@Query(nativeQuery = true, value = "select * from purchase where id = ?1")
	Purchase getPurchaseStockById(int id);

	@Modifying
	@Query(nativeQuery = true, value = "INSERT INTO purchase(average_price, description, exchange_rate, purchase_date, purchase_price, quantity_large, quantity_medium, quantity_small, currency_id, stock_id, total_qty) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11)")
	void insertStockPurchase(Double avgPrice, String desc, Integer exchangeRate, Date date, Double purchasePrice, Integer qtyL, Integer qtyM, Integer qtyS, Integer curId, Integer stockId, Integer totalQty);

	@Modifying
	@Query(nativeQuery = true, value = "update purchase set average_price = ?1, description = ?2, exchange_rate = ?3, purchase_date = ?4, purchase_price = ?5, quantity_large = ?6, quantity_medium = ?7, quantity_small = ?8, currency_id = ?9, stock_id = ?10, total_qty = ?11 where id = ?12")
	void updateStockPurchase(Double averagePrice, String description, Integer exchangeRate, Date date,
			Double purchasePrice, Integer quantityLarge, Integer quantityMedium, Integer quantitySmall,
			Integer currencyId, Integer stockId, Integer newBalc, Integer id);
}
