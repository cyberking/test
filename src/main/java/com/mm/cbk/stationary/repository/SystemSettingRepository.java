package com.mm.cbk.stationary.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mm.cbk.stationary.form.SystemSetting;

public interface SystemSettingRepository extends JpaRepository<SystemSetting, Integer>{
	
	@Query(nativeQuery = true, value = "select * from system_setting")
	List<SystemSetting> getAllSystemSetting();
	
	@Query(nativeQuery = true, value = "select * from system_setting where code = ?1")
	SystemSetting getSettingByCode(String code);

}
