package com.mm.cbk.stationary.repository;

import java.util.List;

import javax.persistence.PersistenceContext;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mm.cbk.stationary.form.Category;

@PersistenceContext
@Repository
public interface CategoryRepository extends JpaRepository<Category, String>{
	
	@Query(nativeQuery = true, value = "select * from category")
	List<Category> getAllCategory();
	
	@Modifying
	@Query(nativeQuery = true, value = "select name from category")
	List<String> getCategoryList();
	
//	@Query(nativeQuery = true, value = "select * from category INNER JOIN stock ON stock.category_id = category.id and stock.brand = :brand")
//	public List<Category> getCategoryByBrand(@Param("brand") String brandName);
	
	@Query(nativeQuery = true, value = "select distinct category.* from category INNER JOIN stock ON stock.category_id = category.id and stock.brand = :brand")
    public List<Category> getCategoryByBrand(@Param("brand") String brandName);
}
