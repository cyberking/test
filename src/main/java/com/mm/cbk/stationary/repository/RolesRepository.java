package com.mm.cbk.stationary.repository;

import java.util.List;

import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mm.cbk.stationary.form.Roles;

@PersistenceContext
@Transactional
@Repository
public interface RolesRepository extends JpaRepository<Roles, Integer> {

	@Query(nativeQuery = true, value = "select * from roles")
	List<Roles> getAllRoles();
}
