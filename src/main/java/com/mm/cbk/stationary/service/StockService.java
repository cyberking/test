package com.mm.cbk.stationary.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mm.cbk.stationary.form.Category;
import com.mm.cbk.stationary.form.Customer;
import com.mm.cbk.stationary.form.Stock;
import com.mm.cbk.stationary.form.StockForm;
import com.mm.cbk.stationary.form.UnitMeasurement;
import com.mm.cbk.stationary.repository.CategoryRepository;
import com.mm.cbk.stationary.repository.StockBalanceRepository;
import com.mm.cbk.stationary.repository.StockRepository;
import com.mm.cbk.stationary.repository.UnitMeasurementRepository;
import com.mm.cbk.stationary.response.Brand;
import com.mm.cbk.stationary.response.CategoryResponse;
import com.mm.cbk.stationary.response.SubCategory;
import com.mm.cbk.stationary.utils.CommonUtil;

@Service
public class StockService {

	@Autowired
	StockRepository stockRepository;

	@Autowired
	CategoryRepository categoryRepo;

	@Autowired
	StockBalanceRepository stockBalanceRepository;

	@Autowired
	CustomerService customerService;

	@Autowired
	UnitMeasurementRepository unitRepo;

	public List<Brand> getProductByShopName(String shopName) {
		Customer customer = customerService.getCustomerByShopName(shopName);
		List<Stock> productList = new ArrayList<Stock>();
		List<Brand> brandList = new ArrayList<Brand>();
		if (customer.getCustomerLevel().getLevelName().equals("Level1")) {
			List<String> brandNameList = stockRepository.getBrandName();

			for (String brandName : brandNameList) {
				Brand brand = new Brand();
				List<Category> allCategoryList = categoryRepo.getCategoryByBrand(brandName);
				List<CategoryResponse> categoryList = new ArrayList<CategoryResponse>();
				for (Category categoryName : allCategoryList) {
					CategoryResponse categoryResponse = new CategoryResponse();
					List<String> subCategoryNameList = stockRepository.getSubCategoryByCategoryAndBrand(brandName,
							categoryName.getName());
					List<SubCategory> subCategoryList = new ArrayList<SubCategory>();
					for (String subCatName : subCategoryNameList) {
						SubCategory subCategory = new SubCategory();
						List<Stock> productListByBrand = stockRepository.getProductWithSellingPrice1ByBrandAndCat(
								brandName, categoryName.getName(), subCatName);
						List<StockForm> stockFormList = new ArrayList<StockForm>();
						for (Stock stock : productListByBrand) {
							StockForm stockForm = new StockForm();
							stockForm.setStockForm(stock);
							stockForm.setCategoryName(categoryName.getName());
							stockFormList.add(stockForm);
						}
						subCategory.setSubCategoryName(subCatName);
						subCategory.setStockFormList(stockFormList);
						subCategoryList.add(subCategory);
					}
					categoryResponse.setCategoryName(categoryName.getName());
					categoryResponse.setSubCategoryList(subCategoryList);
					categoryList.add(categoryResponse);
				}
				brand.setBrandName(brandName);
				brand.setCategoryList(categoryList);
				brandList.add(brand);
			}
			// productList = productRepository.getProductWithSellingPrice1();
		} else if (customer.getCustomerLevel().getLevelName().equals("Level2")) {
			// productList = productRepository.getProductWithSellingPrice2();
			List<String> brandNameList = stockRepository.getBrandName();

			for (String brandName : brandNameList) {
				Brand brand = new Brand();
				List<Category> categoryNameList = categoryRepo.getCategoryByBrand(brandName);
				List<CategoryResponse> categoryList = new ArrayList<CategoryResponse>();
				for (Category categoryName : categoryNameList) {
					CategoryResponse category = new CategoryResponse();
					List<String> subCategoryNameList = stockRepository.getSubCategoryByCategoryAndBrand(brandName,
							categoryName.getName());
					List<SubCategory> subCategoryList = new ArrayList<SubCategory>();
					for (String subCatName : subCategoryNameList) {
						SubCategory subCategory = new SubCategory();
						List<Stock> productListByBrand = stockRepository.getProductWithSellingPrice2ByBrandAndCat(
								brandName, categoryName.getName(), subCatName);
						List<StockForm> stockFormList = new ArrayList<StockForm>();
						for (Stock stock : productListByBrand) {
							StockForm stockForm = new StockForm();
							stockForm.setCategoryName(categoryName.getName());
							stockForm.setStockForm(stock);
							stockFormList.add(stockForm);
						}
						subCategory.setSubCategoryName(subCatName);
						subCategory.setStockFormList(stockFormList);
						subCategoryList.add(subCategory);
					}
					category.setCategoryName(categoryName.getName());
					category.setSubCategoryList(subCategoryList);
					categoryList.add(category);
				}
				brand.setBrandName(brandName);
				brand.setCategoryList(categoryList);
				brandList.add(brand);
			}
			// productList =
			// productRepository.getProductWithSellingPrice2ByBrandAndCat();
		} else if (customer.getCustomerLevel().getLevelName().equals("Level3")) {
			// productList = productRepository.getProductWithSellingPrice3();
			List<String> brandNameList = stockRepository.getBrandName();

			for (String brandName : brandNameList) {
				Brand brand = new Brand();
				List<Category> categoryNameList = categoryRepo.getCategoryByBrand(brandName);
				List<CategoryResponse> categoryList = new ArrayList<CategoryResponse>();
				for (Category categoryName : categoryNameList) {
					CategoryResponse category = new CategoryResponse();
					List<String> subCategoryNameList = stockRepository.getSubCategoryByCategoryAndBrand(brandName,
							categoryName.getName());
					List<SubCategory> subCategoryList = new ArrayList<SubCategory>();
					for (String subCatName : subCategoryNameList) {
						SubCategory subCategory = new SubCategory();
						List<Stock> productListByBrand = stockRepository.getProductWithSellingPrice3ByBrandAndCat(
								brandName, categoryName.getName(), subCatName);
						List<StockForm> stockFormList = new ArrayList<StockForm>();
						for (Stock stock : productListByBrand) {
							StockForm stockForm = new StockForm(stock);
							stockForm.setCategoryName(categoryName.getName());
							stockForm.setStockForm(stock);
							stockFormList.add(stockForm);
						}
						subCategory.setSubCategoryName(subCatName);
						subCategory.setStockFormList(stockFormList);
						subCategoryList.add(subCategory);
					}
					category.setCategoryName(categoryName.getName());
					category.setSubCategoryList(subCategoryList);
					categoryList.add(category);
				}
				brand.setBrandName(brandName);
				brand.setCategoryList(categoryList);
				brandList.add(brand);
			}
			// productList =
			// productRepository.getProductWithSellingPrice3ByBrandAndCat();
		}
		else if (customer.getCustomerLevel().getLevelName().equals("Level4")) {
			// productList = productRepository.getProductWithSellingPrice3();
			List<String> brandNameList = stockRepository.getBrandName();

			for (String brandName : brandNameList) {
				Brand brand = new Brand();
				List<Category> categoryNameList = categoryRepo.getCategoryByBrand(brandName);
				List<CategoryResponse> categoryList = new ArrayList<CategoryResponse>();
				for (Category categoryName : categoryNameList) {
					CategoryResponse category = new CategoryResponse();
					List<String> subCategoryNameList = stockRepository.getSubCategoryByCategoryAndBrand(brandName,
							categoryName.getName());
					List<SubCategory> subCategoryList = new ArrayList<SubCategory>();
					for (String subCatName : subCategoryNameList) {
						SubCategory subCategory = new SubCategory();
						List<Stock> productListByBrand = stockRepository.getProductWithSellingPrice4ByBrandAndCat(
								brandName, categoryName.getName(), subCatName);
						List<StockForm> stockFormList = new ArrayList<StockForm>();
						for (Stock stock : productListByBrand) {
							StockForm stockForm = new StockForm(stock);
							stockForm.setCategoryName(categoryName.getName());
							stockForm.setStockForm(stock);
							stockFormList.add(stockForm);
						}
						subCategory.setSubCategoryName(subCatName);
						subCategory.setStockFormList(stockFormList);
						subCategoryList.add(subCategory);
					}
					category.setCategoryName(categoryName.getName());
					category.setSubCategoryList(subCategoryList);
					categoryList.add(category);
				}
				brand.setBrandName(brandName);
				brand.setCategoryList(categoryList);
				brandList.add(brand);
			}
			// productList =
			// productRepository.getProductWithSellingPrice3ByBrandAndCat();
		}
		if (productList.size() > 0) {

		}
		return brandList;
	}

	public List<StockForm> getAllStockList() {
		List<StockForm> stockFormList = new ArrayList<StockForm>();
		List<Stock> stockList = stockRepository.getAllStocks();
		if (stockList != null) {
			for (Stock stock : stockList) {
				StockForm stockForm = new StockForm(stock);
				stockFormList.add(stockForm);
			}
		}
		return stockFormList;
	}

	public void manageStock(StockForm stockForm) {
		stockForm.setAveragePrice(stockForm.getAveragePrice() == null ? 0.0 : stockForm.getAveragePrice());
		stockForm.setTotalQty(stockForm.getTotalQty() == null ? 0 : stockForm.getTotalQty());
		stockForm.setExchangeRate(stockForm.getExchangeRate() == null ? 0 : stockForm.getExchangeRate());
		stockForm.setImageString(null);
		if (!stockForm.getStockImage().isEmpty()) {
			byte[] imageByte = CommonUtil.convertFileToByteArray(stockForm.getStockImage());
			// String imageDataString =
			// "data:image/png;base64,".concat(Base64.encodeBase64String(imageByte));
			stockForm.setImageByte(imageByte);
			// stockForm.setImageString(imageDataString);
		}
		if (stockForm.getId() != null && stockForm.getId() > 0) {
			stockRepository.updateStock(stockForm.getId(), stockForm.getCodeNumber(), stockForm.getStockName(),
					stockForm.getCategoryId(), stockForm.getSubCategory(), stockForm.getBrandName(),
					stockForm.getColor(), stockForm.getUnitMeasurement(), stockForm.getTotalQty(),
					stockForm.getAveragePrice(), stockForm.getSellingPrice1(), stockForm.getSellingPrice2(),
					stockForm.getSellingPrice3(), stockForm.getSellingPrice4(), stockForm.getDescription(),
					stockForm.getPackingMedium(), stockForm.getPackingSmall(), stockForm.getImageByte());
		} else {
			stockRepository.insertStock(stockForm.getAveragePrice(), stockForm.getBrandName(),
					stockForm.getCodeNumber(), stockForm.getColor(), stockForm.getDescription(),
					stockForm.getImageByte(), stockForm.getImageString(), stockForm.getPackingMedium(),
					stockForm.getPackingSmall(), stockForm.getSellingPrice1(), stockForm.getSellingPrice2(),
					stockForm.getSellingPrice3(), stockForm.getSellingPrice4(), stockForm.getStockName(),
					stockForm.getSubCategory(), stockForm.getTotalQty(), stockForm.getUnitMeasurement(),
					stockForm.getCategoryId());
		}
	}

	public boolean deleteStock(Integer id) {
		Integer count = stockBalanceRepository.getStockBalanceCount(id);
		if (count > 0)
			return false;
		else {
			stockRepository.deleteStock(id);
			return true;
		}
	}

	public List<StockForm> searchStockByCode(String stkCode) {
		List<StockForm> dtoList = new ArrayList<StockForm>();
		if (stkCode != null && !stkCode.trim().isEmpty()) {
			List<Stock> entityList = stockRepository.searchStockByCode(stkCode.trim());
			for (Stock entity : entityList) {
				StockForm dto = new StockForm(entity);
				dtoList.add(dto);
			}
		}
		return dtoList;
	}

	public StockForm getStockById(Integer id) {
		Stock stock = stockRepository.getStockById(id);
		StockForm stockForm = new StockForm();
		if (stock != null)
			stockForm = new StockForm(stock);
		return stockForm;
	}

	public boolean checkDuplicateCodeNo(String codeNumber) {
		Integer count = stockRepository.getStockCountByCode(codeNumber);
		if (count > 0)
			return false;
		return true;
	}

	public List<String> getBrandNameList() {
		List<String> brandNameList = stockRepository.getBrandName();
		return brandNameList;
	}

	public List<String> getCategoryList() {
		List<String> categoryList = categoryRepo.getCategoryList();
		return categoryList;
	}

	public List<UnitMeasurement> getAllUnitMeasurement() {
		List<UnitMeasurement> unitList = unitRepo.getAllUnitMeasurement();
		return unitList;
	}
}
