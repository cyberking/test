package com.mm.cbk.stationary.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.mm.cbk.stationary.form.Product;
import com.mm.cbk.stationary.form.ProductForm;
import com.mm.cbk.stationary.repository.ProductRepository;
import com.mm.cbk.stationary.utils.CommonConstant;

@Service
public class ProductService {

	@Autowired
	ProductRepository productRepository;

	@Autowired
	CustomerService customerService;

	public List<Product> getProductList() {
		List<Product> productList = productRepository.getProductList();
		if (productList.size() < 1) {
			productList = new ArrayList<Product>();
		}
		return productList;
	}

	public List<ProductForm> getProductFormList() {
		List<Product> productList = productRepository.getProductList();
		List<ProductForm> productFormList = new ArrayList<ProductForm>();
		for (Product product : productList) {
			ProductForm form = new ProductForm(product);
			productFormList.add(form);

		}
		return productFormList;
	}

	public void insertProduct(ProductForm productForm) {
		if (!productForm.getStockImage().isEmpty()) {
			byte[] imageByte = this.convertFileToByteArray(productForm.getStockImage());
			String imageDataString = "data:image/png;base64,".concat(Base64.encodeBase64String(imageByte));
			productForm.setImageByte(imageByte);
			productForm.setImageString(imageDataString);
		}
		if (productForm.getCurrencyId() == CommonConstant.CurrencyType.KYATS.getCode())
			productForm.setExchangeRate(0);
		if (productForm.getId() > 0) {
			productRepository.updateProduct(productForm.getId(), productForm.getAveragePrice(),
					productForm.getPurchasePrice(), productForm.getBrandName(), productForm.getCodeNumber(),
					productForm.getColor(), productForm.getDescription(), productForm.getExchangeRate(),
					productForm.getImageByte(), productForm.getImageString(), productForm.getProductName(),
					productForm.getQuantityLarge(), productForm.getQuantityMedium(), productForm.getQuantitySmall(),
					productForm.getPackingMedium(), productForm.getPackingSmall(), productForm.getReorderLevel(),
					productForm.getSellingPrice1(), productForm.getSellingPrice2(), productForm.getSellingPrice3(),
					productForm.getSellingPrice4(), productForm.getSubCategory(), productForm.getUnitMeasurement(),
					productForm.getCategoryId(), productForm.getCurrencyId());
		} else {
			Product product = productRepository.getProductByCodeNumber(productForm.getCodeNumber());
			if(product != null)
			{
				Integer prevPackMediumQty = product.getPackingMedium();
				Integer prevPackSmallQty = product.getPackingSmall();
				Integer prevQtyLarge = product.getQuantityLarge();
				Integer prevQtyM = product.getQuantityMedium();
				Integer prevQtyS = product.getQuantitySmall();
				Integer prevQtyL = product.getQuantityLarge();
				
				System.out.println(product.toString());
			}
			
			productRepository.insertProduct(productForm.getAveragePrice(), productForm.getPurchasePrice(),
					productForm.getBrandName(), productForm.getCodeNumber(), productForm.getColor(),
					productForm.getDescription(), productForm.getExchangeRate(), productForm.getImageByte(),
					productForm.getImageString(), productForm.getProductName(), productForm.getQuantityLarge(),
					productForm.getQuantityMedium(), productForm.getQuantitySmall(), productForm.getPackingMedium(),
					productForm.getPackingSmall(), productForm.getReorderLevel(), productForm.getSellingPrice1(),
					productForm.getSellingPrice2(), productForm.getSellingPrice3(), productForm.getSellingPrice4(),
					productForm.getSubCategory(), productForm.getUnitMeasurement(), productForm.getCategoryId(),
					productForm.getCurrencyId());
		}

	}

	public List<ProductForm> searchStockByCode(String stkCode) {
		List<ProductForm> dtoList = new ArrayList<ProductForm>();
		if (stkCode != null && !stkCode.trim().isEmpty()) 
		{
			List<Product> entityList = productRepository.searchStockByCode(stkCode.trim());			
			for (Product entity : entityList) 
			{
				ProductForm dto = new ProductForm(entity);
				dtoList.add(dto);
			}
		}
		return dtoList;
	}
	/*
	 * public void insertProduct(ProductForm productForm) { String[] codeNumArr
	 * = productForm.getCodeNumber().split(","); String[] productArr =
	 * productForm.getProductName().split(","); String[] categoryArr =
	 * productForm.getCategory().split(","); String[] brandNameArr =
	 * productForm.getBrandName().split(","); String[] colorArr =
	 * productForm.getColor().split(","); String[] price1Arr =
	 * productForm.getSellingPrice1().split(","); String[] price2Arr =
	 * productForm.getSellingPrice2().split(","); String[] price3Arr =
	 * productForm.getSellingPrice3().split(","); String[] piecesArr =
	 * productForm.getPieces().split(","); String[] quantityArr =
	 * productForm.getQuantity().split(","); String[] unitMeasurementArr =
	 * productForm.getUnitMeasurement().split(","); String[] descriptionArr =
	 * productForm.getDescription().split(","); String[] subCategoryArr =
	 * productForm.getSubCategory().split(","); List<MultipartFile> imgArr =
	 * productForm.getProductImage();
	 * 
	 * int productsNum = codeNumArr.length; for (int index = 0; index <
	 * productsNum; index++) { ProductForm2 form = new ProductForm2();
	 * form.setCodeNumber(codeNumArr[index]);
	 * form.setProductName(productArr[index]);
	 * form.setCategory(categoryArr[index]);
	 * form.setBrandName(brandNameArr[index]); form.setColor(colorArr[index]);
	 * form.setSellingPrice1(price1Arr[index]);
	 * form.setSellingPrice2(price2Arr[index]);
	 * form.setSellingPrice3(price3Arr[index]);
	 * form.setPieces(piecesArr[index]); form.setQuantity(quantityArr[index]);
	 * form.setUnitMeasurement(unitMeasurementArr[index]);
	 * form.setDescription(descriptionArr[index]);
	 * form.setSubCategory(subCategoryArr[index]); byte[] imageByte =
	 * this.convertFileToByteArray(imgArr.get(index)); String imageDataString =
	 * "data:image/png;base64,".concat(Base64.encodeBase64String(imageByte));
	 * form.setImageByte(imageByte); form.setImageString(imageDataString);
	 * form.setAveragePrice("100.0"); //
	 * productRepository.insertProduct(Double.parseDouble(form.getAveragePrice()
	 * ), 0.0, form.getBrandName(), // form.getCategory(), form.getCodeNumber(),
	 * form.getColor(), form.getDescription(), // form.getImageByte(),
	 * Integer.parseInt(form.getPieces()), form.getProductName(),
	 * form.getQuality(), // Integer.parseInt(form.getQuantity()),
	 * Double.parseDouble(form.getSellingPrice1()), //
	 * Double.parseDouble(form.getSellingPrice2()),
	 * Double.parseDouble(form.getSellingPrice3()), //
	 * form.getUnitMeasurement(), form.getSubCategory()); } }
	 */

	public Product getProduct(int productId) {
		Product product = new Product();
		product = productRepository.getProduct(productId);
		return product;
	}

	public void deleteProduct(int productId) {
		productRepository.deleteProduct(productId);
	}

	public void updateProduct(Product product) {
		// productRepository.updateProduct(product.getid(),
		// product.getproductName(), product.getbrandName(),
		// product.getcategory(), product.getcodeNumber(), product.getcolor(),
		// product.getaveragePrice(),
		// product.getpieces(), product.getquantity(), product.getimageString(),
		// product.getimageByte());
	}

	public String convertFileToByteStream(MultipartFile multipartFile) {
		String imageDataString = "";
		try {
			byte[] imageBytes = multipartFile.getBytes();
			imageDataString = Base64.encodeBase64URLSafeString(imageBytes);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return imageDataString;
	}

	public byte[] convertFileToByteArray(MultipartFile multipartFile) {
		byte[] imageBytes = null;
		// String imageDataString = "";
		try {
			imageBytes = multipartFile.getBytes();
			// imageDataString = Base64.encodeBase64URLSafeString(imageBytes);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return imageBytes;
	}

	/*
	 * public List<Brand> getProductByCustomerName(String customerName) {
	 * Customer customer = customerService.getCustomerByName(customerName);
	 * List<Product> productList = new ArrayList<Product>(); List<Brand>
	 * brandList = new ArrayList<Brand>(); if
	 * (customer.getCategory().equals("Level1")) { List<String> brandNameList =
	 * productRepository.getBrandName();
	 * 
	 * for (String brandName : brandNameList) { Brand brand = new Brand();
	 * List<String> categoryNameList =
	 * productRepository.getCategoryByBrand(brandName); List<Category>
	 * categoryList = new ArrayList<Category>(); for (String categoryName :
	 * categoryNameList) { Category category = new Category(); List<String>
	 * subCategoryNameList =
	 * productRepository.getSubCategoryByCategoryAndBrand(brandName,
	 * categoryName); List<SubCategory> subCategoryList = new
	 * ArrayList<SubCategory>(); for (String subCatName : subCategoryNameList) {
	 * SubCategory subCategory = new SubCategory(); List<Product>
	 * productListByBrand = productRepository
	 * .getProductWithSellingPrice1ByBrandAndCat(brandName, categoryName,
	 * subCatName); subCategory.setSubCategoryName(subCatName);
	 * subCategory.setProductList(productListByBrand);
	 * subCategoryList.add(subCategory); }
	 * category.setCategoryName(categoryName);
	 * category.setSubCategoryList(subCategoryList); categoryList.add(category);
	 * } List<Integer> categoryNameList =
	 * productRepository.getCategoryByBrand(brandName); List<Category>
	 * categoryList = new ArrayList<Category>(); for (Integer categoryName :
	 * categoryNameList) { Category category = new Category(); List<String>
	 * subCategoryNameList =
	 * productRepository.getSubCategoryByCategoryAndBrand(brandName,
	 * categoryName); List<SubCategory> subCategoryList = new
	 * ArrayList<SubCategory>(); for (String subCatName : subCategoryNameList) {
	 * SubCategory subCategory = new SubCategory(); List<Product>
	 * productListByBrand = productRepository
	 * .getProductWithSellingPrice1ByBrandAndCat(brandName, categoryName,
	 * subCatName); subCategory.setSubCategoryName(subCatName);
	 * subCategory.setProductList(productListByBrand);
	 * subCategoryList.add(subCategory); }
	 * category.setCategoryName(categoryName.toString());
	 * category.setSubCategoryList(subCategoryList); categoryList.add(category);
	 * } brand.setBrandName(brandName); brand.setCategoryList(categoryList);
	 * brandList.add(brand); } // productList =
	 * productRepository.getProductWithSellingPrice1(); } else if
	 * (customer.getCategory().equals("Level2")) { // productList =
	 * productRepository.getProductWithSellingPrice2(); List<String>
	 * brandNameList = productRepository.getBrandName();
	 * 
	 * for (String brandName : brandNameList) { Brand brand = new Brand();
	 * List<String> categoryNameList =
	 * productRepository.getCategoryByBrand(brandName); List<Category>
	 * categoryList = new ArrayList<Category>(); for (String categoryName :
	 * categoryNameList) { Category category = new Category(); List<String>
	 * subCategoryNameList =
	 * productRepository.getSubCategoryByCategoryAndBrand(brandName,
	 * categoryName); List<SubCategory> subCategoryList = new
	 * ArrayList<SubCategory>(); for (String subCatName : subCategoryNameList) {
	 * SubCategory subCategory = new SubCategory(); List<Product>
	 * productListByBrand = productRepository
	 * .getProductWithSellingPrice2ByBrandAndCat(brandName, categoryName,
	 * subCatName); subCategory.setSubCategoryName(subCatName);
	 * subCategory.setProductList(productListByBrand);
	 * subCategoryList.add(subCategory); }
	 * category.setCategoryName(categoryName);
	 * category.setSubCategoryList(subCategoryList); categoryList.add(category);
	 * } List<Integer> categoryNameList =
	 * productRepository.getCategoryByBrand(brandName); List<Category>
	 * categoryList = new ArrayList<Category>(); for (Integer categoryName :
	 * categoryNameList) { Category category = new Category(); List<String>
	 * subCategoryNameList =
	 * productRepository.getSubCategoryByCategoryAndBrand(brandName,
	 * categoryName); List<SubCategory> subCategoryList = new
	 * ArrayList<SubCategory>(); for (String subCatName : subCategoryNameList) {
	 * SubCategory subCategory = new SubCategory(); List<Product>
	 * productListByBrand = productRepository
	 * .getProductWithSellingPrice1ByBrandAndCat(brandName, categoryName,
	 * subCatName); subCategory.setSubCategoryName(subCatName);
	 * subCategory.setProductList(productListByBrand);
	 * subCategoryList.add(subCategory); }
	 * category.setCategoryName(categoryName.toString());
	 * category.setSubCategoryList(subCategoryList); categoryList.add(category);
	 * } brand.setBrandName(brandName); brand.setCategoryList(categoryList);
	 * brandList.add(brand); } // productList = //
	 * productRepository.getProductWithSellingPrice2ByBrandAndCat(); } else if
	 * (customer.getCategory().equals("Level3")) { // productList =
	 * productRepository.getProductWithSellingPrice3(); List<String>
	 * brandNameList = productRepository.getBrandName();
	 * 
	 * for (String brandName : brandNameList) { Brand brand = new Brand();
	 * List<String> categoryNameList =
	 * productRepository.getCategoryByBrand(brandName); List<Category>
	 * categoryList = new ArrayList<Category>(); for (String categoryName :
	 * categoryNameList) { Category category = new Category(); List<String>
	 * subCategoryNameList =
	 * productRepository.getSubCategoryByCategoryAndBrand(brandName,
	 * categoryName); List<SubCategory> subCategoryList = new
	 * ArrayList<SubCategory>(); for (String subCatName : subCategoryNameList) {
	 * SubCategory subCategory = new SubCategory(); List<Product>
	 * productListByBrand = new ArrayList<Product>(); // List<Product>
	 * productListByBrand = productRepository //
	 * .getProductWithSellingPrice3ByBrandAndCat(brandName, categoryName,
	 * subCatName); subCategory.setSubCategoryName(subCatName);
	 * subCategory.setProductList(productListByBrand);
	 * subCategoryList.add(subCategory); }
	 * category.setCategoryName(categoryName);
	 * category.setSubCategoryList(subCategoryList); categoryList.add(category);
	 * } List<Integer> categoryNameList =
	 * productRepository.getCategoryByBrand(brandName); List<Category>
	 * categoryList = new ArrayList<Category>(); for (Integer categoryName :
	 * categoryNameList) { Category category = new Category(); List<String>
	 * subCategoryNameList =
	 * productRepository.getSubCategoryByCategoryAndBrand(brandName,
	 * categoryName); List<SubCategory> subCategoryList = new
	 * ArrayList<SubCategory>(); for (String subCatName : subCategoryNameList) {
	 * SubCategory subCategory = new SubCategory(); List<Product>
	 * productListByBrand = productRepository
	 * .getProductWithSellingPrice1ByBrandAndCat(brandName, categoryName,
	 * subCatName); subCategory.setSubCategoryName(subCatName);
	 * subCategory.setProductList(productListByBrand);
	 * subCategoryList.add(subCategory); }
	 * category.setCategoryName(categoryName.toString());
	 * category.setSubCategoryList(subCategoryList); categoryList.add(category);
	 * } brand.setBrandName(brandName); brand.setCategoryList(categoryList);
	 * brandList.add(brand); } // productList = //
	 * productRepository.getProductWithSellingPrice3ByBrandAndCat(); } if
	 * (productList.size() > 0) {
	 * 
	 * } return brandList; }
	 */

	public List<String> getBrandNameList() {
		List<String> brandNameList = productRepository.getBrandName();
		return brandNameList;
	}

	// public MultipartFile convertByteStreamToMultipartFile(byte[]
	// imageByteStream) {
	// String fileName = "productImage.png";
	// CustomMultipartFile customMultipartFile = new
	// CustomMultipartFile(imageByteStream, fileName);
	// try {
	// customMultipartFile.transferTo(customMultipartFile.getFile());
	// } catch (IllegalStateException e) {
	// e.printStackTrace();
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	// return customMultipartFile;
	// }

}
