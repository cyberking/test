package com.mm.cbk.stationary.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mm.cbk.stationary.form.Roles;
import com.mm.cbk.stationary.repository.RolesRepository;

@Service
public class RolesService {

	@Autowired
	public RolesRepository rolesRepo;

	public List<Roles> getRole() {
		List<Roles> roles = rolesRepo.getAllRoles();
		return roles;
	}
}
