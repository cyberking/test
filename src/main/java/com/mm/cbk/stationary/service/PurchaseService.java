package com.mm.cbk.stationary.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mm.cbk.stationary.form.Purchase;
import com.mm.cbk.stationary.form.PurchaseForm;
import com.mm.cbk.stationary.form.Stock;
import com.mm.cbk.stationary.form.StockBalance;
import com.mm.cbk.stationary.repository.PurchaseRepository;
import com.mm.cbk.stationary.repository.StockBalanceRepository;
import com.mm.cbk.stationary.repository.StockRepository;
import com.mm.cbk.stationary.utils.CommonConstant;
import com.mm.cbk.stationary.utils.CommonUtil;

@Service
public class PurchaseService {

	@Autowired
	PurchaseRepository purchaseRepository;

	@Autowired
	StockRepository stockRepository;

	@Autowired
	StockBalanceRepository stockBalanceRepository;

	SimpleDateFormat format = new SimpleDateFormat(CommonConstant.STANDARD_DATE_FORMAT);

	public List<PurchaseForm> getAllPurchaseStockList() {
		List<Purchase> purchaseList = purchaseRepository.getAllPurchaseStockList();
		List<PurchaseForm> purchaseFormList = new ArrayList<PurchaseForm>();
		for (Purchase entity : purchaseList) {
			PurchaseForm form = new PurchaseForm(entity);
			purchaseFormList.add(form);
		}
		return purchaseFormList;
	}

	public PurchaseForm getPurchaseStockById(int id) {
		Purchase entity = purchaseRepository.getPurchaseStockById(id);
		PurchaseForm form = new PurchaseForm();
		if (entity != null)
			form = new PurchaseForm(entity);
		return form;
	}

	public String manageStockPurchase(PurchaseForm form) {
		Date date = new Date();
		Stock stock = stockRepository.getStockById(form.getStockId());
		// check previous average price exist or not, if exist, calculate again
		// else insert purchase price
		Double prevAvgPrice = stock.getAveragePrice() == null ? 0.0 : stock.getAveragePrice();
		Double newAvgPrice = form.getPurchasePrice() == null ? 0.0 : form.getPurchasePrice();
		if (prevAvgPrice > 0)
			newAvgPrice = (form.getPurchasePrice() + prevAvgPrice) / 2;
		// set exchange rate to 0 when currency is ks
		if (form.getCurrencyId() == CommonConstant.CurrencyType.KYATS.getCode())
			form.setExchangeRate(0);
		try {
			Integer totalQty = 0;
			totalQty = calculateTotalQty(stock, form);
			purchaseRepository.insertStockPurchase(newAvgPrice, form.getDescription(), form.getExchangeRate(), date,
					form.getPurchasePrice(), form.getQuantityLarge(), form.getQuantityMedium(), form.getQuantitySmall(),
					form.getCurrencyId(), form.getStockId(), totalQty);
			// insert or update stock balance in stock balance table
			Date purchaseDate = format.parse(form.getPurchaseDateStr());
			StockBalance stockBalance = stockBalanceRepository.getStockBalanceWithPurchaseDate(form.getStockId(),
					purchaseDate);
			if (stockBalance == null)
				stockBalanceRepository.insertStockBalance(totalQty, purchaseDate, date, form.getStockId());
			else {
				Integer originalBal = stockBalance.getBalance() == null ? 0 : stockBalance.getBalance();
				originalBal += totalQty;
				stockBalanceRepository.updateStockBalance(stockBalance.getId(), originalBal, new Date());
			}
			// for stock total balance in stock table
			stockRepository.updateStockBalAndAvgPriceById(form.getStockId(), totalQty, newAvgPrice);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	private Integer calculateTotalQty(Stock stock, PurchaseForm form) {
		Integer qtyPerBox = stock.getPackingMedium() == null ? 0 : stock.getPackingMedium();
		Integer qtyPerCard = stock.getPackingSmall() == null ? 0 : stock.getPackingSmall();
		Integer box = form.getQuantityLarge() == null ? 0 : form.getQuantityLarge();
		Integer card = form.getQuantityMedium() == null ? 0 : form.getQuantityMedium();
		Integer pieces = form.getQuantitySmall() == null ? 0 : form.getQuantitySmall();
		Integer totalPack = (box * qtyPerBox) + card;
		Integer totalQty = (totalPack * qtyPerCard) + pieces;
		return totalQty;
	}

	public Integer checkAvalStockBalance(PurchaseForm form) {
		Date purchaseDate = CommonUtil.stringToDate(form.getPurchaseDateStr(), CommonConstant.STANDARD_DATE_FORMAT);
		StockBalance stockBalance = stockBalanceRepository.getStockBalanceWithPurchaseDate(form.getStockId(),
				purchaseDate);
		if (stockBalance != null) {
			Integer qtyPerBox = form.getPackingMedium() == null ? 0 : form.getPackingMedium();
			Integer qtyPerCard = form.getPackingSmall() == null ? 0 : form.getPackingSmall();
			Integer box = form.getQuantityLarge() == null ? 0 : form.getQuantityLarge();
			Integer card = form.getQuantityMedium() == null ? 0 : form.getQuantityMedium();
			Integer pieces = form.getQuantitySmall() == null ? 0 : form.getQuantitySmall();
			Integer totalPack = (box * qtyPerBox) + card;
			Integer totalQty = (totalPack * qtyPerCard) + pieces;
			return (stockBalance.getBalance() - totalQty);
		}
		return 0;
	}

	public boolean updatePurchase(PurchaseForm form) {
		Date date = new Date();
		if (form.getId() != null && form.getId() > 0) {
			Purchase purchase = purchaseRepository.getPurchaseStockById(form.getId());
			Integer purchaseQty = purchase.getTotalQty();
			// prevStock
			Stock prevStock = stockRepository.getStockById(purchase.getStock().getId());
			Integer stockBal = prevStock.getTotalBalance();
			Integer remainingBalance = stockBal - purchaseQty;
			Integer updatedPurchaseQty = calculateTotalQty(prevStock, form);
			if (updatedPurchaseQty > purchaseQty) {
				// Integer updatedPurchaseQty = calculateTotalQty(prevStock,
				// form);
				String dateStr = format.format(purchase.getPurchaseDate());
				Date prevPurchaseDate = CommonUtil.stringToDate(dateStr, CommonConstant.STANDARD_DATE_FORMAT);
				Date todayDate = new Date();
				if (prevStock.getId() != form.getStockId()) {
					// reduce previous stock balance in stock and stockBalance
					// table
					stockRepository.updateStockBalanceById(prevStock.getId(), remainingBalance);
					stockBalanceRepository.updateBalanceByStockIdAndPurchaseDate(prevStock.getId(), purchaseQty,
							prevPurchaseDate, todayDate);
				}

				Date purchaseDate = CommonUtil.stringToDate(form.getPurchaseDateStr(),
						CommonConstant.STANDARD_24H_DATE_TIME_FORMAT);
				double avgPrice = 0.0;
				if (purchase.getPurchasePrice() == form.getPurchasePrice())
					avgPrice = purchase.getAveragePrice();
				else {
					avgPrice = (purchase.getAveragePrice() * 2) - purchase.getPurchasePrice();
					avgPrice = (form.getPurchasePrice() + avgPrice) / 2;
				}
				purchaseRepository.updateStockPurchase(avgPrice, form.getDescription(), form.getExchangeRate(),
						purchaseDate, form.getPurchasePrice(), form.getQuantityLarge(), form.getQuantityMedium(),
						form.getQuantitySmall(), form.getCurrencyId(), form.getStockId(), updatedPurchaseQty,
						form.getId());
				// insert or update stock balance in stock balance table
				/*
				 * StockBalance stockBalance =
				 * stockBalanceRepository.getStockBalanceWithPurchaseDate(form.
				 * getStockId(), purchaseDate); if (stockBalance == null)
				 * stockBalanceRepository.insertStockBalance(totalQty,
				 * purchaseDate, date, form.getStockId()); else { Integer
				 * originalBal = stockBalance.getBalance() == null ? 0 :
				 * stockBalance.getBalance(); //originalBal += totalQty;
				 * stockBalanceRepository.updateStockBalance(stockBalance.getId(
				 * ), originalBal, new Date()); } //for stock total balance in
				 * stock table
				 * stockRepository.updateStockBalAndAvgPriceById(form.getStockId
				 * (), totalQty, newAvgPrice);
				 */
			} else if (remainingBalance >= 0) {

			} else
				return false;
			Integer newBalc = 0;
			Integer oldBalc = prevStock.getTotalBalance() == null ? 0 : prevStock.getTotalBalance();
			newBalc = oldBalc + ((form.getTotalQty() == null ? 0 : form.getTotalQty()) * 1);
			purchaseRepository.updateStockPurchase(form.getAveragePrice(), form.getDescription(),
					form.getExchangeRate(), date, form.getPurchasePrice(), form.getQuantityLarge(),
					form.getQuantityMedium(), form.getQuantitySmall(), form.getCurrencyId(), form.getStockId(), newBalc,
					form.getId());
		}
		return true;
	}

	public boolean deletePurchaseStock(int purchaseId) {
		Purchase purchase = purchaseRepository.getPurchaseStockById(purchaseId);

		Integer purchaseQty = purchase.getTotalQty();
		Stock stock = stockRepository.getStockById(purchase.getStock().getId());
		Integer stockBalance = stock.getTotalBalance();
		Integer remainingBalance = stockBalance - purchaseQty;
		// check stock remaining balance and purchaseQty, if diffAmt is greater
		// than or equal, allow delete otherwise not allow to delete
		if (remainingBalance >= 0) {
			String dateStr = format.format(purchase.getPurchaseDate());
			Date purchaseDate = CommonUtil.stringToDate(dateStr, CommonConstant.STANDARD_DATE_FORMAT);
			Date todayDate = new Date();
			stockRepository.updateStockBalanceById(stock.getId(), remainingBalance);
			stockBalanceRepository.updateBalanceByStockIdAndPurchaseDate(stock.getId(), purchaseQty, purchaseDate,
					todayDate);
			purchaseRepository.delete(purchase);
		} else
			return false;

		return true;
	}

}
