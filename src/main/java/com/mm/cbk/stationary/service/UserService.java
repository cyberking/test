package com.mm.cbk.stationary.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mm.cbk.stationary.form.ResponseStatus;
import com.mm.cbk.stationary.form.Users;
import com.mm.cbk.stationary.form.UsersForm;
import com.mm.cbk.stationary.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	UserRepository userRepo;

	public ResponseStatus registerUser(UsersForm usersForm) {
		String userName = usersForm.getName();
		String password = usersForm.getPassword();
		String loginId = usersForm.getLoginId();
		int roleId = usersForm.getRoleId();
		ResponseStatus responseStatus = new ResponseStatus();
		responseStatus.setStatus("Fail");
		try {
			userRepo.insertUser(userName, password, roleId, loginId);
			responseStatus.setStatus("Success");
		} catch (Exception exp) {
			exp.printStackTrace();
		}
		return responseStatus;
	}

	public Users getUserByNameAndPassword(String loginId, String password) {
		Users users = new Users();
		users = userRepo.getUserByNameAndPassword(loginId, password);
		return users;
	}

	public List<Users> getUserList() {
		List<Users> userList = userRepo.getUserList();
		if (userList.size() < 1) {
			userList = new ArrayList<Users>();
		}
		return userList;
	}

	public void deactiveUsers(String userIdStr) {
		int userId = Integer.parseInt(userIdStr);
		userRepo.deactiveUser(userId);
	}

	public Users getUserById(String userIdStr) {
		Users user = new Users();
		int userId = Integer.parseInt(userIdStr);
		user = userRepo.getUserById(userId);
		return user;
	}

	public void addGCMId(int userId, String gcmId) {
		userRepo.addGCMId(userId, gcmId);
	}

	public void addGCMIdByLoginId(String loginId, String gcmId) {
		userRepo.addGCMIdByLoginId(loginId, gcmId);
	}

	public void updateUser(UsersForm userForm) {
		String loginId = userForm.getLoginId();
		String name = userForm.getName();
		String password = userForm.getPassword();
		int roleId = userForm.getRoleId();
		userRepo.updateUser(loginId, name, password, roleId);
	}

}
