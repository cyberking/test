package com.mm.cbk.stationary.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mm.cbk.stationary.form.Customer;
import com.mm.cbk.stationary.form.OrderForm;
import com.mm.cbk.stationary.form.OrderRecord;
import com.mm.cbk.stationary.form.OrderRecordForm;
import com.mm.cbk.stationary.form.OrderRequestForm;
import com.mm.cbk.stationary.form.Orders;
import com.mm.cbk.stationary.form.Stock;
import com.mm.cbk.stationary.form.StockBalance;
import com.mm.cbk.stationary.form.Users;
import com.mm.cbk.stationary.repository.CustomerRepository;
import com.mm.cbk.stationary.repository.OrderRecordRepository;
import com.mm.cbk.stationary.repository.OrdersRepository;
import com.mm.cbk.stationary.repository.PurchaseRepository;
import com.mm.cbk.stationary.repository.StockBalanceRepository;
import com.mm.cbk.stationary.repository.StockRepository;
import com.mm.cbk.stationary.repository.UserRepository;
import com.mm.cbk.stationary.utils.CommonConstant;
import com.mm.cbk.stationary.utils.CommonConstant.OrderStatus;
import com.mm.cbk.stationary.utils.CommonUtil;

@Service
public class OrderService {

	@Autowired
	OrderRecordRepository orderRecordRepository;

	@Autowired
	OrdersRepository orderRepository;

	@Autowired
	StockRepository stockRepository;

	@Autowired
	StockBalanceRepository stockBalanceRepository;

	@Autowired
	PurchaseRepository purchaseRepository;

	@Autowired
	CustomerRepository customerRepo;

	@Autowired
	UserRepository userRepo;

	/* ============= start previous methods ============= */
	public void insertOrderData(OrderRequestForm orderRequestForm) {
		String shopName = orderRequestForm.getShopName();
		String loginId = orderRequestForm.getLoginId();
		String location = orderRequestForm.getLocation();
		Users orderUser = userRepo.getUserByloginId(loginId);
		Customer orderCustomer = customerRepo.getCustomerByShopName(shopName);
		Orders orders = new Orders();
		orders.setCustomer(orderCustomer);
		orders.setUser(orderUser);
		orders.setDeliveryAddress(location);
		Date orderDate = new Date();
		orders.setOrderDate(orderDate);
		orders.setOrderStatus(1);
		orderRepository.save(orders);
		for (Stock stockData : orderRequestForm.getStockList()) {
			OrderRecord orderRecord = new OrderRecord();
			orderRecord.setOrder(orders);
			Stock stock = stockRepository.getStockByCode(stockData.getCodeNumber());
			orderRecord.setStock(stock);
			orderRecord.setPrice(stockData.getSellingPrice1());
			orderRecord.setQtySmall(stockData.getPcsQty());

			orderRecord.setDeliveryQtySmall(stockData.getQty());
			orderRecord.setUpdatedDate(orderDate);
			orderRecord.setOrderDate(orderDate);
			orderRecord.setOrderStatus(1);
			orderRecordRepository.save(orderRecord);
			// TODO save to order record
		}
		/*
		 * for (int index = 0; index < orderRequestForm.getStockList().size();
		 * index++) { Stock stock = new Stock(); stock =
		 * orderRequestForm.getStockList().get(index);
		 * 
		 * // orderRecordRepository.insertOrderData(customerName, userName, //
		 * location, stock.getCodeNumber(), // stock.getQuantity(),
		 * stock.getUnitMeasurement(), // stock.getPrice(), stock.getPieces(),
		 * stock.getBrandName(), // stock.getProductName(),
		 * stock.getSubcaegory(), // stock.getCaegory()); }
		 */

	}

	public List<Orders> getOrderList() {
		List<Orders> orderList = orderRecordRepository.getOrderList();
		if (orderList.size() < 1) {
			orderList = new ArrayList<Orders>();
		}
		return orderList;
	}

	/* ============= end previous methods ============= */

	/* ============= Wut Yi Kyaw start order methods ============= */

	public List<OrderForm> getOrderListByNotTargetStatus(Integer orderStatus) {
		List<Orders> orderList = orderRepository.getOrderListByNotTargetStatus(orderStatus);
		List<OrderForm> orderFormList = new ArrayList<OrderForm>();
		for (Orders entity : orderList) {
			OrderForm form = new OrderForm(entity);
			orderFormList.add(form);
		}
		return orderFormList;
	}

	public List<OrderForm> getAllOrderList() {
		List<Orders> orderList = orderRepository.getAllOrderList();
		List<OrderForm> orderFormList = new ArrayList<OrderForm>();
		for (Orders entity : orderList) {
			OrderForm form = new OrderForm(entity);
			orderFormList.add(form);
		}
		return orderFormList;
	}

	private Integer calculateOrderQty(OrderRecordForm orderRecordForm) {
		Integer qtyPerBox = orderRecordForm.getStockPackM();
		Integer qtyPerCard = orderRecordForm.getStockPackS();
		Integer box = orderRecordForm.getDeliveryQtyL();
		Integer card = orderRecordForm.getDeliveryQtyM();
		Integer pieces = orderRecordForm.getDeliveryQtyS();
		Integer totalPack = (box * qtyPerBox) + card;
		Integer totalOrderQty = (totalPack * qtyPerCard) + pieces;
		return totalOrderQty;
	}

	public boolean manageOrderConfirm(OrderRecordForm orderRecordForm) {
		if (orderRecordForm.getId() != null && orderRecordForm.getId() > 0) {
			Stock stock = stockRepository.getStockById(orderRecordForm.getStockId());
			Integer qtyPerBox = stock.getPackingMedium();
			Integer qtyPerCard = stock.getPackingSmall();
			Integer box = orderRecordForm.getDeliveryQtyL();
			Integer card = orderRecordForm.getDeliveryQtyM();
			Integer pieces = orderRecordForm.getDeliveryQtyS();
			Integer totalPack = (box * qtyPerBox) + card;
			Integer totalOrderQty = (totalPack * qtyPerCard) + pieces;
			if (stock.getTotalBalance() >= totalOrderQty) {
				orderRecordRepository.updateOrderRecordConfirm(orderRecordForm.getDeliveryQtyL(),
						orderRecordForm.getDeliveryQtyM(), orderRecordForm.getDeliveryQtyS(),
						orderRecordForm.getOrderRecDiscAmt(), new Date(), OrderStatus.CONFIRM.getCode(),
						orderRecordForm.getSellingPrice(), orderRecordForm.getId());
				return true;
			}
		}
		return false;
	}

	public OrderRecordForm getOrderRecordsListByOrderId(Integer orderId) {
		OrderRecordForm orderRecForm = new OrderRecordForm();
		Integer confirmOk = 0;// 0 - remaining stock balance enough and confirm
								// ok, otherwise - 1
		Orders orders = orderRepository.getOrderById(orderId);
		Double totalSellingAmt = 0.0;
		Double totalDiscAmt = 0.0;

		// getting and setting order record list information
		List<OrderRecordForm> orderRecordFormList = new ArrayList<OrderRecordForm>();
		List<OrderRecord> orderRecordList = orderRecordRepository.getOrderRecordsListById(orderId);
		for (OrderRecord entity : orderRecordList) {
			OrderRecordForm orderRecordForm = new OrderRecordForm(entity);
			// calculate and set totalQty(pieces) for each stock order
			Integer totalOrderQty = calculateOrderQty(orderRecordForm);

			// check remaining stock balance is enough or not
			if (orderRecordForm.getTotalStockBalance() < totalOrderQty) {
				if (confirmOk == 0)// cannot click confirm button once one order
									// qty is greater than stock remaining
									// balance
					confirmOk = 1;
			}

			// add selling price and discount amount for each order
			totalSellingAmt += orderRecordForm.getSellingPrice();
			totalDiscAmt += orderRecordForm.getOrderRecDiscAmt();
			orderRecordForm.setTotalOrderQty(totalOrderQty);
			orderRecordFormList.add(orderRecordForm);
		}
		orderRecForm.setOrderRecordList(orderRecordFormList);

		// setting order information
		orderRecForm.setOrderId(orders.getId());
		orderRecForm.setCustomerName(orders.getCustomer().getCustomerName());
		orderRecForm.setUserName(orders.getUser().getName());
		if (orders.getOrderDate() != null)
			orderRecForm.setOrdersDateStr(
					CommonUtil.dateToString(orders.getOrderDate(), CommonConstant.STANDARD_DATE_TIME_FORMAT));
		orderRecForm.setOrderStatus(orders.getOrderStatus());
		orderRecForm.setOrdersStatusDesc(CommonConstant.OrderStatus.getDescription(orders.getOrderStatus()));
		orderRecForm.setDeliveryAddress(orders.getDeliveryAddress());
		orderRecForm.setConfirmOk(confirmOk);
		orderRecForm.setDiscAmt(orders.getDiscountAmount() == null ? 0.0 : orders.getDiscountAmount());
		// total selling price and amount for order invoice form
		orderRecForm.setSellingPrice(totalSellingAmt);
		orderRecForm.setTotalAmt(totalSellingAmt - orderRecForm.getDiscAmt());

		// Start Modified by AungPhyo
		String deliveryDate = orders.getDeliveryDate() == null ? ""
				: CommonUtil.dateToString(orders.getDeliveryDate(), CommonConstant.STANDARD_DATE_TIME_FORMAT);
		orderRecForm.setDeliveryDate(deliveryDate);
		// End Modified by AungPhyo

		return orderRecForm;
	}

	public boolean invoiceOrderConfirm(OrderRecordForm form) {
		// update order status to confirm for target order id
		List<OrderRecord> orderRecordList = orderRecordRepository.getOrderRecordsListById(form.getOrderId());
		for (OrderRecord entity : orderRecordList) {
			OrderRecordForm orderRecordForm = new OrderRecordForm(entity);

			// calculate and set totalQty(pieces) for each stock order
			Integer remainingStockBal = orderRecordForm.getTotalStockBalance();
			Integer totalOrderQty = calculateOrderQty(orderRecordForm);
			if (remainingStockBal >= totalOrderQty
					&& orderRecordForm.getOrderStatus() != OrderStatus.CANCEL.getCode()) {
				// update stock balance in stock table
				stockRepository.updateStockBalanceById(orderRecordForm.getStockId(), totalOrderQty);

				// update stock balance in stock balance table by purchase date
				List<StockBalance> stockBalanceList = stockBalanceRepository
						.getStockBalanceListByStockId(orderRecordForm.getStockId());
				for (StockBalance sbal : stockBalanceList) {
					Integer stockBalanceQty = 0;
					if (totalOrderQty <= sbal.getBalance()) {
						stockBalanceQty = sbal.getBalance() - totalOrderQty;
						stockBalanceRepository.updateStockBalance(sbal.getId(), stockBalanceQty, new Date());
						break;
					} else {
						totalOrderQty = totalOrderQty - sbal.getBalance();
						stockBalanceRepository.updateStockBalance(sbal.getId(), stockBalanceQty, new Date());
					}
				}
				// update order status in orderRecord table
				orderRecordRepository.updateOrderRecordInvoice(OrderStatus.CONFIRM.getCode(), new Date(),
						form.getOrderId());
				// update order status, final selling price and discount amt for
				// target order id
				double disAmt = form.getDiscAmt() == null ? 0.0 : form.getDiscAmt();
				double finalSellingPrice = form.getSellingPrice() - disAmt;
				orderRepository.updateOrderStatusAndDiscount(OrderStatus.CONFIRM.getCode(), disAmt, form.getOrderId(),
                        finalSellingPrice, CommonUtil.stringToDate(form.getDeliveryDate(), CommonConstant.STANDARD_DATE_FORMAT));
			} else
				return false;

		}
		return true;
	}

	public Orders getOrderByOrderId(Integer orderId) {
		Orders order = orderRepository.getOrderById(orderId);
		return order;
	}

	public void cancelOrderById(Integer orderId) {
		orderRepository.cancelOrderStatus(orderId, OrderStatus.CANCEL.getCode());
		orderRecordRepository.changeOrderStatusByOrderId(orderId, OrderStatus.CANCEL.getCode(), new Date());
	}

	public void calculatRemainingeStockBal(Integer stockId) {
		Stock stock = stockRepository.getStockById(stockId);
		Integer totalBal = stock.getTotalBalance();
		Integer qtyPerBox = stock.getPackingMedium();
		Integer qtyPerCard = stock.getPackingSmall();

		Integer total = totalBal / qtyPerBox;
		Integer totalCard = totalBal / qtyPerCard;
		Integer remainingPieces = totalBal % qtyPerCard;
		Integer remainingBox = totalCard / qtyPerBox;
		Integer remainingCard = totalCard % qtyPerBox;
		System.out.println(total + " => Total Boxes " + remainingBox + ", Total remainingCard " + remainingCard
				+ ", pieces " + remainingPieces);
	}
	/* ============= Wut Yi Kyaw end order methods ============= */

	public List<OrderForm> getOrderHistoryWithOrderRecord(int customerId) {

		List<Orders> orderList = orderRepository.getOrdersList(customerId);
		List<OrderForm> resultList = new ArrayList<OrderForm>();
		for (Orders orderData : orderList) {
			OrderForm orderForm = new OrderForm(orderData);
			List<OrderRecord> orderRecordList = orderRecordRepository.getOrderHistoryWithRecord(orderData.getId());
			List<OrderRecordForm> orderRecordFormList = new ArrayList<OrderRecordForm>();
			for (OrderRecord orderRecord : orderRecordList) {
				OrderRecordForm orderRecordForm = new OrderRecordForm(orderRecord);
				// stockRepository.getStockByCode(orderRecord.getsto)
				orderRecordFormList.add(orderRecordForm);
			}

			orderForm.setOrderRecordFormList(orderRecordFormList);
			resultList.add(orderForm);
		}
		return resultList;
	}

}
