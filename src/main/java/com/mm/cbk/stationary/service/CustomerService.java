package com.mm.cbk.stationary.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mm.cbk.stationary.controller.LoginController;
import com.mm.cbk.stationary.form.Customer;
import com.mm.cbk.stationary.form.CustomerForm;
import com.mm.cbk.stationary.form.CustomerLevel;
import com.mm.cbk.stationary.form.CustomerRequestForm;
import com.mm.cbk.stationary.form.ResponseStatus;
import com.mm.cbk.stationary.repository.CustomerLevelRepository;
import com.mm.cbk.stationary.repository.CustomerRepository;

@Service
public class CustomerService {

	@Autowired
	CustomerRepository customerRepo;

	@Autowired
	CustomerLevelRepository cusLevelRepo;

	public ResponseStatus registerCustomer(CustomerForm customerForm) {
		ResponseStatus responseStatus = new ResponseStatus();
		responseStatus.setStatus("Fail");
		// String level = customerForm.getCustomerLevel().getLevelName();

		CustomerLevel customerLevel = new CustomerLevel();
		customerLevel.setLevel(customerForm.getLevel());
		customerLevel.setId(customerForm.getLevel());
		Customer customer = new Customer(customerForm);
		customer.setCustomerLevel(customerLevel);

		customerRepo.save(customer);
		// try {
		// customerRepo.insertCustomer(customerName, address, phone1, phone2,
		// phone3, email1, email2, level);
		// responseStatus.setStatus("Success");
		// } catch (Exception exp) {
		// exp.printStackTrace();
		// }
		return responseStatus;
	}

	// public Users getUserByNameAndPassword(String loginId, String password) {
	// Users users = new Users();
	// users = customerRepo.getUserByNameAndPassword(loginId, password);
	// return users;
	// }

	public List<Customer> getCustomerList() {
		List<Customer> customerList = customerRepo.getCustomerList();
		if (customerList.size() < 1) {
			customerList = new ArrayList<Customer>();
		}
		return customerList;
	}

	public void deleteCustomer(String customerIdStr) {
		int customerId = Integer.parseInt(customerIdStr);
		customerRepo.deleteCustomerById(customerId);
	}

	public Customer getCustomerById(String customerIdStr) {
		Customer customer = new Customer();
		int customerId = Integer.parseInt(customerIdStr);
		customer = customerRepo.getCustomerById(customerId);
		return customer;
	}

	public Customer getCustomerByShopName(String shopName) {
		Customer customer = new Customer();
		customer = customerRepo.getCustomerByShopName(shopName);
		return customer;
	}

	// public void addGCMId(int userId, String gcmId) {
	// customerRepo.addGCMId(userId, gcmId);
	// }
	//
	// public void addGCMIdByLoginId(String loginId, String gcmId) {
	// customerRepo.addGCMIdByLoginId(loginId, gcmId);
	// }

	public void updateCustomer(Customer customerForm) {
		String customerName = customerForm.getCustomerName();
		String address = customerForm.getAddress();
		String phone1 = customerForm.getPhoneNumber1();
		String phone2 = customerForm.getPhoneNumber2();
		String phone3 = customerForm.getPhoneNumber3();
		String email1 = customerForm.getEmail1();
		String email2 = customerForm.getEmail2();
		CustomerLevel customerLevel = customerForm.getCustomerLevel();
		int customerId = customerForm.getCustomerId();
		customerRepo.updateCustomer(customerName, address, phone1, phone2, phone3, email1, email2,
				customerLevel.getId(), customerId);
	}

	public Customer checkSignUpCustomer(CustomerRequestForm customerForm) {
		LoginController loginController = new LoginController();
		customerForm.setPassword(loginController.getHashPassword(customerForm.getPassword()));
		Customer customerResult = customerRepo.checkSignUpCustomer(customerForm.getShopName(),
				customerForm.getLoginName(), customerForm.getPassword());
		return customerResult;
	}

	public List<CustomerLevel> getCustomerLevelList() {
		List<CustomerLevel> cusLevelList = cusLevelRepo.getCustomerLevelList();
		return cusLevelList;
	}

}
