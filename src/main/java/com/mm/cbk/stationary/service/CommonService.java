package com.mm.cbk.stationary.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mm.cbk.stationary.form.Category;
import com.mm.cbk.stationary.form.Currency;
import com.mm.cbk.stationary.form.SystemSetting;
import com.mm.cbk.stationary.repository.CategoryRepository;
import com.mm.cbk.stationary.repository.CurrencyRepository;
import com.mm.cbk.stationary.repository.SystemSettingRepository;

@Service
public class CommonService {

	@Autowired
	public CategoryRepository categoryRepository;
	
	@Autowired
	public CurrencyRepository currencyRepository;
	
	@Autowired
	public SystemSettingRepository systemSettingRepository;
	
	
	public List<Category> getAllCategory(){
		List<Category> categoryList = categoryRepository.getAllCategory();
		return categoryList;
	}

	public List<Currency> getAllCurrency() {
		List<Currency> currencyList = currencyRepository.getAllCurrency();
		return currencyList;
	}

	public SystemSetting getSettingByCode(String exchangeRate) {		
		return systemSettingRepository.getSettingByCode(exchangeRate);
	}
}
