package com.mm.cbk.stationary.utils;

public class CommonConstant {

	public static final String EXCHANGE_RATE = "EXCHANGE_RATE";

	public static final String STANDARD_DATE_FORMAT = "dd/MM/yyyy";

	public static final String STANDARD_DATE_TIME_FORMAT = "dd/MM/yyyy hh:mm a";
	
	public static final String STANDARD_24H_DATE_TIME_FORMAT = "dd/MM/yyyy hh:mm:ss";
	
	public static final String ERROR = "ERROR";
	
	public static final String SUCCESS = "SUCCESS";
	
	public static final Integer SUCCESS_CODE = 0;
	
	public static final Integer FAILURE_CODE = 1;
	
	public enum CurrencyType {
		KYATS(1, "Ks"), USD(2, "USD");
		Integer code;
		String description;

		@Override
		public String toString() {
			return description;
		}

		private CurrencyType(Integer code, String description) {
			this.code = code;
			this.description = description;
		}

		public Integer getCode() {
			return code;
		}

		public String getDescription() {
			return description;
		}

		public static String getDescription(int code) {
			for (CurrencyType t : CurrencyType.values()) {
				if (t.code == code)
					return t.getDescription();
			}
			return "";
		}
	}

	public enum OrderStatus {
		ORDER(1, "Ordered"), CONFIRM(2, "Confirmed"), PARTIAL(3, "Partial"), WAREHOUSE_CONFIRM(4,
				"Warehouse Confirmed"), DELIVERED(5, "Delivered"), CANCEL(6,"Canceled");
		Integer code;
		String description;

		@Override
		public String toString() {
			return description;
		}

		private OrderStatus(Integer code, String description) {
			this.code = code;
			this.description = description;
		}

		public Integer getCode() {
			return code;
		}

		public String getDescription() {
			return description;
		}

		public static String getDescription(Integer code) {
			for (OrderStatus t : OrderStatus.values()) {
				if (t.code == code)
					return t.getDescription();
			}
			return "";
		}
	}
}
