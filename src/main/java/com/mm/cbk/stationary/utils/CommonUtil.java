package com.mm.cbk.stationary.utils;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

public class CommonUtil {

	public static byte[] convertFileToByteArray(MultipartFile multipartFile) {
		byte[] imageBytes = null;
		// String imageDataString = "";
		try {
			imageBytes = multipartFile.getBytes();
			// imageDataString = Base64.encodeBase64URLSafeString(imageBytes);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return imageBytes;
	}

	public static Date stringToDate(String dateTime, String standardDateInputFormat) {
		SimpleDateFormat sdf = new SimpleDateFormat(standardDateInputFormat);
		Date retDate = new Date();
		try {
			if (!dateTime.isEmpty())
				retDate = sdf.parse(dateTime);
		} catch (ParseException e) {
			//
		}
		return retDate;
	}

	public static String dateToString(Date dateTime, String standardDateInputFormat) {
		SimpleDateFormat sdf = new SimpleDateFormat(standardDateInputFormat);
		String retDate = new String();
		if (dateTime != null)
			retDate = sdf.format(dateTime);
		return retDate;
	}
}
