package com.mm.cbk.stationary.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.xml.ws.WebServiceException;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class NginxUtil {

	@Value("${nginx.directory}")
	private String nginxDirectory;

	@SuppressWarnings("resource")
	public String saveImageToNginx(MultipartFile multipartFile, int folderId, String fileConstantName,
			String folderDir) {
		String fileName = multipartFile.getOriginalFilename();
		String fileDirectory = null;
		File file = new File(multipartFile.getOriginalFilename());
		try {
			file.createNewFile();
			FileOutputStream fileOutputStream = new FileOutputStream(file);
			fileOutputStream.write(multipartFile.getBytes());
			fileOutputStream.close();

			FileInputStream fileInputStream = new FileInputStream(file);
			BufferedInputStream inputStream = new BufferedInputStream(fileInputStream);
			byte[] imageBytes = new byte[(int) file.length()];
			inputStream.read(imageBytes);

			String imageDataString = Base64.encodeBase64URLSafeString(imageBytes);
			System.out.println(imageDataString);
			
			byte[] imageByteArray = Base64.decodeBase64(imageDataString);
			String filePathStr = nginxDirectory + folderDir + "\\" + folderId + "\\" + fileConstantName;
			File filePath = new File(filePathStr);
			if (!filePath.exists()) {
				boolean folderExisted = filePath.getParentFile().mkdirs();
				if (!folderExisted) {
					throw new IOException("Unable to create path");
				}
			}
			FileOutputStream outputStream = new FileOutputStream(
					nginxDirectory + folderDir + "\\" + folderId + "\\" + fileConstantName);
			outputStream.write(imageByteArray);

			inputStream.close();
			outputStream.close();
			System.out.println("Received file: " + fileName);
			String nginxDir = "http://192.168.0.27:8081/" + folderDir + "/" + folderId + "/" + fileConstantName;
			fileDirectory = nginxDir;

		} catch (IOException ex) {
			System.err.println(ex);
			ex.printStackTrace();
			throw new WebServiceException(ex);
		} catch (IllegalStateException e) {
			e.printStackTrace();
		}
		return fileDirectory;
	}
}
