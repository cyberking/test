package com.mm.cbk.stationary.form;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "unit_measurement")
@Data
public class UnitMeasurement {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "unit_id", columnDefinition = "serial")
	private int id;

	@Column(name = "unit_name")
	private String unitName;

	@Column(name = "pack_level")
	private String packLevel;
}
