package com.mm.cbk.stationary.form;

import lombok.Data;

@Data
public class UserLoginRequestForm {

	public String loginId;

	public String password;

	public String gcmId;

}