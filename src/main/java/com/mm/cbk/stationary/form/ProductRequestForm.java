package com.mm.cbk.stationary.form;

import lombok.Data;

@Data
public class ProductRequestForm {

//	private int id;
//	private String productName;
//	private String brandName;
//	private String caegory;
//	private String code;
//	private String color;
//	private double averagePrice;
//	private int quantity;
//	private double price;
//	private String imageStr;
//	private byte[] imageByte;
//	private double sellingPrice1;
//	private String description;
//	private String unitOfMeasurment;
	
	// private int id;
    private String productName;
    private String brandName;
    private String caegory;
    private String subcaegory;
    private String code;
    // private String color;
    // private double averagePrice;
    private int quantity;
    // private double price;
    private String price;
    private String pieces;
    // private String imageStr;
    // private byte[] imageByte;
    // private double sellingPrice1;
    // private String description;
    private String unitOfMeasurment;
}
