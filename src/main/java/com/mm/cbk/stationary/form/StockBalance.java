package com.mm.cbk.stationary.form;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "stock_balance")
public class StockBalance implements Serializable{
	
	private static final long serialVersionUID = 8402402131769349082L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name = "stock_id")
	private Stock stock;	

	@Column(name = "purchaseDate")
    private Date purchaseDate;

	@Column(name = "balance")
    private Integer balance;

    @Column(name = "UpdatedDate")
    private Date updatedDate;
}
