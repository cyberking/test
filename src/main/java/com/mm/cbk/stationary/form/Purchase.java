package com.mm.cbk.stationary.form;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "purchase")
public class Purchase implements Serializable {
	private static final long serialVersionUID = 2305912203437346946L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name = "stock_id")
	private Stock stock;	

	@Column(name = "purchaseDate")
    private Date purchaseDate;
	
	@Column(name = "purchase_price")
	private Double purchasePrice;

	@Column(name = "average_price")
	private Double averagePrice;

	@Column(name = "quantity_large")
	private Integer quantityLarge;

	@Column(name = "quantity_medium")
	private Integer quantityMedium;

	@Column(name = "quantity_small")
	private Integer quantitySmall;

	@Column(name = "total_qty")
	private Integer totalQty;
	
	@Column(name = "description")
	private String description;

	@Column(name = "exchange_rate")
	private Integer exchangeRate;

	@ManyToOne
	@JoinColumn(name = "currency_id")
	private Currency currency;

}
