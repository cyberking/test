package com.mm.cbk.stationary.form;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Table(name = "customer_level")
@Entity
@Data
public class CustomerLevel {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "level_id", columnDefinition = "serial")
	private int id;

	@Column(name = "level")
	private int level;

	@Column(name = "level_name")
	private String levelName;
}
