package com.mm.cbk.stationary.form;

import lombok.Data;

@Data
public class UserRequestForm {

	private String customerName;

	private String customerPhone;

	private String customerEmail;

	private String customerPassword;

	private String customerAddress;

	private String customerType;

	private String location;

	private String gcmId;
}
