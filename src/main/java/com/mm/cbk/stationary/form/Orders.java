package com.mm.cbk.stationary.form;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import lombok.Data;

@Data
@Entity
@Table(name = "orders")
public class Orders implements Serializable {

	private static final long serialVersionUID = -1469752425424576863L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "customer_id")
	private Customer customer;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private Users user;

	@Column(name = "delivery_address")
	private String deliveryAddress;

	@Column(name = "order_status")
	private Integer orderStatus;

	@Column(name = "selling_price")
	private Double sellingPrice;// amount for whole order

	@Column(name = "discount_amount")
	private Double discountAmount;

	// @Temporal(TemporalType.TIMESTAMP)
	// @Column(name = "order_date", columnDefinition = "TIMESTAMP DEFAULT
	// CURRENT_TIMESTAMP")
	@Column(name = "order_date")
	private Date orderDate;

	// Start Modified by AungPhyo

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "delivery_date", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date deliveryDate;

	// End Modified by AungPhyo

	@Transient
	private List<OrderRecordForm> orderRecordFormList;
}
