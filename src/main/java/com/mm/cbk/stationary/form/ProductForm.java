package com.mm.cbk.stationary.form;

import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

@Data
public class ProductForm {	

	private int id;

	private String codeNumber;

	private String productName;

	private int categoryId;
	
	private String subCategory;

	private String brandName;

	private String color;

	private double sellingPrice1;

	private double sellingPrice2;

	private double sellingPrice3;
	
	private double sellingPrice4;
	
	private double averagePrice;
	
	private double purchasePrice;
	
	private int quantityLarge;
	
	private int quantityMedium;
	
	private int quantitySmall;
	
	private int packingMedium;
	
	private int packingSmall;
	
	private String unitMeasurement;

	private String imageString;

	private String description;

	private String quality;

	private transient List<MultipartFile> productImage;
	
	private MultipartFile stockImage;
	
	private byte[] imageByte;
	
	private String imageDataString;
	
	private int currencyId;
	
	private int exchangeRate;
	
	private String reorderLevel;
	
	//private String prevCodeNo;//use to clear purchasePrice when changing codeNo after using autocomplete
	
	//search
	private String categoryName;
	private String currencyName;
	
	public ProductForm(Product product) {
		id = product.getid();
		codeNumber = product.getcodeNumber();
		productName = product.getproductName();
		categoryId = product.getCategory().getId();
		categoryName = product.getCategory().getName();
		subCategory = product.getsubCategory();
		brandName = product.getbrandName();
		color = product.getcolor();
		averagePrice = product.getaveragePrice();
		purchasePrice = product.getPurchasePrice();
		sellingPrice1 = product.getsellingPrice1();
		sellingPrice2 = product.getsellingPrice2();
		sellingPrice3 = product.getsellingPrice3();
		sellingPrice4 = product.getSellingPrice4();
		description = product.getdescription();
		quantityLarge = product.getQuantityLarge();
		quantityMedium = product.getQuantityMedium();
		quantitySmall = product.getQuantitySmall();
		packingMedium = product.getPackingMedium();
		packingSmall = product.getPackingSmall();
		unitMeasurement = product.getUnitMeasurement();
		currencyId = product.getCurrency().getId();
		currencyName = product.getCurrency().getName();
		if (product.getimageByte() != null) {			
			String imgString = "data:image/png;base64,".concat(Base64.encodeBase64String(product.getimageByte()));
			product.setimageString(imgString);
			imageDataString = imgString;
		}
		imageByte = product.getimageByte();
	}

	public ProductForm() {
		// do nothing
	}
}