package com.mm.cbk.stationary.form;

import lombok.Data;

@Data
public class CustomerForm {

	private Integer customerId;

	private String customerName;

	private String shopName;

	private String township;

	private String address;

	private String userId;

	private String loginId;

	private String phoneNumber1;

	private String phoneNumber2;

	private String phoneNumber3;

	private String email1;

	private String email2;

	private int level;
}
