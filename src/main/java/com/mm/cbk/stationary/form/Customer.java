package com.mm.cbk.stationary.form;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;

@Data
@Entity
@Table(name = "customers")
public class Customer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "customer_id", columnDefinition = "serial")
	private Integer customerId;

	@Column(name = "customer_name")
	private String customerName;

	@Column(name = "shop_name")
	private String shopName;

	@Column(name = "township")
	private String township;

	@Column(name = "address")
	private String address;

	// @OneToOne(optional= true)
	// @JoinTable(name="user_id")
	@Transient
	private Users users;

	@Column(name = "user_id")
	private String userId;

	@Column(name = "login_id")
	private String loginId;

	@Column(name = "phone_number1")
	private String phoneNumber1;

	@Column(name = "phone_number2")
	private String phoneNumber2;

	@Column(name = "phone_number3")
	private String phoneNumber3;

	@Column(name = "email1")
	private String email1;

	@Column(name = "email2")
	private String email2;

	@ManyToOne
	@JoinColumn(name = "level_id")
	private CustomerLevel customerLevel;

	public Customer() {
		//do nothing
	}

	public Customer(CustomerForm customerForm) {
		this.customerId = customerForm.getCustomerId();
		this.customerName = customerForm.getCustomerName();
		this.shopName = customerForm.getShopName();
		this.email1 = customerForm.getEmail1();
		this.email2 = customerForm.getEmail2();
		this.phoneNumber1 = customerForm.getPhoneNumber1();
		this.phoneNumber2 = customerForm.getPhoneNumber2();
		this.phoneNumber3 = customerForm.getPhoneNumber3();
		this.address = customerForm.getAddress();
		this.township = customerForm.getTownship();
	}
}
