package com.mm.cbk.stationary.form;

import java.text.SimpleDateFormat;

import org.apache.commons.codec.binary.Base64;
import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

@Data
public class StockForm {

	private Integer id;

	private String codeNumber;

	private String stockName;

	private Integer categoryId;
	
	private String subCategory;

	private String brandName;

	private String color;

	private Double sellingPrice1;

	private Double sellingPrice2;

	private Double sellingPrice3;
	
	private Double sellingPrice4;
	
	private Double averagePrice;
	
	private Integer packingMedium;
	
	private Integer packingSmall;
	
	private String unitMeasurement;

	private String imageString;

	private String description;

	private Integer totalQty;
	
	private MultipartFile stockImage;
	
	private byte[] imageByte;
	
	private String imageDataString;
	
	private Integer currencyId;
	
	private Integer exchangeRate;
	
	//search
	private String categoryName;
	private String currencyName;
	
	private String oldCodeNumber;
	
	
	private String updatedDate;
	
	public StockForm() {
		super();
	}
	
	public StockForm(Stock stock) {
		id = stock.getId();
		codeNumber = stock.codeNumber;
		oldCodeNumber = stock.codeNumber;
		stockName = stock.stockName;
		categoryId = stock.getCategory().getId();
		categoryName = stock.getCategory().getName();
		subCategory = stock.getSubCategory();
		brandName = stock.getBrandName();
		color = stock.getColor();
		averagePrice = stock.getAveragePrice() == null ? 0 : stock.getAveragePrice();
		sellingPrice1 = stock.getSellingPrice1();
		sellingPrice2 = stock.getSellingPrice2();
		sellingPrice3 = stock.getSellingPrice3();
		sellingPrice4 = stock.getSellingPrice4();
		description = stock.getDescription();
		packingMedium = stock.getPackingMedium();
		packingSmall = stock.getPackingSmall();
		unitMeasurement = stock.getUnitMeasurement();
		totalQty = stock.getTotalBalance() == null ? 0 : stock.getTotalBalance();
		if (stock.getImageByte() != null) {			
			String imgString = "data:image/png;base64,".concat(Base64.encodeBase64String(stock.getImageByte()));
			stock.setImageString(imgString);
			imageDataString = imgString;
		}
		imageByte = stock.getImageByte();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
		updatedDate = formatter.format(stock.getUpdatedDate());
		
	}
	
	public void setStockForm(Stock stock) {
		id = stock.getId();
		codeNumber = stock.codeNumber;
		oldCodeNumber = stock.codeNumber;
		stockName = stock.stockName;
//		categoryId = stock.getCategory()== null ? 1: stock.getCategory().getId();
//		categoryName = stock.getCategory().getName();
		subCategory = stock.getSubCategory();
		brandName = stock.getBrandName();
		color = stock.getColor();
		averagePrice = stock.getAveragePrice() == null ? 0 : stock.getAveragePrice();
		sellingPrice1 = stock.getSellingPrice1();
		sellingPrice2 = stock.getSellingPrice2();
		sellingPrice3 = stock.getSellingPrice3();
		sellingPrice4 = stock.getSellingPrice4();
		description = stock.getDescription();
		packingMedium = stock.getPackingMedium();
		packingSmall = stock.getPackingSmall();
		unitMeasurement = stock.getUnitMeasurement();
		totalQty = stock.getTotalBalance() == null ? 0 : stock.getTotalBalance();
		if (stock.getImageByte() != null) {			
			String imgString = "data:image/png;base64,".concat(Base64.encodeBase64String(stock.getImageByte()));
			stock.setImageString(imgString);
			imageDataString = imgString;
		}
		imageByte = stock.getImageByte();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
		updatedDate = formatter.format(stock.getUpdatedDate());
		
	}

	public Stock toStock() {
		Stock stock = new Stock();
		stock.setId(id);
		stock.setAveragePrice(averagePrice == null ? 0.0 : averagePrice);
		stock.setBrandName(brandName);
		stock.setStockName(stockName);
		
		return null;
	}
	
	
}
