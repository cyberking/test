package com.mm.cbk.stationary.form;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class OrderConfirmForm implements Serializable{
	
	private static final long serialVersionUID = -8095789916634033966L;	
	
	private Integer orderId;
	private Integer userId;
	private String userName;
	private Integer customerId;
	private String customerName;
	private String deliveryAddress;
	private Integer orderStatus;
	private String orderStatusDesc;
	private Double discountAmount;
	private String orderDateStr;
	private List<OrderRecordForm> orderRecordList;

}
