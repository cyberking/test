package com.mm.cbk.stationary.form;

import lombok.Data;

@Data
public class UsersForm {

	private String name;

	private String loginId;
	
	private String password;

	private String confirmPassword;

	private String oldPassword;

	private int roleId;
}
