package com.mm.cbk.stationary.form;

import java.io.Serializable;
import java.util.List;

import com.mm.cbk.stationary.utils.CommonConstant;
import com.mm.cbk.stationary.utils.CommonUtil;

import lombok.Data;

@Data
public class OrderForm implements Serializable {

	private static final long serialVersionUID = 555352867377446975L;
	private Integer id;
	private Integer userId;
	private String userName;
	private Integer customerId;
	private String customerName;
	private String deliveryAddress;
	private Integer orderStatus;
	private String orderStatusDesc;
	private Double discountAmount;
	private String orderDateStr;
	private boolean isConfirmOK;
	private String deliveryDate;
	private String unitMeasurement;

	private List<OrderRecordForm> orderRecordFormList;

	public OrderForm() {
	}

	public OrderForm(Orders entity) {
		id = entity.getId();
		userId = entity.getUser().getId();
		userName = entity.getUser() == null ? "" : entity.getUser().getName();
		customerId = entity.getCustomer().getCustomerId();
		customerName = entity.getCustomer() == null ? "" : entity.getCustomer().getCustomerName();
		deliveryAddress = entity.getDeliveryAddress();
		orderStatus = entity.getOrderStatus();
		orderStatusDesc = CommonConstant.OrderStatus.getDescription(entity.getOrderStatus());
		discountAmount = entity.getDiscountAmount() == null ? 0.0 : entity.getDiscountAmount();
		orderDateStr = entity.getOrderDate() == null ? ""
				: CommonUtil.dateToString(entity.getOrderDate(), CommonConstant.STANDARD_DATE_TIME_FORMAT);
		// Start Modified by AungPhyo
		deliveryDate = entity.getDeliveryDate() == null ? ""
				: CommonUtil.dateToString(entity.getDeliveryDate(), CommonConstant.STANDARD_DATE_FORMAT);
		// End Modified by AungPhyo
	}

}
