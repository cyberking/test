package com.mm.cbk.stationary.form;

import java.io.Serializable;
import java.util.List;

import com.mm.cbk.stationary.utils.CommonConstant;
import com.mm.cbk.stationary.utils.CommonUtil;

import lombok.Data;

@Data
public class OrderRecordForm implements Serializable {
	private static final long serialVersionUID = -8390500630078870956L;
	private Integer id;
	private Integer orderId;
	private Integer stockId;
	private Integer orderStatus;
	private String orderStatusDesc;
	private Double orderRecDiscAmt;//discount amount for each order record by stockId
	private String orderDateStr;
	private String updatedDateStr;
	private Integer qtyL;
	private Integer qtyM;
	private Integer qtyS;
	private Integer deliveryQtyL;
	private Integer deliveryQtyM;
	private Integer deliveryQtyS;
	private Double sellingPrice;//selling amount for each order record by stockId
	private Integer packingL;
	private Integer packingM;
	private Integer totalOrderQty;
	private Integer orderQty;
	private Integer confirmOk;//0 - OK, 1- Not OK ,to check can click Confirm button or not when target stock balance is not enough
	//stock data
	private String codeNumber;
	private String stockName;
	private String categoryName;
	private String subCategory;
	private String brandName;
	private String color;
	private Integer totalStockBalance;//stock remaining balance
	private Integer stockPackM;
	private Integer stockPackS;
	private Orders orders;
	//order data
	private Integer userId;
	private String userName;
	private Integer customerId;
	private String customerName;
	private String deliveryAddress;
	private String ordersStatusDesc;
	private Double discAmt;//whole order discount amount
	private String ordersDateStr;
	
	private Double totalAmt;
	private List<OrderRecordForm> orderRecordList;
	private String deliveryDate;
	private String unitMeasurement;
	public OrderRecordForm() {
	}

	public OrderRecordForm(OrderRecord entity) {
		//for order record
		id = entity.getId();
		orderId = entity.getOrder().getId();
		stockId = entity.getStock().getId();
		stockPackM = entity.getStock().getPackingMedium() == null ? 0 : entity.getStock().getPackingMedium();
		stockPackS = entity.getStock().getPackingSmall() == null ? 0 : entity.getStock().getPackingSmall();
		orderStatus = entity.getOrderStatus();
		orderStatusDesc = CommonConstant.OrderStatus.getDescription(entity.getOrderStatus());
		orderRecDiscAmt = entity.getDiscountAmount() == null ? 0.0 : entity.getDiscountAmount();
		orderDateStr = entity.getOrderDate() == null ? ""
				: CommonUtil.dateToString(entity.getOrderDate(), CommonConstant.STANDARD_DATE_TIME_FORMAT);
		qtyL = entity.getQtyLarge() == null ? 0 : entity.getQtyLarge();
		qtyM = entity.getQtyMedium() == null ? 0 : entity.getQtyMedium();
		qtyS = entity.getQtySmall() == null ? 0 : entity.getQtySmall();
		deliveryQtyL = entity.getDeliveryQtyLarge() == null ? 0 : entity.getDeliveryQtyLarge();
		deliveryQtyM = entity.getDeliveryQtyMedium() == null ? 0 : entity.getDeliveryQtyMedium();
		deliveryQtyS = entity.getDeliveryQtySmall() == null ? 0 : entity.getDeliveryQtySmall();
		sellingPrice = entity.getPrice() == null ? 0 : entity.getPrice();
		packingL =entity.getPackingLarge() == null ? 0 : entity.getPackingLarge();
		packingM = entity.getPackingMedium() == null ? 0 : entity.getPackingMedium();
		codeNumber = entity.getStock() == null ? "" : entity.getStock().getCodeNumber();
		stockName = entity.getStock() == null ? "" : entity.getStock().getStockName();
		subCategory = entity.getStock() == null ? "" : entity.getStock().getSubCategory();
		categoryName = entity.getStock() == null ? "" : entity.getStock().getCategory().getName();
		brandName = entity.getStock() == null ? "" : entity.getStock().getBrandName();
		color = entity.getStock() == null ? "" : entity.getStock().getColor();
		totalStockBalance = entity.getStock() == null ? 0 : entity.getStock().getTotalBalance();
		unitMeasurement = entity.getStock() == null ? "" :entity.getStock().getUnitMeasurement();
	}

}
