package com.mm.cbk.stationary.form;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "order_record")
public class OrderRecord implements Serializable {

	private static final long serialVersionUID = -6901283765446357883L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "order_id")
	private Orders order;

	@ManyToOne
	@JoinColumn(name = "stock_id")
	private Stock stock;

	@Column(name = "order_status")
	private Integer orderStatus;

	@Column(name = "order_date")
	private Date orderDate;

	@Column(name = "updated_date")
	private Date updatedDate;

	@Column(name = "qty_large")
	private Integer qtyLarge;

	@Column(name = "qty_medium")
	private Integer qtyMedium;

	@Column(name = "qty_small")
	private Integer qtySmall;

	@Column(name = "price")
	private Double price;// actual selling price

	@Column(name = "discount_amount")
	private Double discountAmount;

	@Column(name = "delivery_qty_large")
	private Integer deliveryQtyLarge;

	@Column(name = "delivery_qty_medium")
	private Integer deliveryQtyMedium;

	@Column(name = "delivery_qty_small")
	private Integer deliveryQtySmall;
	
	@Column(name = "packing_large")
	private Integer packingLarge;

	@Column(name = "packing_medium")
	private Integer packingMedium;
}
