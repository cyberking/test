package com.mm.cbk.stationary.form;

import lombok.Data;

@Data
public class CustomerRequestForm {

	private String shopName;
	private String loginName;
	private String password;
	private String key;
}
