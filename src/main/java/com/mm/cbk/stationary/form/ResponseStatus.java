package com.mm.cbk.stationary.form;

import lombok.Data;

@Data
public class ResponseStatus {

	private String status;

}
