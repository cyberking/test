package com.mm.cbk.stationary.form;

import lombok.Data;

@Data
public class ProductForm2 {
	private int id;

	private String codeNumber;

	private String productName;

	private String category;
	
	private String subCategory;

	private String brandName;

	private String color;

	private String sellingPrice1;

	private String sellingPrice2;

	private String sellingPrice3;
	
	private String averagePrice;
	
	private String pieces;

	private String quantity;
	
	private String unitMeasurement;

	private String imageString;

	private String description;

	private String quality;

	private byte[] imageByte;

	// @Column(name = "phone")
	// private String phoneNo;
	//
	// @Column(name = "promotion")
	// private int promotion;
}