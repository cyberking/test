package com.mm.cbk.stationary.form;

import org.apache.commons.codec.binary.Base64;
import org.springframework.web.multipart.MultipartFile;

import com.mm.cbk.stationary.utils.CommonConstant;
import com.mm.cbk.stationary.utils.CommonUtil;

import lombok.Data;

@Data
public class PurchaseForm {
	
	private Integer id;	
	
	private Integer quantityLarge;

	private Integer quantityMedium;

	private Integer quantitySmall;

	private Double purchasePrice;
	
	private Integer currencyId;

	private Integer exchangeRate;
	
	private Double averagePrice;
	
	private String purchaseDateStr;
	
	private String prevPurchaseDateStr;
	
	// stock data
	private Integer stockId;
	
	private String codeNumber;

	private String stockName;

	private Integer categoryId;

	private String subCategory;

	private String brandName;

	private String color;

	private Double sellingPrice1;

	private Double sellingPrice2;

	private Double sellingPrice3;

	private Double sellingPrice4;	

	private Integer packingMedium;

	private Integer packingSmall;

	private String unitMeasurement;

	private String imageString;

	private String description;

	private Integer totalQty;

	private MultipartFile stockImage;

	private byte[] imageByte;

	private String imageDataString;
	
	// search
	private String categoryName;
	
	private String currencyName;

	private String oldCodeNumber;

	public PurchaseForm() {
		super();
	}
	
	public PurchaseForm(Purchase entity){
		id = entity.getId();
		stockId = entity.getStock().getId();
		codeNumber = entity.getStock().codeNumber;
		oldCodeNumber = entity.getStock().codeNumber;
		stockName = entity.getStock().stockName;
		categoryId = entity.getStock().getCategory().getId();
		categoryName = entity.getStock().getCategory().getName();
		subCategory = entity.getStock().getSubCategory();
		brandName = entity.getStock().getBrandName();
		color = entity.getStock().getColor();
		purchasePrice = entity.getPurchasePrice() == null ? 0.0 : entity.getPurchasePrice();
		averagePrice = entity.getAveragePrice() == null ? 0.0 : entity.getAveragePrice();
		sellingPrice1 = entity.getStock().getSellingPrice1() == null ? 0.0 : entity.getStock().getSellingPrice1();
		sellingPrice2 = entity.getStock().getSellingPrice2() == null ? 0.0 : entity.getStock().getSellingPrice2();
		sellingPrice3 = entity.getStock().getSellingPrice3() == null ? 0.0 : entity.getStock().getSellingPrice3();
		sellingPrice4 = entity.getStock().getSellingPrice4() == null ? 0.0 : entity.getStock().getSellingPrice4();		
		packingMedium = entity.getStock().getPackingMedium() == null ? 0 :  entity.getStock().getPackingMedium();
		packingSmall = entity.getStock().getPackingSmall() == null ? 0 :  entity.getStock().getPackingSmall();
		unitMeasurement = entity.getStock().getUnitMeasurement();
		
		quantityLarge = entity.getQuantityLarge() == null ? 0 :  entity.getQuantityLarge();
		quantityMedium = entity.getQuantityMedium() == null ? 0 :  entity.getQuantityMedium();
		quantitySmall = entity.getQuantitySmall() == null ? 0 :  entity.getQuantitySmall();
		exchangeRate = entity.getExchangeRate() == null ? 0 : entity.getExchangeRate();
		description = entity.getDescription();		
		currencyId = entity.getCurrency().getId();
		currencyName = entity.getCurrency().getName();
		if(entity.getPurchaseDate() != null)
		{
			purchaseDateStr = CommonUtil.dateToString(entity.getPurchaseDate(), CommonConstant.STANDARD_DATE_FORMAT);
			prevPurchaseDateStr = purchaseDateStr;
		}
		if (entity.getStock().getImageByte() != null) {			
			String imgString = "data:image/png;base64,".concat(Base64.encodeBase64String(entity.getStock().getImageByte()));
			entity.getStock().setImageString(imgString);
			imageDataString = imgString;
		}
		imageByte = entity.getStock().getImageByte();
		totalQty = entity.getTotalQty() == null ? 0 : entity.getTotalQty();
	}
}
