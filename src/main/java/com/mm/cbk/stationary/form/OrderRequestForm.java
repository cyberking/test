package com.mm.cbk.stationary.form;

import java.util.List;

import lombok.Data;

@Data
public class OrderRequestForm {

	private String shopName;
	private String loginId;
	private String location;
	private List<Stock> stockList;
}
