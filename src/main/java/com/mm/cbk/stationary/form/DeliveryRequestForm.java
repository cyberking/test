package com.mm.cbk.stationary.form;

import lombok.Data;

@Data
public class DeliveryRequestForm {

	private int userId;
	private String userName;
	private String rank;
	private String department;
	private String phone;
	private String address;
	private String currentLocation;
	private String registrationDate;
}
