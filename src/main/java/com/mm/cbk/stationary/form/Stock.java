package com.mm.cbk.stationary.form;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Table(name = "stock")
public class Stock implements Serializable{

	public static final long serialVersionUID = 201421432259100066L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Integer id;

	@Column(name = "code")
	public String codeNumber;

	@Column(name = "stock_name")
	public String stockName;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "category_id")
	public Category category;

	@Column(name = "sub_category")
	public String subCategory;

	@Column(name = "brand")
	public String brandName;

	@Column(name = "color")
	public String color;

	@Column(name = "unit_measurement")
	public String unitMeasurement;

	@Column(name = "total_balance")
	public Integer totalBalance;

	@Column(name = "average_price")
	public Double averagePrice;

	@Column(name = "price1")
	public Double sellingPrice1;

	@JsonIgnore
	@Column(name = "price2")
	public Double sellingPrice2;

	@JsonIgnore
	@Column(name = "price3")
	public Double sellingPrice3;

	@JsonIgnore
	@Column(name = "price4")
	public Double sellingPrice4;

	@Column(name = "description")
	public String description;

	@Column(name = "packing_medium")
	public Integer packingMedium;

	@Column(name = "packing_small")
	public Integer packingSmall;
	
	@Transient
	private Integer qty;
	
	@Transient
	private Integer pcsQty;

	@Column(name = "image_byte_array")
	public byte[] imageByte;

	@Column(name = "image_str")
	public String imageString;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_date", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date updatedDate;

	@Transient
	private String categoryName;

	public Stock() {
		// do nothing
	}
	
	/**
	 * @param id
	 * @param codeNumber
	 * @param stockName
	 * @param category
	 * @param subCategory
	 * @param brandName
	 * @param color
	 * @param unitMeasurement
	 * @param totalBalance
	 * @param averagePrice
	 * @param sellingPrice1
	 * @param description
	 * @param packingMedium
	 * @param packingSmall
	 * @param imageByte
	 * @param updatedDate
	 */
	public Stock(Integer id, String codeNumber, String stockName, String categoryName, String subCategory,
			String brandName, String color, String unitMeasurement, Integer totalBalance, Double averagePrice,
			Double sellingPrice1, String description, Integer packingMedium, Integer packingSmall, byte[] imageByte,
			Date updatedDate) {
		this.id = id;
		this.codeNumber = codeNumber;
		this.stockName = stockName;
		this.categoryName = categoryName;
		this.subCategory = subCategory;
		this.brandName = brandName;
		this.color = color;
		this.unitMeasurement = unitMeasurement;
		this.totalBalance = totalBalance;
		this.averagePrice = averagePrice;
		this.sellingPrice1 = sellingPrice1;
		this.description = description;
		this.packingMedium = packingMedium;
		this.packingSmall = packingSmall;
		this.imageByte = imageByte;
		this.updatedDate = updatedDate;
	}
}
