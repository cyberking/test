package com.mm.cbk.stationary.form;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

@Data
@Entity
@Table(name = "product")
public class Product implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// public Product(int id, String productName, String brandName, String
	// category, String codeNumber, String color,
	// double averagePrice, int quantity, int qtyL, int qtyM, int qtyS, String
	// imageString, byte[] imageByte,
	// double sellingPrice1, String description, String unitMeasurement) {
	// super();
	// this.id = id;
	// this.productName = productName;
	// this.averagePrice = averagePrice;
	// this.sellingPrice1 = sellingPrice1;
	// this.category = category;
	// this.brandName = brandName;
	// this.quantity = quantity;
	// this.description = description;
	// this.codeNumber = codeNumber;
	// this.imageString = imageString;
	// this.imageByte = imageByte;
	// this.color = color;
	// this.qtyL = qtyL;
	// this.qtyM = qtyM;
	// this.qtyS = qtyS;
	// }
	//
	// public Product(int id, String productName, String brandName, String
	// category, String codeNumber, String color,
	// double averagePrice, int quantity, int qtyL, int qtyM, int qtyS, String
	// imageString, byte[] imageByte,
	// double sellingPrice1, String description, String unitMeasurement, String
	// subCategory) {
	// super();
	// this.id = id;
	// this.productName = productName;
	// this.averagePrice = averagePrice;
	// this.sellingPrice1 = sellingPrice1;
	// this.category = category;
	// this.brandName = brandName;
	// this.quantity = quantity;
	// this.description = description;
	// this.codeNumber = codeNumber;
	// this.imageString = imageString;
	// this.imageByte = imageByte;
	// this.color = color;
	// this.qtyL = qtyL;
	// this.qtyM = qtyM;
	// this.qtyS = qtyS;
	// this.subCategory = subCategory;
	// }
	//
	// public Product(int id, String productName, double averagePrice, double
	// sellingPrice1, double sellingPrice2,
	// double sellingPrice3, double balance, String reorderLevel, String
	// category, String subCategory,
	// String brandName, String unitMeasurement, int quantity, String
	// description, String codeNumber,
	// String imageString, byte[] imageByte, String color, int qtyL, int qtyM,
	// int qtyS,
	// MultipartFile productImage) {
	// super();
	// this.id = id;
	// this.productName = productName;
	// this.averagePrice = averagePrice;
	// this.sellingPrice1 = sellingPrice1;
	// this.sellingPrice2 = sellingPrice2;
	// this.sellingPrice3 = sellingPrice3;
	// this.balance = balance;
	// this.reorderLevel = reorderLevel;
	// this.category = category;
	// this.subCategory = subCategory;
	// this.brandName = brandName;
	// this.quantity = quantity;
	// this.description = description;
	// this.codeNumber = codeNumber;
	// this.imageString = imageString;
	// this.imageByte = imageByte;
	// this.color = color;
	// this.qtyL = qtyL;
	// this.qtyM = qtyM;
	// this.qtyS = qtyS;
	// this.productImage = productImage;
	// }

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "product_id")
	public int id;

	@Column(name = "code")
	public String codeNumber;
	
	@Column(name = "product_name")
	public String productName;
	
	@ManyToOne
	@JoinColumn(name = "category_id")
	public Category category;
	
	@Column(name = "sub_category")
	public String subCategory;

	@Column(name = "brand")
	public String brandName;
	
	@Column(name = "color")
	public String color;

	@Column(name = "unit_measurement")
	public String unitMeasurement;
	
	@Column(name = "purchase_price")
	public double purchasePrice;

	@Column(name = "average_price")
	public double averagePrice;

	@Column(name = "price1")
	public double sellingPrice1;

	@Column(name = "price2")
	public double sellingPrice2;

	@Column(name = "price3")
	public double sellingPrice3;

	@Column(name = "price4")
	public double sellingPrice4;

	@Column(name = "reorder_level")
	public String reorderLevel;	

	@Column(name = "quantity_large")
	public int quantityLarge;

	@Column(name = "quantity_medium")
	public int quantityMedium;

	@Column(name = "quantity_small")
	public int quantitySmall;

	@Column(name = "description")
	public String description;

	@Column(name = "image_str")
	public String imageString;

	@Column(name = "image_byte_array")
	public byte[] imageByte;

	@Column(name = "exchange_rate")
	public int exchangeRate;

	@ManyToOne
	@JoinColumn(name = "currency_id")
	public Currency currency;

	@Column(name = "packing_medium")
	public int packingMedium;

	@Column(name = "packing_small")
	public int packingSmall;

	public transient MultipartFile productImage;

	public Product() {
	}

	public int getid() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getproductName() {
		return productName;
	}

	public void setproductName(String productName) {
		this.productName = productName;
	}

	public double getaveragePrice() {
		return averagePrice;
	}

	public void setaveragePrice(double averagePrice) {
		this.averagePrice = averagePrice;
	}

	public double getsellingPrice1() {
		return sellingPrice1;
	}

	public void setsellingPrice1(double sellingPrice1) {
		this.sellingPrice1 = sellingPrice1;
	}

	public double getsellingPrice2() {
		return sellingPrice2;
	}

	public void setsellingPrice2(double sellingPrice2) {
		this.sellingPrice2 = sellingPrice2;
	}

	public double getsellingPrice3() {
		return sellingPrice3;
	}

	public void setsellingPrice3(double sellingPrice3) {
		this.sellingPrice3 = sellingPrice3;
	}

	public String getreorderLevel() {
		return reorderLevel;
	}

	public void setreorderLevel(String reorderLevel) {
		this.reorderLevel = reorderLevel;
	}

	public String getsubCategory() {
		return subCategory;
	}

	public void setsubCategory(String subCategory) {
		this.subCategory = subCategory;
	}

	public String getbrandName() {
		return brandName;
	}

	public void setbrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getdescription() {
		return description;
	}

	public void setdescription(String description) {
		this.description = description;
	}

	public String getcodeNumber() {
		return codeNumber;
	}

	public void setcodeNumber(String codeNumber) {
		this.codeNumber = codeNumber;
	}

	public String getimageString() {
		return imageString;
	}

	public void setimageString(String imageString) {
		this.imageString = imageString;
	}

	public byte[] getimageByte() {
		return imageByte;
	}

	public void setimageByte(byte[] imageByte) {
		this.imageByte = imageByte;
	}

	public String getcolor() {
		return color;
	}

	public void setcolor(String color) {
		this.color = color;
	}

	public MultipartFile getproductImage() {
		return productImage;
	}

	public void setproductImage(MultipartFile productImage) {
		this.productImage = productImage;
	}

	public Product(int id, String productName, String brandName, int category, String codeNumber, String color,
			double averagePrice, int quantity, String imageString, byte[] imageByte, double sellingPrice1,
			String description, String unitMeasurement, String subCategory) {
		this.id = id;
		this.productName = productName;
		this.brandName = brandName;
		if (category > 0) {
			Category cate = new Category();
			cate.id = category;
			this.category = cate;
		}
		this.codeNumber = codeNumber;
		this.color = color;
		this.averagePrice = averagePrice;
		this.quantityLarge = quantity;
		this.imageString = imageString;
		this.imageByte = imageByte;
		this.sellingPrice1 = sellingPrice1;
		this.description = description;
		this.unitMeasurement = unitMeasurement;
		this.subCategory = subCategory;
	}
}