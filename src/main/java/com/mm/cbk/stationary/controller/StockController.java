package com.mm.cbk.stationary.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mm.cbk.stationary.form.Category;
import com.mm.cbk.stationary.form.StockForm;
import com.mm.cbk.stationary.service.CommonService;
import com.mm.cbk.stationary.service.PurchaseService;
import com.mm.cbk.stationary.service.StockService;

@Controller
public class StockController {

	@Autowired
	StockService stockService;
	
	@Autowired
	PurchaseService purchaseService;

	@Autowired
	CommonService commonService;

	/*
	 * ======== start of stock insert, update, delete , search methods ========
	 */
	@GetMapping("/viewStockList")
	public String viewStockList(Model model) {
		List<StockForm> stockList = stockService.getAllStockList();
		model.addAttribute("stockList", stockList);
		return "stockList";
	}

	@RequestMapping(value = "/stockDetail", method = RequestMethod.GET)
	public String showStockDetail(Model model, @RequestParam(value="id", required=true) String id) {
		StockForm stockForm = stockService.getStockById(Integer.parseInt(id));
		model.addAttribute("StockForm", stockForm);
		return "stockDetail";
	}
	
	@RequestMapping(value = "/viewStockRegister", method = RequestMethod.GET)
	public String viewStockRegisterk(Model model, @RequestParam(value="id", required=false) String id){
		StockForm stockForm = new StockForm();
		if(id != null)
			stockForm = stockService.getStockById(Integer.parseInt(id));		
		initStockForm(model);
		model.addAttribute("StockForm", stockForm);
		return "stockRegister";
	}
	
	@PostMapping("/registerStock")
	public String registerStock(Model model, @ModelAttribute("StockForm") StockForm stockForm) {
		boolean isUniqueCode = true;
		if (stockForm.getId() != null && stockForm.getId() > 0) {
			if (!stockForm.getCodeNumber().trim().equals(stockForm.getOldCodeNumber().trim()))
			{				
				isUniqueCode = stockService.checkDuplicateCodeNo(stockForm.getCodeNumber());
			}
		}
		else
			isUniqueCode = stockService.checkDuplicateCodeNo(stockForm.getCodeNumber());
		if (isUniqueCode) {
			stockService.manageStock(stockForm);
			if (stockForm.getId() != null && stockForm.getId() > 0) 
				model.addAttribute("successMsg", "Stock Register is updated successfully.");
			else
				model.addAttribute("successMsg", "Stock Register is finished successfully.");
			model.addAttribute("StockForm", new StockForm());
		} else {
			model.addAttribute("errorMsg", "Duplicate Code Number. Please use new code number.");
			model.addAttribute("StockForm", stockForm);
		}		
		initStockForm(model);
		return "stockRegister";
	}

	private void initStockForm(Model model) {
		List<Category> categoryList = commonService.getAllCategory();
		System.out.println(categoryList);
		model.addAttribute("categoryList", categoryList);
	}

	@RequestMapping(value = "/stockDelete", method = RequestMethod.GET)
	public String deleteStock(Model model, @RequestParam(value="id", required=true) String id) {	
		boolean status = stockService.deleteStock(Integer.parseInt(id));
		if (status)
			model.addAttribute("successMsg", "Stock record is deleted successfully.");
		else
			model.addAttribute("errorMsg", "Cannot delete stock because this stock has stock balance record.");
		List<StockForm> stockList = stockService.getAllStockList();
		model.addAttribute("stockList", stockList);
		return "stockList";
	}

	@RequestMapping(value = "/searchStockByCode.html", method = RequestMethod.GET)
	public @ResponseBody String searchStockByCode(@RequestParam(value = "code", defaultValue = "") String stkCode) {
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();
		List<StockForm> stkList = stockService.searchStockByCode(stkCode);
		return gson.toJson(stkList);
	}
	
	//======== end of stock insert, update, delete , search methods ========
}
