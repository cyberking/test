package com.mm.cbk.stationary.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mm.cbk.stationary.form.Customer;
import com.mm.cbk.stationary.form.CustomerRequestForm;
import com.mm.cbk.stationary.response.Meta;
import com.mm.cbk.stationary.response.Response;
import com.mm.cbk.stationary.response.ResponseForm;
import com.mm.cbk.stationary.service.CustomerService;

@RestController
public class CustomerRestController {

	@Autowired
	CustomerService customerService;

	@GetMapping("/getCustomerName")
	public ResponseForm getCustomerName() {
		List<Customer> customerList = customerService.getCustomerList();
		Meta metaForm = new Meta();
		Response response = new Response();
		ResponseForm responseForm = new ResponseForm();
		metaForm.setResponseCode("000");
		metaForm.setResponseMessage("Success");
		response.setCustomerList(customerList);
		responseForm.setResponse(response);
		responseForm.setMeta(metaForm);
		return responseForm;
	}
	
	@PostMapping(value = "/signUpCustomer")
	public ResponseForm registerNewCustomer(CustomerRequestForm customerForm) {
		ResponseForm responseForm = new ResponseForm();
		Response response = new Response();
		Meta metaForm = new Meta();
		Customer customer = customerService.checkSignUpCustomer(customerForm);
		if (null == customer) {
			metaForm.setResponseCode("500");
			metaForm.setResponseMessage("User doesn't exist.");
		} else {
			customer.getCustomerLevel().getLevelName();
			metaForm.setResponseCode("000");
			metaForm.setResponseMessage("Success");
		}
		responseForm.setResponse(response);
		responseForm.setMeta(metaForm);
		return responseForm;
	}
}
