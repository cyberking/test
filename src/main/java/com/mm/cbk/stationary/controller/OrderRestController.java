package com.mm.cbk.stationary.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.mm.cbk.stationary.form.Customer;
import com.mm.cbk.stationary.form.OrderForm;
import com.mm.cbk.stationary.form.OrderRequestForm;
import com.mm.cbk.stationary.response.Meta;
import com.mm.cbk.stationary.response.Response;
import com.mm.cbk.stationary.response.ResponseForm;
import com.mm.cbk.stationary.service.CustomerService;
import com.mm.cbk.stationary.service.OrderService;

@RestController
public class OrderRestController {

	@Autowired
	public OrderService orderService;

	@Autowired
	public CustomerService customerService;

	@PostMapping(value = "/orderProduct")
	public ResponseForm orderProduct(String orderRequestForm) {
		// List<ProductRequestForm> productRequestList =
		// orderForm.getProductRequestList();
		//
		// Meta metaForm = new Meta();
		// Response response = new Response();
		// ResponseForm responseForm = new ResponseForm();
		// metaForm.setResponseCode("000");
		// metaForm.setResponseMessage("Success");
		// response.setProductRequestList(productRequestList);
		// responseForm.setResponse(response);
		// return responseForm;

		Gson gson = new Gson();
		OrderRequestForm orderForm = gson.fromJson(orderRequestForm, OrderRequestForm.class);

		if (orderForm.getStockList().size() > 0) {
			orderService.insertOrderData(orderForm);
		}
		Meta metaForm = new Meta();
		Response response = new Response();
		ResponseForm responseForm = new ResponseForm();
		metaForm.setResponseCode("000");
		metaForm.setResponseMessage("Success");
		// response.setProductRequestList(productRequestList);
		responseForm.setMeta(metaForm);
		responseForm.setResponse(response);
		return responseForm;
	}

	@PostMapping(value = "/checkOrderHistory")
	public ResponseForm checkOrderHistory(String shopName, String key) {
		Customer customer = customerService.getCustomerByShopName(shopName);
		int customerId = customer.getCustomerId();
		List<OrderForm> orderFormList = orderService.getOrderHistoryWithOrderRecord(customerId);

		Meta metaForm = new Meta();
		Response response = new Response();
		response.setOrderFormList(orderFormList);

		ResponseForm responseForm = new ResponseForm();
		metaForm.setResponseCode("000");
		metaForm.setResponseMessage("Success");
		// response.setProductRequestList(productRequestList);
		responseForm.setMeta(metaForm);
		responseForm.setResponse(response); 	
		return responseForm;
	}

}
