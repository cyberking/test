package com.mm.cbk.stationary.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mm.cbk.stationary.form.UnitMeasurement;
import com.mm.cbk.stationary.response.Brand;
import com.mm.cbk.stationary.response.Meta;
import com.mm.cbk.stationary.response.Response;
import com.mm.cbk.stationary.response.ResponseForm;
import com.mm.cbk.stationary.service.StockService;

@RestController
public class StockRestController {

	@Autowired
	StockService stockService;

	@PostMapping("/getProduct")
	private ResponseForm getProductByCustomerName(@RequestParam("shopName") String shopName, @RequestParam("key") String key, @RequestParam("updatedDate") String updatedDate) {
		//TODO check date
		List<Brand> productList = stockService.getProductByShopName(shopName);
		ResponseForm responseForm = new ResponseForm();
		Response response = new Response();
		Meta metaForm = new Meta();
		metaForm.setResponseCode("000");
		metaForm.setResponseMessage("Success");
		response.setBrandList(productList);
		responseForm.setMeta(metaForm);
		responseForm.setResponse(response);
		return responseForm;
	}

	@GetMapping("/getBrandName")
	private ResponseForm getBrandName() {
		ResponseForm responseForm = new ResponseForm();
		List<String> brandNameList = stockService.getBrandNameList();
		Response response = new Response();
		List<Brand> brandList = new ArrayList<Brand>();
		for (String brandName : brandNameList) {
			Brand brand = new Brand();
			brand.setBrandName(brandName);
			brandList.add(brand);
		}
		response.setBrandNameList(brandList);
		Meta metaForm = new Meta();
		metaForm.setResponseCode("000");
		metaForm.setResponseMessage("Success");
		responseForm.setMeta(metaForm);
		responseForm.setResponse(response);
		return responseForm;
	}

	@GetMapping("/getCategoryName")
	private ResponseForm getCategoryName() {
		ResponseForm responseForm = new ResponseForm();
		Response response = new Response();
		List<String> categoryList = stockService.getCategoryList();
		response.setCategoryList(categoryList);
		Meta metaForm = new Meta();
		metaForm.setResponseCode("000");
		metaForm.setResponseMessage("Success");
		responseForm.setMeta(metaForm);
		responseForm.setResponse(response);
		return responseForm;
	}

	@PostMapping("/getAllUnitMeasurement")
	private ResponseForm getAllUnitMeasurement() {
		ResponseForm responseForm = new ResponseForm();
		Response response = new Response();

		List<UnitMeasurement> unitList = stockService.getAllUnitMeasurement();

		response.setUnitList(unitList);
		Meta metaForm = new Meta();
		metaForm.setResponseCode("000");
		metaForm.setResponseMessage("Success");
		responseForm.setMeta(metaForm);
		responseForm.setResponse(response);
		return responseForm;
	}
}
