package com.mm.cbk.stationary.controller;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.mm.cbk.stationary.form.Users;
import com.mm.cbk.stationary.form.UsersForm;
import com.mm.cbk.stationary.service.UserService;

@Controller
public class LoginController {

	@Autowired
	UserService userService;

	@GetMapping("/index")
	public String showLogin() {
		return "login";
	}

	@PostMapping("/login")
	public String login(UsersForm usersForm, Model model) {
		String password = this.getHashPassword(usersForm.getPassword());
		usersForm.setPassword(password);
		Users users = userService.getUserByNameAndPassword(usersForm.getName(), usersForm.getPassword());
		if (null == users) {
			return "redirect:/index";
		}
		return "redirect:/orderReport";
	}

	@GetMapping("/orderReport")
	public String showHome() {
		return "orderReport";
	}

	/**
	 * encrypt password with MD5
	 * 
	 * @param password
	 *            is used to change password
	 * @return encrypted password
	 */
	public String getHashPassword(String password) {
		MessageDigest md;
		String hashPwd = "";
		try {
			md = MessageDigest.getInstance("MD5");
			md.update(password.getBytes());
			byte[] digest = md.digest();
			hashPwd = DatatypeConverter.printHexBinary(digest).toUpperCase();
		} catch (NoSuchAlgorithmException algExp) {
			algExp.printStackTrace();
		}
		return hashPwd;
	}
}
