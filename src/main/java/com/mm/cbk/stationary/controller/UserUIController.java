package com.mm.cbk.stationary.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mm.cbk.stationary.form.Roles;
import com.mm.cbk.stationary.form.Users;
import com.mm.cbk.stationary.form.UsersForm;
import com.mm.cbk.stationary.service.RolesService;
import com.mm.cbk.stationary.service.UserService;

@Controller
public class UserUIController {

	@Autowired
	public UserService userService;

	@Autowired
	public RolesService rolesService;

	@Autowired
	LoginController loginController;

	@GetMapping("/showUserList")
	public String showUserList(Model model) {
		List<Users> userList = new ArrayList<Users>();
		userList = userService.getUserList();
		model.addAttribute("userList", userList);
		return "userList";
	}

	@PostMapping("/showUserRegister")
	public String showUserRegister(Model model, UsersForm usersForm) {
		List<Roles> rolesList = rolesService.getRole();
		model.addAttribute("rolesList", rolesList);
		return "userRegister";
	}

	@PostMapping("/registerUsers")
	public String registerUser(Model model, UsersForm usersForm, RedirectAttributes redirectAttr) {
		if (!usersForm.getPassword().equals(usersForm.getConfirmPassword())) {
			model.addAttribute("PasswordNotMatch", "True");
			List<Roles> rolesList = rolesService.getRole();
			model.addAttribute("rolesList", rolesList);
			return "userRegister";
		}
		String hashPwd = loginController.getHashPassword(usersForm.getPassword());
		usersForm.setPassword(hashPwd);
		userService.registerUser(usersForm);
		return "redirect:/showUserList";
	}

	@PostMapping("/deleteUsers")
	public String deleteUser(Model model, @RequestParam("userId") String userIdStr) {
		userService.deactiveUsers(userIdStr);
		return "redirect:/showUserList";
	}

	@PostMapping("/showUpdateUsers")
	public String showUpdateUser(Model model, @RequestParam("userId") String userIdStr) {
		Users user = userService.getUserById(userIdStr);
		List<Roles> rolesList = rolesService.getRole();
		model.addAttribute("user", user);
		model.addAttribute("rolesList", rolesList);
		return "userUpdate";
	}

	@PostMapping("updateUsers")
	public String updateUser(Model model, UsersForm userForm) {
		String oldPwd = loginController.getHashPassword(userForm.getOldPassword());
		String pwd = loginController.getHashPassword(userForm.getPassword());
		Users user = userService.getUserByNameAndPassword(userForm.getConfirmPassword(), oldPwd);
		if (null != user) {
			userForm.setPassword(pwd);
			userService.updateUser(userForm);
			return "redirect:/showUserList";
		}
		return "/showUpdateUsers";
	}
}
