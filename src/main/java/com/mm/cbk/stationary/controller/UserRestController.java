package com.mm.cbk.stationary.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mm.cbk.stationary.form.UserLoginRequestForm;
import com.mm.cbk.stationary.form.Users;
import com.mm.cbk.stationary.response.Meta;
import com.mm.cbk.stationary.response.Response;
import com.mm.cbk.stationary.response.ResponseForm;
import com.mm.cbk.stationary.service.UserService;

@RestController
public class UserRestController {

	@Autowired
	UserService userService;

	// @PostMapping(value = "/registerUser", headers = {
	// "Content-type=application/json" })
	// public ResponseStatus registerUser(@RequestBody String request) {
	// Gson gson = new Gson();
	// UserRequestForm userForm = new UserRequestForm();
	// userForm = gson.fromJson(request, UserRequestForm.class);
	// ResponseStatus responseStatus = new ResponseStatus();
	// responseStatus = userService.registerUser(userForm);
	// return responseStatus;
	// }

	@PostMapping(value = "/loginUser")
	public ResponseForm loginUser(UserLoginRequestForm request) {
		LoginController loginController = new LoginController();
		ResponseForm responseForm = new ResponseForm();
		String password = loginController.getHashPassword(request.getPassword());
		Users users = userService.getUserByNameAndPassword(request.getLoginId(), password);
		Meta metaForm = new Meta();
		Response response = new Response();
		if (null != users) {
			metaForm.setResponseCode("000");
			metaForm.setResponseMessage("Success");
			response.setRoleId(users.getRoleId().getRoleId() + "");
			response.setRoleName(users.getRoleId().getRoleName());
			if (null != request.getGcmId()) {
				userService.addGCMId(users.getId(), request.getGcmId());
			}
		} else {
			metaForm.setResponseCode("500");
			metaForm.setResponseMessage("User doesn't exist.");
		}
		responseForm.setMeta(metaForm);
		responseForm.setResponse(response);
		return responseForm;
	}

	@PostMapping("/addGCMId")
	public void addGCMId(UserLoginRequestForm request) {
		userService.addGCMIdByLoginId(request.getLoginId(), request.getGcmId());
	}

	// @PostMapping(value = "/registerUser")
	// public void insertUser(User user) {
	// // Gson gson = new Gson();
	// // User user = gson.fromJson(request, User.class);
	// System.out.println(user.getUserName());
	// userService.registerUser(user);
	// // System.out.println(password);
	//
	// }
}
