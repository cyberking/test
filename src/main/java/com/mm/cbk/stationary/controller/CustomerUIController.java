package com.mm.cbk.stationary.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mm.cbk.stationary.form.Customer;
import com.mm.cbk.stationary.form.CustomerForm;
import com.mm.cbk.stationary.form.CustomerLevel;
import com.mm.cbk.stationary.form.UsersForm;
import com.mm.cbk.stationary.service.CustomerService;

@Controller
public class CustomerUIController {

	@Autowired
	public CustomerService customerService;
	

	@GetMapping("/showCustomerList")
	public String showCustomerList(Model model) {
		List<Customer> customerList = new ArrayList<Customer>();
		customerList = customerService.getCustomerList();
		model.addAttribute("customerList", customerList);
		return "customerList";
	}

	@PostMapping("/showCustomerRegister")
	public String showUserRegister(Model model, UsersForm usersForm) {
		List<CustomerLevel> customerLevelList = customerService.getCustomerLevelList();
		model.addAttribute("customerLevelList", customerLevelList);
		return "customerRegister";
	}

	@PostMapping("/registerCustomer")
	public String registerCustomer(Model model, CustomerForm customerForm, RedirectAttributes redirectAttr) {
		customerService.registerCustomer(customerForm);
		return "redirect:/showCustomerList";
	}

	@PostMapping("/deleteCustomer")
	public String deleteCustomer(Model model, @RequestParam("customerId") String customerIdStr) {
		customerService.deleteCustomer(customerIdStr);
		return "redirect:/showCustomerList";
	}

	@PostMapping("/showUpdateCustomer")
	public String showUpdateCustomer(Model model, @RequestParam("customerId") String customerIdStr) {
		Customer customer = customerService.getCustomerById(customerIdStr);
		List<CustomerLevel> customerLevelList = customerService.getCustomerLevelList();
		model.addAttribute("customer", customer);
		model.addAttribute("customerLevelList", customerLevelList);
		return "customerUpdate";
	}

	@PostMapping("updateCustomer")
	public String updateUser(Model model, Customer customer) {
		if (null != customer) {
			customerService.updateCustomer(customer);
			return "redirect:/showCustomerList";
		}
		return "/showUpdateCustomer";
	}
}
