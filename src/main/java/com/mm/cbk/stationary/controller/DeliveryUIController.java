package com.mm.cbk.stationary.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DeliveryUIController {

	@GetMapping("/deliveryReport")
	public String checkDelivery(){
		return "deliveryReport";
	}
}
