//package com.mm.cbk.stationary.controller;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.mm.cbk.stationary.response.Brand;
//import com.mm.cbk.stationary.response.Meta;
//import com.mm.cbk.stationary.response.Response;
//import com.mm.cbk.stationary.response.ResponseForm;
//import com.mm.cbk.stationary.service.ProductService;
//
//@RestController
//public class ProductRestController {
//
//	@Autowired
//	ProductService productService;
//
//	@PostMapping("/getProduct")
//	private ResponseForm getProductByCustomerName(@RequestParam("customerName") String customerName) {
//		List<Brand> productList = new ArrayList<>();// productService.getProductByCustomerName(customerName);
//		ResponseForm responseForm = new ResponseForm();
//		Response response = new Response();
//		Meta metaForm = new Meta();
//		metaForm.setResponseCode("000");
//		metaForm.setResponseMessage("Success");
//		response.setBrandList(productList);
//		responseForm.setMeta(metaForm);
//		responseForm.setResponse(response);
//		return responseForm;
//	}
//
//	@GetMapping("/getBrandName")
//	private ResponseForm getBrandName() {
//		ResponseForm responseForm = new ResponseForm();
//		List<String> brandNameList = productService.getBrandNameList();
//		Response response = new Response();
//		List<Brand> brandList = new ArrayList<Brand>();
//		for (String brandName : brandNameList) {
//			Brand brand = new Brand();
//			brand.setBrandName(brandName);
//			brandList.add(brand);
//		}
//		response.setBrandNameList(brandList);
//		Meta metaForm = new Meta();
//		metaForm.setResponseCode("000");
//		metaForm.setResponseMessage("Success");
//		responseForm.setMeta(metaForm);
//		responseForm.setResponse(response);
//		return responseForm;
//	}
//}
