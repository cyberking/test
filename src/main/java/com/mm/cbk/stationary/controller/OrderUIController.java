package com.mm.cbk.stationary.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.mm.cbk.stationary.form.Orders;
import com.mm.cbk.stationary.service.OrderService;

@Controller
public class OrderUIController {

	@Autowired
	OrderService orderService;
	
	@GetMapping("/orderHistory")
	public String checkOrder() {
		return "orderHistory";
	}
	
	@PostMapping("/updateOrder")
	public String showUpdateOrder(){
		return "orderUpdate";
	}
	
	@GetMapping("/showOrderList")
	public String showOrderList(Model model) {
		
		List<Orders> orderList = new ArrayList<>();
		String customerName = "";
		String userName = "";
		String location = "";
		String dateTime = "";
		orderList = orderService.getOrderList();
		if (orderList.size() > 0) {
			customerName = orderList.get(0).getCustomer().getCustomerName();
			userName = orderList.get(0).getUser().getName();
			location = orderList.get(0).getDeliveryAddress();
			dateTime = orderList.get(0).getOrderDate().toString();
		}
		model.addAttribute("orderList", orderList);
		model.addAttribute("customerName", customerName);
		model.addAttribute("userName", userName);
		model.addAttribute("location", location);
		model.addAttribute("dateTime", dateTime);

		return "orderList";
	}
}
