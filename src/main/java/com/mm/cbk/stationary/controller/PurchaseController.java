package com.mm.cbk.stationary.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.mm.cbk.stationary.form.Currency;
import com.mm.cbk.stationary.form.PurchaseForm;
import com.mm.cbk.stationary.form.SystemSetting;
import com.mm.cbk.stationary.service.CommonService;
import com.mm.cbk.stationary.service.PurchaseService;
import com.mm.cbk.stationary.utils.CommonConstant;

@Controller
public class PurchaseController {
	
	@Autowired
	PurchaseService purchaseService;

	@Autowired
	CommonService commonService;
	
	// ======== start of stock purchase insert, update, delete , search methods
	@GetMapping("/viewStockPurchaseList")
	public String showProductList(Model model) {
		initStockPurchaseForm(model);
		model.addAttribute("PurchaseForm", new PurchaseForm());
		return "purchase";
	}

	private void initStockPurchaseForm(Model model) {
		List<Currency> currencyList = commonService.getAllCurrency();
		List<PurchaseForm> purchaseList = purchaseService.getAllPurchaseStockList();
		model.addAttribute("purchaseList", purchaseList);
		SystemSetting setting = commonService.getSettingByCode(CommonConstant.EXCHANGE_RATE);
		model.addAttribute("currencyList", currencyList);
		model.addAttribute("exchangeRateAmt", setting == null ? 0 : Integer.parseInt(setting.getValue()));
	}

	@RequestMapping(value = "/viewStockPurchase", method = RequestMethod.GET)
	public String viewStockPurchase(Model model, @RequestParam(value = "id", required = false) String id) {
		PurchaseForm form = new PurchaseForm();
		if (id != null)
			form = purchaseService.getPurchaseStockById(Integer.parseInt(id));
		model.addAttribute("PurchaseForm", form);
		return "stockPurchaseDetail";
	}

	@PostMapping("/purchaseStock")
	public String manageStockPurchase(Model model, @ModelAttribute("PurchaseForm") PurchaseForm form) {
		Integer requiredBalance = 0;
		if (form.getId() != null && form.getId() > 0) 
		{
			if (form.getPrevPurchaseDateStr() != null && form.getPurchaseDateStr() != null
					&& !form.getPrevPurchaseDateStr().equals(form.getPurchaseDateStr()))
				requiredBalance = purchaseService.checkAvalStockBalance(form);
			if (requiredBalance >= 0)
				purchaseService.updatePurchase(form);
			else {
				Integer qtyPerBox = form.getPackingMedium() == null ? 0 : form.getPackingMedium();
				Integer qtyPerCard = form.getPackingSmall() == null ? 0 : form.getPackingSmall();
				Integer box = form.getQuantityLarge() == null ? 0 : form.getQuantityLarge();
				Integer card = form.getQuantityMedium() == null ? 0 : form.getQuantityMedium();
				Integer pieces = form.getQuantitySmall() == null ? 0 : form.getQuantitySmall();
				Integer totalQty = (((box * qtyPerBox) + card) * qtyPerCard) + pieces;
				model.addAttribute("errorMsg", "The available drug balance is " + (totalQty - ((-1) * requiredBalance))
						+ ". Are you sure want to update?");
				model.addAttribute("purchaseForm", form);
			}
		} else {
			purchaseService.manageStockPurchase(form);
			model.addAttribute("PurchaseForm", new PurchaseForm());
			model.addAttribute("successMsg", "Stock Purchase data has been saved successfully.");
		}
		initStockPurchaseForm(model);
		return "purchase";
	}

	@RequestMapping(value = "/purchaseStockDelete", method = RequestMethod.GET)
	public String deletePurchaseStock(Model model, @RequestParam(value = "id", required = true) String id) {
		boolean status = purchaseService.deletePurchaseStock(Integer.parseInt(id));
		if (status)
			model.addAttribute("successMsg", "Purchase stock record is deleted successfully.");
		else
			model.addAttribute("errorMsg",
					"Cannot delete purchase stock record because stock is already used in sales.");
		initStockPurchaseForm(model);
		model.addAttribute("PurchaseForm", new PurchaseForm());
		return "purchase";
	}
}
