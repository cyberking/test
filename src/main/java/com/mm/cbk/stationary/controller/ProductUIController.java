package com.mm.cbk.stationary.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mm.cbk.stationary.form.Category;
import com.mm.cbk.stationary.form.Currency;
import com.mm.cbk.stationary.form.Product;
import com.mm.cbk.stationary.form.ProductForm;
import com.mm.cbk.stationary.form.SystemSetting;
import com.mm.cbk.stationary.service.CommonService;
import com.mm.cbk.stationary.service.ProductService;
import com.mm.cbk.stationary.utils.CommonConstant;

@Controller
public class ProductUIController {

	@Autowired
	public ProductService productService;

	@Autowired
	public CommonService commonService;
	
	String imageString;

	// byte[] imageByte;

	@GetMapping("/showProductList")
	public String showProductList(Model model) {
		List<Product> productList = new ArrayList<Product>();
		productList = productService.getProductList();
		model.addAttribute("productList", productList);
		return "productList";
	} 

	@PostMapping("/registerProducts")
	public String registerUser(Model model, @ModelAttribute("ProductForm") ProductForm productForm) {
		productService.insertProduct(productForm);
		initStockRegisterForm(model);
		return "productRegister";
	}
	
	private void initStockRegisterForm(Model model){
		List<Category> categoryList = commonService.getAllCategory();
		List<Currency> currencyList = commonService.getAllCurrency();
		List<ProductForm> productList =  productService.getProductFormList();
		SystemSetting setting = commonService.getSettingByCode(CommonConstant.EXCHANGE_RATE);
		model.addAttribute("productList", productList);
		model.addAttribute("ProductForm", new ProductForm());
		model.addAttribute("categoryList", categoryList);
		model.addAttribute("currencyList", currencyList);
		model.addAttribute("exchangeRateAmt", setting==null ? 0 : Integer.parseInt(setting.getValue()));
	}

	@GetMapping("/showStockRegister")
	public String showStockRegister(Model model) {
		initStockRegisterForm(model);
		return "productRegister";
	}

	/*@RequestMapping(value = "/stockDelete", method=RequestMethod.POST)
	public String deleteStock(Model model, @ModelAttribute("ProductForm") ProductForm productForm, @PathVariable int id) {
		productService.deleteProduct(productForm.getId());
		initStockRegisterForm(model);
		return "productRegister";
	}*/
	
	/*@RequestMapping(value = { "", "/stock/delete/{id}" })
	public String showStockDelete(Model model, @PathVariable int id) {
		productService.deleteProduct(id);
		initStockRegisterForm(model);
		return "productRegister";
	}
	
	@RequestMapping(value = "/searchStockByCode.html", method = RequestMethod.GET)
	public @ResponseBody String searchStockByCode(@RequestParam(value = "code", defaultValue = "") String stkCode) {
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();
		List<ProductForm> stkList = productService.searchStockByCode(stkCode);
		return gson.toJson(stkList);
	}
	
	@RequestMapping(value = { "", "/stock/detail/{id}" })
	public String viewStockDetail(Model model, @PathVariable int id) {
		Product product = productService.getProduct(id);
		ProductForm productForm = new ProductForm();
		if(product != null)
		{
			productForm = new ProductForm(product);
			String imageDataString = "";
			if (product.getimageByte() != null) {
				product.setimageString("data:image/png;base64,".concat(Base64.encodeBase64String(product.getimageByte())));
				imageDataString = "data:image/png;base64,".concat(Base64.encodeBase64String(product.getimageByte()));
			}
			model.addAttribute("imageDataString", imageDataString);
		}
		else
			product = new Product();
		model.addAttribute("ProductForm", productForm);
		SystemSetting setting = commonService.getSettingByCode(CommonConstant.EXCHANGE_RATE);
		model.addAttribute("categoryList", commonService.getAllCategory());
		model.addAttribute("currencyList", commonService.getAllCurrency());
		model.addAttribute("exchangeRateAmt", setting==null ? 0 : Integer.parseInt(setting.getValue()));
		return "productDetail";
	}
*/
	
	/*===========previous method=============*/
	
	//@RequestMapping(value = { "", "/stock/detail/{id}" })
	public String showStockDetail(Model model, @PathVariable int id) {
		Product product = new Product();
		product = productService.getProduct(id);
		String imageDataString = "";
		if (product.getimageByte() != null) {
			product.setimageString("data:image/png;base64,".concat(Base64.encodeBase64String(product.getimageByte())));
			imageDataString = "data:image/png;base64,".concat(Base64.encodeBase64String(product.getimageByte()));
		}
		System.out.println(product.getimageString());
		model.addAttribute("imageDataString", imageDataString);
		model.addAttribute("ProductForm", product);
		return "productDetail";
	}

	/*@RequestMapping(value = { "", "/stock/update/{id}" })
	public String showStockUpdate(Model model, @PathVariable int id) {
		Product product = new Product();
		String imageDataString = "";
		product = productService.getProduct(id);
		if (product.getimageByte() != null) {
			product.setimageString("data:image/png;base64,".concat(Base64.encodeBase64String(product.getimageByte())));
			imageDataString = "data:image/png;base64,".concat(Base64.encodeBase64String(product.getimageByte()));
		}
		System.out.println(imageDataString);
		model.addAttribute("imageDataString", imageDataString);
		model.addAttribute("ProductForm", product);
		return "productUpdate";
	}*/

	/*@RequestMapping(method = RequestMethod.POST, value = "/showUpdateConfirm")
	public String showUpdateConfirm(Model model, @ModelAttribute("ProductForm") Product productForm) {
		String imageDataString = "";
		byte[] imageByte = null;
		if (productForm.getproductImage() != null
				&& !(("").equals(productForm.getproductImage().getOriginalFilename()))) {
			imageByte = productService.convertFileToByteArray(productForm.getproductImage());
			imageDataString = "data:image/png;base64,".concat(Base64.encodeBase64String(imageByte));
		} else {
			if (productForm.getimageByte() != null) {
				imageByte = productForm.getimageByte();
				imageDataString = "data:image/png;base64,"
						.concat(Base64.encodeBase64String(productForm.getimageByte()));
			}
		}
		productForm.setimageByte(imageByte);
		System.out.println(imageDataString);
		model.addAttribute("ProductForm", productForm);
		model.addAttribute("imageDataString", imageDataString);
		return "productUpdateConfirm";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/showUpdateConfirm", params = "updateConfirm")
	public String updateProduct(Model model, @ModelAttribute("ProductForm") Product productForm) {
		productForm.setimageString(imageString);
		// productForm.setImageByte(imageByte);
		productService.updateProduct(productForm);
		List<Product> productList = new ArrayList<Product>();
		productList = productService.getProductList();
		model.addAttribute("productList", productList);
		return "productList";
	}*/

	/*@RequestMapping(value = { "", "/stock/delete/{id}" })
	public String showStockDelete(Model model, @PathVariable int id) {
		Product product = new Product();
		product = productService.getProduct(id);
		String imageDataString = "";
		if (product.getimageByte() != null) {
			product.setimageString("data:image/png;base64,".concat(Base64.encodeBase64String(product.getimageByte())));
			imageDataString = "data:image/png;base64,".concat(Base64.encodeBase64String(product.getimageByte()));
		}
		model.addAttribute("imageDataString", imageDataString);
		model.addAttribute("ProductForm", product);
		return "productDelete";
	}*/

	/*@RequestMapping(method = RequestMethod.POST, value = "/showDeleteConfirm")
	public String deleteProduct(Model model, @ModelAttribute("ProductForm") Product productForm) {
		productService.deleteProduct(productForm.getid());
		List<Product> productList = new ArrayList<Product>();
		productList = productService.getProductList();
		model.addAttribute("productList", productList);
		return "productList";
	}*/

}