package com.mm.cbk.stationary.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.mm.cbk.stationary.form.OrderForm;
import com.mm.cbk.stationary.form.OrderRecordForm;
import com.mm.cbk.stationary.form.Orders;
import com.mm.cbk.stationary.repository.OrderRecordRepository;
import com.mm.cbk.stationary.service.OrderService;
import com.mm.cbk.stationary.utils.CommonConstant.OrderStatus;

@Controller
public class OrderController {

	@Autowired
	OrderService orderService;
	
	@Autowired
	OrderRecordRepository orderRecordRepository;
	
	/*
	 * ======== Wut Yi Kyaw - start of order insert, update, delete , search methods ========
	 */
	@GetMapping("/viewOrdersList")
	public String viewOrdersList(Model model) {
		List<OrderForm> orderList = orderService.getOrderListByNotTargetStatus(OrderStatus.DELIVERED.getCode());
		model.addAttribute("orderList", orderList);
		return "ordersList";
	}
	
	@RequestMapping(value = "/orderCancel", method = RequestMethod.GET)
	public String cancelOrder(Model model, @RequestParam(value="id", required=true) String id) {		
		orderService.cancelOrderById(Integer.valueOf(id));
		List<OrderForm> orderList = orderService.getOrderListByNotTargetStatus(OrderStatus.DELIVERED.getCode());
		model.addAttribute("orderList", orderList);
		model.addAttribute("successMsg", "Order Cancel is finished successfully.");
		return "ordersList";
	}
	
	@GetMapping("/viewOrderRecordList")
	public String viewOrderRecordList(Model model, @RequestParam(value="id", required=true) String id) {
		initOrderRecordForm(Integer.parseInt(id), model);
		return "orderConfirm";
	}
	
	private void initOrderRecordForm(Integer id, Model model){
		OrderRecordForm orderForm = orderService.getOrderRecordsListByOrderId(id);
		model.addAttribute("OrderRecordForm", orderForm);
	}
	
	@RequestMapping(value = "/manageOrderConfirm", method = RequestMethod.POST)
	public String manageSalesOrder(@ModelAttribute("OrderRecordForm") OrderRecordForm form, Model model) {
		boolean isSuccess = orderService.manageOrderConfirm(form);
		if(isSuccess)
			model.addAttribute("successMsg", "Order Record updating is finished successfully.");
		else
			model.addAttribute("errorMsg", "Remaining Stock Balance is not enough. Please check order delivery quantity.");
		initOrderRecordForm(form.getOrderId(), model);
		return "orderConfirm";
	}
	
	@GetMapping("/viewInvoiceOrderList")
	public String viewInvoiceOrderList(Model model, @RequestParam(value="id", required=true) String id,
			@RequestParam(value="message", required=false) String msg) {
		initOrderRecordForm(Integer.parseInt(id), model);
		if(msg != null)//trick for retrieve data from cache after clicking Order Confirm in order invoice page
			model.addAttribute("invConfirmOKMsg", msg);
		return "orderInvoice";
	}
	
	@RequestMapping(value = "/invoiceOrderConfirm", method = RequestMethod.POST)
	public String manageInvoiceOrderConfirm(@ModelAttribute("OrderRecordForm") OrderRecordForm form, Model model) {

		Orders order = orderService.getOrderByOrderId(form.getOrderId());
		if(order.getOrderStatus() == OrderStatus.ORDER.getCode()){
			boolean isSuccess = orderService.invoiceOrderConfirm(form);
			if(isSuccess)
				model.addAttribute("successMsg", "Order confirmation is finished successfully.");
			else
				model.addAttribute("errorMsg", "Failed to make order confirmation. Please check stock remaining balance (or) order status.");
		}
		else if(order.getOrderStatus() == OrderStatus.CONFIRM.getCode())
			model.addAttribute("errorMsg", "This order is already confirmed.");
		else if(order.getOrderStatus() == OrderStatus.CANCEL.getCode())
			model.addAttribute("errorMsg", "This order is already cancelled.");
		else
			model.addAttribute("errorMsg", "You cannot confirm that order. Please check order status.");
		initOrderRecordForm(form.getOrderId(), model);
		return "orderInvoice";
	}
		
}
