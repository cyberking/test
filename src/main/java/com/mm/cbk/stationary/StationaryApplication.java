package com.mm.cbk.stationary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StationaryApplication extends SpringApplication{

	public static void main(String[] args) {
		SpringApplication.run(StationaryApplication.class, args);
	}
}
